<?php declare(strict_types=1);

namespace Terah\Utils;

use Exception;
use ZipArchive;
use Terah\Assert\Assert;

/**
 * Class FileUtils
 *
 * @package Terah\Utils
 */
class FileUtils
{
    /**
     * Append data on to a file
     *
     * @param string $fullPath
     * @param string $line
     *
     * @return bool
     */
    public static function appendToFile(string $fullPath, string $line) : bool
    {
        Assert::that(file_put_contents($fullPath, $line, FILE_APPEND))->notFalse("File ({$fullPath})could not be appended to.");
        return true;
    }

    /**
     * @param string $fullPath
     * @return int
     */
    public static function countLines(string $fullPath) : int
    {
        foreach ( ['/usr/bin/wc'] as $wcPath )
        {
            if ( file_exists($wcPath) )
            {
                $fullPath   = escapeshellarg($fullPath);
                $output     = static::runCommand("{$wcPath} -l {$fullPath}");
                $output     = trim(implode('', $output));
                return (int)trim(StringUtils::before(' ', $output));
            }
        }
        $handle = gzopen($fullPath, 'r');
        Assert::that($handle)->notEmpty("Could not open file handle");
        return self::countLinesInHandle($handle);
    }

    /**
     * @param string $command
     * @return mixed
     */
    public static function runCommand(string $command)
    {
        exec($command, $output, $return);
        Assert::that($return)->eq(0, "The command ({$command}) failed to run");

        return $output;
    }

    /**
     * Truncate a file
     *
     * @param string $fullPath
     * @param bool $createIfMissing
     *
     * @return bool
     */
    public static function truncateFile(string $fullPath, bool $createIfMissing=false) : bool
    {
        if ( $createIfMissing && ! file_exists($fullPath) )
        {
            return static::createDirectoriesAndSaveFile($fullPath, '');
        }
        Assert::that(file_put_contents($fullPath, ''))->notFalse("File ({$fullPath})could not be truncated.");

        return true;
    }

    /**
     * Move a file
     *
     * @param string $origin
     * @param string $destination
     *
     * @return bool
     */
    public static function moveFile(string $origin, string $destination) : bool
    {
        Assert::that($origin)->file("The file ({$origin}) does not exist");
        Assert::that(rename($origin, $destination))->notFalse("File could move file {$origin} from to {$destination}");

        return true;
    }

    /**
     * Move a file
     *
     * @param string $origin
     * @param string $destination
     *
     * @return bool
     */
    public static function moveUploadedFile(string $origin, string $destination) : bool
    {
        Assert::that($origin)->file("The file ({$origin}) does not exist");
        Assert::that(move_uploaded_file($origin, $destination))->notFalse("File could move file {$origin} from to {$destination}");
        Assert::that($origin)->file("The file ({$destination}) does not exist");

        return true;
    }

    /**
     * Get a list of files in a directory
     *
     * @param string $directory
     * @param bool $skipHiddenFiles
     * @param string $fileMaskRegex
     *
     * @return array
     */
    public static function getDirectoryListing(string $directory, bool $skipHiddenFiles=true, string $fileMaskRegex='') : array
    {
        Assert::that($directory)->directory("The directory ({$directory}) does not exist");
        $files      = scandir($directory);
        Assert::that($files)->notFalse("The directory ({$directory}) could not be scanned.");
        if ( $skipHiddenFiles )
        {
            $files =  array_filter($files, function($fileName) {

                return ( ! preg_match('/^\./', $fileName) );
            });
        }
        if ( $fileMaskRegex )
        {
            // Use preg_filter for this
            $files =  array_filter($files, function($fileName) use ($fileMaskRegex) {

                return preg_match($fileMaskRegex, $fileName);
            });
        }

        return $files;
    }

    /**
     * @param array $file_list
     * @param string $base_dir
     * @return bool
     */
    public static function deleteFiles(array $file_list, string $base_dir='') : bool
    {
        throw new Exception("Needs testing");
//        $base_dir = empty($base_dir) ? '' : StringUtils::addTrailingSlash($base_dir);
//        $new_list = [];
//        foreach ( $file_list as $idx => $file )
//        {
//            $file = "{$base_dir}{$file}";
//            if ( mb_strpos($file, '*') !== false )
//            {
//                $globbed_files = glob($file);
//                if ( is_array($globbed_files) )
//                {
//                    $new_list = array_merge($new_list, $globbed_files);
//                }
//            }
//            else
//            {
//                $new_list[] = $file;
//            }
//            unset($file_list[$idx]);
//        }
//        $new_list = array_unique($new_list);
//        foreach ( $new_list as $idx => $file )
//        {
//            Logger::debug("Deleting file ({$file})");
//            if ( !file_exists($file) )
//            {
//                Logger::debug("File does not exists: {$file}");
//                unset($new_list[$idx]);
//                continue;
//            }
//            if ( !empty($base_dir) && mb_strpos($file, $base_dir) === false )
//            {
//                Logger::error("Attempting to delete file ({$file}) outside the 'base_dir' directory");
//                unset($new_list[$idx]);
//                continue;
//            }
//            if ( !static::deleteFileOrDirectory($file) )
//            {
//                Logger::error("Failed to delete file ({$file}).");
//                unset($new_list[$idx]);
//                continue;
//            }
//            unset($new_list[$idx]);
//        }
//        return true;
    }

    /**
     * @param string $volume
     * @return bool
     */
    public static function isVolumeMounted(string $volume) : bool
    {
        $command    = "mount | grep -i \"{$volume}\"";
        exec($command, $output, $result);
        Assert::that($result)->eq(0, "The command ({$command}) failed with output: " . print_r($output, true));

        $output = ! empty($output) ? array_shift($output) : '';

        return empty($output) ? false : true;
    }

    /**
     * @param string $fileOrDirectory
     * @return bool
     */
    public static function deleteFileOrDirectory(string $fileOrDirectory) : bool
    {
        clearstatcache();
        Assert::that($fileOrDirectory)
            ->notEmpty("The file or directory is empty")
            ->notEq(DIRECTORY_SEPARATOR, "You cannot delete the file or directory ({$fileOrDirectory}).")
            ->fileOrDirectoryExists($fileOrDirectory, "The file or directory ({$fileOrDirectory}) does not exist.");

        if ( is_dir($fileOrDirectory) )
        {
            $fileOrDirectory = escapeshellarg($fileOrDirectory);
            exec("rm -rf {$fileOrDirectory}", $output, $return);
            Assert::that($return)->eq(0, "The directory you are trying to delete could not be deleted ({$fileOrDirectory}).");
            clearstatcache();

            return true;
        }
        if ( is_file($fileOrDirectory) )
        {
            Assert::that(unlink($fileOrDirectory))->notFalse("The file you are trying to delete could not be deleted ({$fileOrDirectory}).");
            clearstatcache();

            return true;
        }
        Assert::that(false)->true("The file or directory you are trying to delete could not be identified ({$fileOrDirectory}).");

        return false;
    }

    /**
     * @param string $fileOrDirectory
     * @return bool
     */
    public static function deleteFileOrDirectoryIfExists(string $fileOrDirectory) : bool
    {
        clearstatcache();
        Assert::that($fileOrDirectory)
            ->notEmpty("The file or directory is empty")
            ->notEq(DIRECTORY_SEPARATOR, "You cannot delete the file or directory ({$fileOrDirectory}).");
        if ( ! file_exists($fileOrDirectory) )
        {
            return true;
        }

        return static::deleteFileOrDirectory($fileOrDirectory);
    }


    public static function createDirectoriesAndSaveFile(string $filePath, string $data, int $flags=0, int $dirMode=0755) : bool
    {
        static::createParentDirectories($filePath, $dirMode);
        Assert::that(file_put_contents($filePath, $data, $flags))->notFalse("Failed to put contents in file ({$filePath})");

        return true;
    }


    public static function createDirectoriesAndMoveFile(string $sourceFile, string $destFile) : bool
    {
        static::createParentDirectories($destFile);
        return static::moveFile($sourceFile, $destFile);
    }


    public static function createDirectoriesAndCopyFile(string $sourcePath, string $destPath) : bool
    {
        Assert::that($sourcePath)->notEmpty()->file("Failed to copy file ({$sourcePath}) to ({$destPath}), source file does not exist.");
        static::createParentDirectories($destPath);
        Assert::that(copy($sourcePath, $destPath))->notFalse("Failed to copy file ({$sourcePath}) to ({$destPath})");
        Assert::that($destPath)->file("File copied from ({$sourcePath}) to ({$destPath}) OK but can't be found.");
        return true;
    }


    public static function createParentDirectories(string $filePath, int $mode=0755) : bool
    {
        $directoryPath = StringUtils::beforeLast(DIRECTORY_SEPARATOR, $filePath);
        Assert::that($directoryPath)->notEmpty("Failed to identify path ({$directoryPath}) to create");
        if ( file_exists($directoryPath) )
        {
            Assert::that(is_dir($directoryPath))->notFalse("Failed to create parent directories.. files exists and is not a directory({$directoryPath})");

            return true;
        }
        Assert::that(mkdir($directoryPath, $mode, true))->notFalse("Failed to create parent directories ({$directoryPath})");
        Assert::that($directoryPath)->directory();

        return true;
    }

    /**
     * @param string   $fullPathToFile
     * @param bool     $withMeta
     * @param ?callable $fnIsDataLine
     * @param ?callable $fnLineToArray
     *
     * @return array|resource
     */
    public static function getFileHandle(string $fullPathToFile, bool $withMeta=false, ?callable $fnIsDataLine=null, ?callable $fnLineToArray=null)
    {
        Assert::that($fullPathToFile)->file("The file does not exist");
        Assert::that($withMeta)->boolean();

        $handle = gzopen($fullPathToFile, 'r');
        Assert::that($handle)->notEmpty("Could not open file handle");

        if ( ! $withMeta )
        {
            return $handle;
        }
        $count                  = self::countLinesInHandle($handle, $fnIsDataLine);
        $columns                = self::getRowFromHandle($handle, true, $fnIsDataLine, $fnLineToArray);

        return [
            'count'     => $count,
            'columns'   => array_keys($columns),
            'handle'    => $handle,
        ];
    }

    /**
     * @param resource $handle
     * @param bool     $rewind
     * @param ?callable $fnIsDataLine
     * @param ?callable $fnLineToArray
     *
     * @return bool|string|array
     */
    public static function getRowFromHandle($handle, bool $rewind=false, ?callable $fnIsDataLine=null, ?callable $fnLineToArray=null)
    {
        // Use a loop as we may have to skip over some invalid lines
        while ( ( $line = gzgets($handle, 4096) ) !== false )
        {
            // No data in this row?
            if ( ! is_null($fnIsDataLine) && ! $fnIsDataLine($line) )
            {
                continue;
            }
            if ( $rewind )
            {
                gzrewind($handle);
            }

            return is_callable($fnLineToArray) ? $fnLineToArray($line) : $line;
        }
        // No rows left?
        gzclose($handle);

        return false;
    }

    /**
     * @param      $filePath
     * @param callable $lineCallback
     * @param callable|null $doRowCallback
     * @return int
     */
    public static function processFile(string $filePath, callable $lineCallback, callable $doRowCallback=null) : int
    {
        $handle     = FileUtils::getFileHandle($filePath);
        $success    = 0;
        while ( $result = FileUtils::getRowFromHandle($handle, false, $doRowCallback, $lineCallback) )
        {
            if ( $result )
            {
                $success++;
            }
            if ( is_null($result) )
            {
                return $success;
            }
        }

        return $success;
    }

    /**
     * @param string $filePath
     * @param string $destZip
     * @return string
     */
    public static function zipFile(string $filePath, string $destZip='') : string
    {
        Assert::that($filePath)->fileOrDirectoryExists("File to zip ({$filePath}) does not exist");
        $pathInfo           = (object)pathinfo($filePath);
        $zip                = new ZipArchive();
        $destZip            = $destZip ? $destZip : tempnam(sys_get_temp_dir(), 'zip');
        $zip->open($destZip, ZipArchive::CREATE);
        $zip->addFile($filePath, $pathInfo->basename);
        $zip->close();

        return $destZip;
    }

    /**
     * @param resource      $handle
     * @param callable|null $fnIsDataLine
     * @return int
     */
    protected static function countLinesInHandle($handle, callable $fnIsDataLine=null) : int
    {
        $count = -1;
        while ( ( $line = gzgets($handle, 4096) ) !== false )
        {
            if ( is_null($fnIsDataLine) || $fnIsDataLine($line) )
            {
                $count++;
            }
        }
        gzrewind($handle);

        return $count === -1 ? 0 : $count;
    }


}

