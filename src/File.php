<?php declare(strict_types=1);

namespace Terah\Utils;

use Exception;
use Terah\Assert\Assert;

/**
 * Class File
 *
 * @package Terah\Utils
 */
class File
{

    protected string $_filePath;

    /**  @var resource $resource The file handle */
    protected $_resource        = null;

    protected bool $_closeLocally   = true;

    protected bool $_useLocking     = false;

    protected bool $_gzipFile       = false;

    protected bool $_createDirs     = false;

    protected ?string $_fileGroup   = null;

    protected ?int $_fileMode       = null;


    public function __construct(string $filePath, $gzipFile=false, $useLocking=false, $createDirs=false)
    {
        $this->filePath($filePath);
        $this->useLocking($useLocking);
        $this->gzipFile($gzipFile);
        $this->createDirs($createDirs);
    }


    public function filePath(string $filePath) : File
    {
        Assert::that($filePath)->notEmpty();
        $this->_filePath        = $filePath;

        return $this;
    }


    public function useLocking(bool $useLocking=true) : File
    {
        Assert::that($useLocking)->boolean();
        $this->_useLocking      = $useLocking;

        return $this;
    }


    public function gzipFile(bool $gzipFile=true) : File
    {
        Assert::that($gzipFile)->boolean();
        $this->_gzipFile        = $gzipFile;

        return $this;
    }


    public function createDirs(bool $createDirs=true) : File
    {
        Assert::that($createDirs)->boolean();
        $this->_createDirs      = $createDirs;

        return $this;
    }


    public function fileMode(int $fileMode=0644) : File
    {
        Assert::that($fileMode)->int();
        $this->_fileMode        = $fileMode;

        return $this;
    }


    public function fileGroup(string $fileGroup) : File
    {
        $this->_fileGroup       = $fileGroup;

        return $this;
    }


    // Write the content to the stream
    public function write(string $content) : bool
    {
        $resource = $this->_getResource();
        if ( $this->_useLocking )
        {
            flock($resource, LOCK_EX);
        }
        gzwrite($resource, $content);
        if ( $this->_useLocking )
        {
            flock($resource, LOCK_UN);
        }

        return true;
    }

    /**
     * @return mixed|resource
     */
    protected function _getResource()
    {
        if ( is_resource($this->_resource) )
        {
            return $this->_resource;
        }
        $this->_closeLocally = true;
        $this->_createParentDir();

        return $this->_openResource();
    }


    protected function _createParentDir() : bool
    {
        $parentDir = dirname($this->_filePath);
        if ( $this->_createDirs && ! file_exists($parentDir) && ! mkdir($parentDir, 0777, true) )
        {
            throw new Exception("Failed to create parent directory ({$parentDir})");
        }
        clearstatcache();
        if ( ! is_writable($parentDir) )
        {
            throw new Exception("The resource directory [{$parentDir}] is not writable");
        }
        if ( file_exists($this->_filePath) && ! is_writable($this->_filePath) )
        {
            throw new Exception("The resource [{$this->_filePath}] is not writable");
        }

        return true;
    }

    /**
     * @return resource
     */
    protected function _openResource()
    {
        if ( ! file_exists($this->_filePath) && ( ! is_null($this->_fileMode) || ! is_null($this->_fileGroup) ) )
        {
            Assert::that(touch($this->_filePath))->notFalse('Failed to touch log file');
            if ( ! is_null($this->_fileMode) )
            {
                clearstatcache();
                Assert::that(chmod($this->_filePath, $this->_fileMode))->notFalse('Failed to chmod log file');
            }
            if ( ! is_null($this->_fileGroup) )
            {
                clearstatcache();
                Assert::that(chgrp($this->_filePath, $this->_fileGroup))->notFalse('Failed to chgrp log file');
            }
        }
        $this->_resource = $this->_gzipFile ? gzopen($this->_filePath, 'a') : fopen($this->_filePath, 'a');
        if ( ! is_resource($this->_resource) )
        {
            throw new Exception("The resource ({$this->_filePath}) could not be opened");
        }
        return $this->_resource;
    }

    public function __destruct()
    {
        if ($this->_closeLocally)
        {
            gzclose($this->_getResource());
        }
    }
}