<?php declare(strict_types=1);

namespace Terah\Utils;

use Closure;
use Exception;
use RuntimeException;
use Terah\Assert\Assert;
use stdClass;

/**
 * Slim HTTP Request
 *
 * This class provides a human-friendly interface to the Slim environment variables;
 * environment variables are passed by reference and will be modified directly.
 *
 * @package Slim
 * @author  Josh Lockhart
 * @since   1.0.0
 */

/**
 * Class Request
 *
 * @package Terah\Utils
 */
class Request
{
    const METHOD_HEAD                               = 'HEAD';
    const METHOD_GET                                = 'GET';
    const METHOD_POST                               = 'POST';
    const METHOD_PUT                                = 'PUT';
    const METHOD_PATCH                              = 'PATCH';
    const METHOD_DELETE                             = 'DELETE';
    const METHOD_OPTIONS                            = 'OPTIONS';
    const METHOD_OVERRIDE                           = '_METHOD';

    /** @var string[]  */
    protected static array $formDataMediaTypes      = [
        'application/x-www-form-urlencoded'
    ];

    protected array $_data                          = [
        'get'                                           => [],
        'post'                                          => [],
        'cookie'                                        => [],
        'files'                                         => [],
        'env'                                           => [],
        //'server'                                        => [],
        'headers'                                       => [],
    ];

    /** @var Closure[]|array  */
    protected array $_middleware                    = [];

     protected array $_special                      = [
        'CONTENT_TYPE',
        'CONTENT_LENGTH',
        'PHP_AUTH_USER',
        'PHP_AUTH_PW',
        'PHP_AUTH_DIGEST',
        'AUTH_TYPE'
    ];

    protected string $auth_token_header             = '';

    protected array $monitoring_ips                 = [];

    protected string $monitoring_regex              = '';


    public function __construct(array $config)
    {
        static::cliToHttpRequest($config);
        $this->_data['get']         = ! empty($_GET) ? $_GET : [];
        $this->_data['post']        = ! empty($_POST) ? $_POST : file_get_contents("php://input");
        $this->_data['post']        = is_array($this->_data['post']) ? $this->_data['post'] : json_decode($this->_data['post'], true);
        $this->_data['cookie']      = ! empty($_COOKIE) ? $_COOKIE : [];
        $this->_data['files']       = ! empty($_FILES) ? $_FILES : [];
        $this->_data['env']         = $this->generateEnv();
        $this->_data['server']      = ! empty($_SERVER) ? $_SERVER : [];
        $this->_data['headers']     = $this->populateHeaderData();
        $this->auth_token_header    = ! empty($config['auth_token_header']) ? $config['auth_token_header'] : '';
        $this->monitoring_ips       = ! empty($config['monitoring_ips']) ? $config['monitoring_ips'] : [];
        $this->monitoring_regex     = ! empty($config['monitoring_regex']) ? $config['monitoring_regex'] : '';
    }


    public function middleware(string $name, Closure $fnMiddleware) : Request
    {
        $this->_middleware[$name] = $fnMiddleware;

        return $this;
    }


    public function runMiddleware(array $routeInfo) : bool
    {
        foreach ( $this->_middleware as $name => $fnCallback )
        {
            $fnCallback->__invoke($this, $routeInfo);
        }

        return true;
    }


    protected function populateHeaderData() : array
    {
        if ( function_exists('getallheaders') )
        {
            $this->_data['headers'] = getallheaders();
            $this->_data['headers'] = $this->_data['headers'] ?: [];

            return $this->_data['headers'];
        }
        foreach ( $this->_data['server'] as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $this->_data['headers'][str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }

        return $this->_data['headers'];
    }


    protected function generateEnv() : array
    {
        $env                    = [];
        //The HTTP request method
        $env['REQUEST_METHOD']  = $_SERVER['REQUEST_METHOD'];
        //The IP
        $env['REMOTE_ADDR']     = $_SERVER['REMOTE_ADDR'];
        // Server params
        $scriptName             = $_SERVER['SCRIPT_NAME']; // <-- "/foo/index.php"
        $requestUri             = $_SERVER['REQUEST_URI']; // <-- "/foo/bar?test=abc" or "/foo/index.php/bar?test=abc"
        $queryString            = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : ''; // <-- "test=abc" or ""
        // Physical path
        $physicalPath           = strpos($requestUri, $scriptName) !== false ? $scriptName : str_replace('\\', '', dirname($scriptName));
        $env['SCRIPT_NAME']     = rtrim($physicalPath, '/'); // <-- Remove trailing slashes
        // Virtual path
        $env['PATH_INFO']       = substr($requestUri, 0, strlen($physicalPath)) == $physicalPath ? substr($requestUri, strlen($physicalPath)) : $requestUri;
        $env['PATH_INFO']       = str_replace('?' . $queryString, '', $env['PATH_INFO']); // <-- Remove query string
        $env['PATH_INFO']       = '/' . ltrim($env['PATH_INFO'], '/'); // <-- Ensure leading slash
        // Query string (without leading "?")
        $env['QUERY_STRING']    = $queryString;
        //Name of server host that is running the script
        $env['SERVER_NAME']     = $_SERVER['SERVER_NAME'];
        //Number of server port that is running the script
        //Fixes: https://github.com/slimphp/Slim/issues/962
        $env['SERVER_PORT']     = isset($_SERVER['SERVER_PORT']) ? $_SERVER['SERVER_PORT'] : 80;
        //HTTP request headers (retains HTTP_ prefix to match $_SERVER)
        $headers                = $this->extractHeaders($_SERVER);
        foreach ($headers as $key => $value)
        {
            $env[$key] = $value;
        }
        //Is the application running under HTTPS or HTTP protocol?
        $env['PROTO']           = empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === 'off' ? 'http' : 'https';
        //Input stream (readable one time only; not available for multipart/form-data requests)
        $env['RAW_INPUT']       = @file_get_contents('php://input');
        if ( ! $env['RAW_INPUT'] )
        {
            $env['RAW_INPUT']       = '';
        }
        //Error stream
        $env['STD_ERR']         = @fopen('php://stderr', 'w');
        $env['FILE_EXT']        = $this->parseFileExt((string)$env['PATH_INFO']);

        return $env;
    }


    // Extract HTTP headers from an array of data (e.g. $_SERVER)
    public function extractHeaders(array $data) : array
    {
        $results                = [];
        foreach ($data as $key => $value)
        {
            $key = strtoupper($key);
            if ( strpos($key, 'X_') === 0 || strpos($key, 'HTTP_') === 0 || in_array($key, $this->_special))
            {
                if ( $key === 'HTTP_CONTENT_LENGTH' )
                {
                    continue;
                }
                $results[$key] = $value;
            }
        }

        return $results;
    }


    public function parseFileExt(string $path='') : string
    {
        $path       = $path ?: $this->getPathInfo();
        preg_match('/\.([a-zA-Z0-9]+)$/', $path, $matches);

        return count($matches) ? (string)$matches[1] : '';
    }


    public function getFileExt() : string
    {
        return (string)$this->env('FILE_EXT');
    }


    public function getMethod() : string
    {
        return (string)$this->env('REQUEST_METHOD');
    }


    public function isGet() : bool
    {
        return $this->getMethod() === self::METHOD_GET;
    }


    public function isPost() : bool
    {
        return $this->getMethod() === self::METHOD_POST;
    }


    public function isPut() : bool
    {
        return $this->getMethod() === self::METHOD_PUT;
    }


    public function isPatch() : bool
    {
        return $this->getMethod() === self::METHOD_PATCH;
    }


    public function isDelete() : bool
    {
        return $this->getMethod() === self::METHOD_DELETE;
    }


    public function isHead() : bool
    {
        return $this->getMethod() === self::METHOD_HEAD;
    }


    public function isOptions() : bool
    {
        return $this->getMethod() === self::METHOD_OPTIONS;
    }


    public function isMonitoring() : bool
    {
        // Don't throw auth errors for Xymon monitoring
        $userAgent              = $this->getUserAgent();
        $remoteIp               = $this->getIp();
        if ( empty($this->monitoring_ips) && empty($this->monitoring_regex) )
        {
            return false;
        }
        if ( $this->monitoring_ips && ! in_array($remoteIp, $this->monitoring_ips) )
        {
            return false;
        }
        if ( $this->monitoring_regex && ! preg_match($this->monitoring_regex, $userAgent) )
        {
            return false;
        }

        return  true;
    }


    public function isAjax() : bool
    {
        if ( $this->params('isajax') )
        {
            return true;
        }
        elseif ( isset($this->_data['headers']['X_REQUESTED_WITH']) && $this->_data['headers']['X_REQUESTED_WITH'] === 'XMLHttpRequest' )
        {
            return true;
        }
        return false;
    }


    public function isXhr() : bool
    {
        return $this->isAjax();
    }

    /**
     * Fetch GET and POST data
     *
     * This method returns a union of GET and POST data as a key-value array, or the value
     * of the array key if requested; if the array key does not exist, NULL is returned,
     * unless there is a default value specified.
     *
     * @param  string           $key
     * @param  mixed            $default
     * @return array|mixed|null
     */
    public function params(string $key='', $default=null)
    {
        $union                  = array_merge($this->get(), $this->post());
        if ( $key )
        {
            return isset($union[$key]) ? $union[$key] : $default;
        }

        return $union;
    }

    /**
     * Fetch PUT data (alias for \Slim\Http\Request::post)
     * @param  string           $key
     * @param  mixed            $default Default return value when key does not exist
     * @return array|mixed|null
     */
    public function put(string $key='', $default=null)
    {
        return $this->post($key, $default);
    }

    /**
     * Fetch PATCH data (alias for \Slim\Http\Request::post)
     * @param  string           $key
     * @param  mixed            $default Default return value when key does not exist
     * @return array|mixed|null
     */
    public function patch(string $key='', $default=null)
    {
        return $this->post($key, $default);
    }

    /**
     * Fetch DELETE data (alias for \Slim\Http\Request::post)
     * @param  string           $key
     * @param  mixed            $default Default return value when key does not exist
     * @return array|mixed|null
     */
    public function delete(string $key='', $default=null)
    {
        return $this->post($key, $default);
    }


    public function getResponseType() : string
    {
        if ( static::isCli() )
        {
            return 'txt';
        }
        if ( StringUtils::before('?', $this->getRequestUri(), true) === '/rpc' )
        {
            return 'json';
        }
        if ( ( $fileExt = $this->getFileExt() ) )
        {
            return $fileExt;
        }
        if ( ( $type = $this->getAcceptType() ) )
        {
            return $type;
        }

        return 'html';
    }



    /**
     * @param string $default
     * @return string
     */
    public function getAcceptType(string $default='html') : string
    {
        $type       = $this->getAcceptMimeType($this->getMimeType($default));
        $parts      = explode('/', $type);
        $type       = isset($parts[1]) ? $parts[1] : $parts[0];

        return      $type === '*' ? 'html' : $type;
    }

    /**
     * @param string $default
     * @return string
     */
    public function getAcceptMimeType(string $default='text/html') : string
    {
        $header     = $this->env('HTTP_ACCEPT');
        if ( empty($header) )
        {
            return $default;
        }
        $sections   = explode(';', $header);
        $types      = explode(',', $sections[0]);

        return $types[0];
    }

    /**
     * @param string $type
     * @return string
     */
    public function getMimeType(string $type) : string
    {
        $formats = [
            'html'  => ['text/html', 'application/xhtml+xml'],
            'txt'   => ['text/plain'],
            'text'  => ['text/plain'],
            'js'    => ['application/javascript', 'application/x-javascript', 'text/javascript'],
            'css'   => ['text/css'],
            'json'  => ['application/json', 'application/x-json'],
            'xml'   => ['text/xml', 'application/xml', 'application/x-xml'],
            'rdf'   => ['application/rdf+xml'],
            'atom'  => ['application/atom+xml'],
            'rss'   => ['application/rss+xml'],
            'form'  => ['application/x-www-form-urlencoded'],
            'png'   => ['image/png'],
            'pdf'   => ['application/pdf'],
        ];

        return array_key_exists($type, $formats) ? (string)$formats[$type][0] : '';
    }

    /**
     * Get Content Type
     *
     * @param string $default
     *
     * @return string
     */
    public function getContentType(string $default='application/json') : string
    {
        return (string)$this->headers('CONTENT_TYPE', $default);
    }

    /**
     * Get Media Type (type/subtype within Content Type header)
     * @return string
     */
    public function getMediaType() : string
    {
        $contentType    = $this->getContentType();
        if ( $contentType )
        {
            $contentTypeParts = preg_split('/\s*[;,]\s*/', $contentType);

            return strtolower($contentTypeParts[0]);
        }

        return '';
    }

    /**
     * Get Media Type Params
     * @return array
     */
    public function getMediaTypeParams() : array
    {
        $contentType = $this->getContentType();
        $contentTypeParams = [];
        if ( $contentType )
        {
            $contentTypeParts       = preg_split('/\s*[;,]\s*/', $contentType);
            $contentTypePartsLength = count($contentTypeParts);
            for ( $i = 1; $i < $contentTypePartsLength; $i++ )
            {
                $paramParts = explode('=', $contentTypeParts[$i]);
                $contentTypeParams[strtolower($paramParts[0])] = $paramParts[1];
            }
        }

        return $contentTypeParams;
    }

    /**
     * Get Content Charset
     * @return string
     */
    public function getContentCharset() : string
    {
        $mediaTypeParams = $this->getMediaTypeParams();
        if ( isset($mediaTypeParams['charset']) )
        {
            return $mediaTypeParams['charset'];
        }

        return '';
    }

    /**
     * Get Content-Length
     * @return int
     */
    public function getContentLength() : int
    {
        return (int)$this->headers('CONTENT_LENGTH', 0);
    }

    /**
     * Get Host
     * @return string
     */
    public function getHost() : string
    {
        if ( ( $host = (string)$this->env('HTTP_HOST') ) )
        {
            if ( strpos($this->env('HTTP_HOST'), ':') !== false )
            {
                $hostParts = explode(':', $this->env('HTTP_HOST'));

                return (string)$hostParts[0];
            }
            return $host;
        }

        return (string)$this->env('SERVER_NAME');
    }

    /**
     * Get Host with Port
     * @return string
     */
    public function getHostWithPort() : string
    {
        return sprintf('%s:%s', $this->getHost(), (string)$this->getPort());
    }

    /**
     * Get Port
     * @return int
     */
    public function getPort() : int
    {
        return (int)$this->env('SERVER_PORT');
    }

    /**
     * Get Scheme (https or http)
     * @return string
     */
    public function getScheme() : string
    {
        return (string)$this->env('PROTO');
    }

    /**
     * Get Script Name (physical path)
     * @return string
     */
    public function getScriptName() : string
    {
        return (string)$this->env('SCRIPT_NAME');
    }

    /**
     * LEGACY: Get Root URI (alias for Slim_Http_Request::getScriptName)
     * @return string
     */
    public function getRootUri() : string
    {
        return $this->getScriptName();
    }

    /**
     * Get Path (physical path + virtual path)
     * @return string
     */
    public function getPath() : string
    {
        return $this->getScriptName() . $this->getPathInfo();
    }

    /**
     * Get Path Info (virtual path)
     * @return string
     */
    public function getPathInfo() : string
    {
        return (string)$this->env('PATH_INFO');
    }

    /**
     * LEGACY: Get Resource URI (alias for Slim_Http_Request::getPathInfo)
     * @return string
     */
    public function getResourceUri() : string
    {
        return $this->getPathInfo();
    }

    /**
     * Get URL (scheme + host [ + port if non-standard ])
     * @return string
     */
    public function getUrl() : string
    {
        $url = $this->getScheme() . '://' . $this->getHost();
        if ( ($this->getScheme() === 'https' && $this->getPort() !== 443) || ($this->getScheme() === 'http' && $this->getPort() !== 80) )
        {
            $url .= sprintf(':%s', (string)$this->getPort());
        }

        return $url;
    }

    /**
     * @return string
     */
    public function getUrlWithQueryString() : string
    {
        $queryStr               = empty($_SERVER['QUERY_STRING']) ? '' : '?' . $_SERVER['QUERY_STRING'];

        return $this->getUrl() . $this->getPathInfo() . $queryStr;
    }

    /**
     * Get IP
     * @return string
     */
    public function getIp() : string
    {
        $keys                   = ['X_FORWARDED_FOR', 'HTTP_X_FORWARDED_FOR', 'CLIENT_IP', 'REMOTE_ADDR'];
        foreach ( $keys as $key )
        {
            if ( ( $ip = $this->env($key) ) )
            {
                // Weirdly, Ips going to the load balanced prism were coming in as 10.48.64.86, 10.208.8.15
                // It's probably got something to to with the multiple layers of HTTP servers this is all running through
                // This picks the first..
                return trim((string)explode(',', $ip)[0]);
            }
        }

        return (string)$this->env('REMOTE_ADDR');
    }

    /**
     * Get Referrer
     * @return string
     */
    public function getReferrer() : string
    {
        return (string)$this->server('HTTP_REFERER');
    }

    /**
     * Get Referer (for those who can't spell)
     * @return string
     */
    public function getReferer() : string
    {
        return $this->getReferrer();
    }

    /**
     * Get User Agent
     * @return string
     */
    public function getUserAgent() : string
    {
        return (string)$this->server('HTTP_USER_AGENT');
    }

    /**
     * @param mixed $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return string|array
     */
    public function get(string $name='', $default=null, bool $unescaped=false, bool $trim=true)
    {
        return $this->_getParam('get', $name, $default, $unescaped, $trim);
    }

    /**
     * @param string $name
     * @param int[] $default
     * @return int[]
     */
    public function getArrayOfInts(string $name='', $default=[]) : array
    {
        $data   = $this->getArray($name, $default);

        return array_map(function($id){ return (int)$id; }, $data);
    }

    /**
     * @param string $name
     * @param array $default
     * @param bool $unescaped
     * @param bool $trim
     * @return array
     */
    public function getArray(string $name='', array $default=[], bool $unescaped=false, bool $trim=true) : array
    {
        $data   = $this->_getParam('get', $name, $default, $unescaped, $trim);
        $data   = $data ?: [];

        return is_array($data) ? $data : [$data];
    }

    /**
     * @param string $name
     * @param int $default
     * @return int
     */
    public function getInt(string $name, int $default=0) : int
    {
        $value = $this->_getParam('get', $name, $default);
        Assert::that($value)->emptyOr()->numeric('Value is not numeric');

        return (int)$value;
    }

    /**
     * @param string $name
     * @param float $default
     * @return float
     */
    public function getFloat(string $name, float $default=0.00) : float
    {
        $value = $this->_getParam('get', $name, $default);
        Assert::that($value)->emptyOr()->numeric('Value is not numeric');

        return (float)$value;
    }

    /**
     * @param string $name
     * @param string $default
     * @param bool $unescaped
     * @param bool $trim
     * @return string
     */
    public function getStr(string $name, string $default='', bool $unescaped=false, bool $trim=true) : string
    {
        $value                  = $this->_getParam('get', $name, $default, $unescaped, $trim);

        return is_array($value) ? implode('|', $value) : (string)$value;
    }

    /**
     * @param string $name
     * @param bool $default
     * @return bool
     */
    public function getBool(string $name, bool $default=false) : bool
    {
        $value = $this->_getParam('get', $name, $default);
        Assert::that($value)->emptyOr()->scalar('Values is not scalar');

        return (bool)$value;
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return string|array
     */
    public function post(string $name='', $default=null, bool $unescaped=false, bool $trim=true)
    {
        return $this->_getParam('post', $name, $default, $unescaped, $trim);
    }

    /**
     * @param string $name
     * @param array $default
     * @param bool $unescaped
     * @param bool $trim
     * @return array
     */
    public function postArray(string $name='', array $default=[], bool $unescaped=false, bool $trim=true) : array
    {
        $data   = $this->_getParam('post', $name, $default, $unescaped, $trim);
        $data   = is_null($data) ? [] : $data;

        return is_array($data) ? $data : [$data];
    }

    /**
     * @param string $name
     * @param int $default
     * @return int
     */
    public function postInt(string $name, int $default=0) : int
    {
        $value = $this->_getParam('post', $name, $default);
        Assert::that($value)->emptyOr()->numeric('Value is not numeric');

        return (int)$value;
    }

    /**
     * @param string $name
     * @param float $default
     * @return float
     */
    public function postFloat(string $name, float $default=0.00) : float
    {
        $value = $this->_getParam('post', $name, $default);
        Assert::that($value)->emptyOr()->numeric('Value is not numeric');

        return (float)$value;
    }

    /**
     * @param string $name
     * @param string $default
     * @param bool $unescaped
     * @param bool $trim
     * @return string
     */
    public function postStr(string $name, string $default='', bool $unescaped=false, bool $trim=true) : string
    {
        $value = $this->_getParam('post', $name, $default, $unescaped, $trim);

        return (string)$value;
    }

    /**
     * @param string $name
     * @param bool $default
     * @return bool
     */
    public function postBool(string $name, bool $default=false) : bool
    {
        $value = $this->_getParam('post', $name, $default);
        Assert::that($value)->emptyOr()->scalar('Values is not scalar');

        return (bool)$value;
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return string|array
     */
    public function cookie(string $name='', $default=null, bool $unescaped=false, bool $trim=true)
    {
        return $this->_getParam('cookie', $name, $default, $unescaped, $trim);
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return string|array
     */
    public function files(string $name='', $default=null, bool $unescaped=false, bool $trim=false)
    {
        return $this->_getParam('files', $name, $default, $unescaped, $trim);
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return string|array
     */
    public function server(string $name='', $default=null, bool $unescaped=false, bool $trim=false)
    {
        return $this->_getParam('server', $name, $default, $unescaped, $trim);
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return string|array
     */
    public function headers(string $name='', $default=null, bool $unescaped=false, bool $trim=false)
    {
        return $this->_getParam('headers', $name, $default, $unescaped, $trim);
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return stdClass
     */
    public function getToObj(string $name='', $default=null, bool $unescaped=false, bool $trim=true) : stdClass
    {
        $data = json_decode(json_encode($this->_getParam('get', $name, $default, $unescaped, $trim)));

        return is_object($data) ? $data : (object)$data;
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return mixed
     */
    public function postToObj(string $name='', $default=null, bool $unescaped=false, bool $trim=true)
    {
        return json_decode(json_encode($this->_getParam('post', $name, $default, $unescaped, $trim)));
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return mixed
     */
    public function cookieToObj(string $name='', $default=null, bool $unescaped=false, bool $trim=true)
    {
        return json_decode(json_encode($this->_getParam('cookie', $name, $default, $unescaped, $trim)));
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return mixed
     */
    public function filesToObj(string $name='', $default=null, bool $unescaped=false, bool $trim=false)
    {
        return json_decode(json_encode($this->_getParam('files', $name, $default, $unescaped, $trim)));
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return mixed
     */
    public function serverToObj(string $name='', $default=null, bool $unescaped=false, bool $trim=false)
    {
        return json_decode(json_encode($this->_getParam('server', $name, $default, $unescaped, $trim)));
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @param bool|false $unescaped
     * @param bool|false $trim
     * @return mixed
     */
    public function headersToObj(string $name='', $default=null, bool $unescaped=false, bool $trim=false)
    {
        return json_decode(json_encode($this->_getParam('headers', $name, $default, $unescaped, $trim)));
    }

    /**
     * @param string $type
     * @param string $field
     * @param mixed $default
     * @param bool $unescaped
     * @param bool $trim
     * @return mixed
     */
    protected function _getParam(string $type, $field='', $default=null, bool $unescaped=false, bool $trim=true)
    {
        Assert::that($this->_data)->keyExists($type);

        if ( empty($field) )
        {
            $data = isset($this->_data[$type]) && is_array($this->_data[$type]) ? $this->_data[$type] : [];

            return $unescaped ? $data : $this->_escape($data, $trim);
        }
        if ( ! is_array($field) )
        {
            $data = isset($this->_data[$type][$field]) ? $this->_data[$type][$field] : $default;

            return $unescaped ? $data : $this->_escape($data, $trim);
        }
        $results = [];
        foreach ( $field as $var )
        {
            $data = isset($this->_data[$type][$var]) ? $this->_data[$type][$var] : $default;
            $results[] = $unescaped ? $data : $this->_escape($data, $trim);
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getRedirectQueryString() : string
    {
        if ( $this->getMethod() !== 'GET' )
        {
            return '';
        }
        $pathInfo   = $this->getPathInfo();
        if ( $pathInfo === '/' )
        {
            return '';
        }
        $urlPart    = StringUtils::before('?', $pathInfo, true);
        if ( preg_match('/html$/', $urlPart ) )
        {
            return '?r=' . base64_encode($pathInfo);
        }
        $referrer           = $this->getReferer();
        $urlPart            = StringUtils::before('?', $referrer, true);
        $hostWithProto      = $this->getScheme() . '://' . $this->getHost();
        $urlPart            = str_replace($hostWithProto, '', $urlPart);
        if ( preg_match('/html$/', $urlPart ) )
        {
            return '?r=' . base64_encode($urlPart);
        }

        return '';
    }

    /**
     * @param $data
     * @param bool|true $trim
     * @return array|mixed
     */
    protected function _escape($data, bool $trim=true)
    {
        if ( is_array($data) )
        {
            foreach ( $data as $idx => $val )
            {
                $data[$idx] = $this->_escape($val, $trim);
            }
            return $data;
        }
        if ( is_string($data) )
        {
            $data   = $trim ? trim($data) : $data;
            $data = str_replace(["\r\n", "\\r\\n", "\\n"], "\n", filter_var($data, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));
            return $trim ? trim($data) : $data;
        }
        Assert::that($data)->nullOr()->scalar('The data passed to escape must be of array or scalar type');

        return $data;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function env(string $name, $default=null)
    {
        $param      = $this->_getParam('env', $name);
        $param      = $param ? $param : $this->_getParam('server', $name, $default);

        return $param ? $param : getenv($name);
    }

    /**
     * @return string
     */
    public function getRequestUri() : string
    {
        return (string)$this->env('REQUEST_URI');
    }

    /**
     * @return bool
     */
    public static function isCli() : bool
    {
        return defined("STDERR");
    }

    /**
     * @return bool
     */
    public function isCommandLine() : bool
    {
        return static::isCli();
    }

    /**
     * @param string $displayField
     * @return array
     */
    public function queryStrToSearchParams(string $displayField) : array
    {
        $getVars = $this->_data['get'];
        if ( isset($getVars['_fields']) )
        {
            $getVars['_fields'] = explode('|', $getVars['_fields']);
            if ( in_array('_display_field', $getVars['_fields']) )
            {
                $getVars['_fields'][] = $displayField;
            }
        }
        if ( isset($getVars['_order']) )
        {
            $getVars['_order'] = explode('|', $getVars['_order']);
        }
        unset($this->_data['get']['_']);

        return $getVars;
    }

    /**
     * @return string
     */
    public function getApiToken() : string
    {
        if ( ! static::isCli() )
        {
            return (string)$this->headers($this->auth_token_header);
        }
        if ( ( $authToken  = $this->env('AUTH_TOKEN') ) )
        {
            return (string)$authToken;
        }

        return '';
    }

    /**
     * PARSE ARGUMENTS
     *
     * This command line option parser supports any combination of three types of options
     * [single character options (`-a -b` or `-ab` or `-c -d=dog` or `-cd dog`),
     * long options (`--foo` or `--bar=baz` or `--bar baz`)
     * and arguments (`arg1 arg2`)] and returns a simple array.
     *
     * [pfisher ~]$ php test.php --foo --bar=baz --spam eggs
     *   ["foo"]   => true
     *   ["bar"]   => "baz"
     *   ["spam"]  => "eggs"
     *
     * [pfisher ~]$ php test.php -abc foo
     *   ["a"]     => true
     *   ["b"]     => true
     *   ["c"]     => "foo"
     *
     * [pfisher ~]$ php test.php arg1 arg2 arg3
     *   [0]       => "arg1"
     *   [1]       => "arg2"
     *   [2]       => "arg3"
     *
     * [pfisher ~]$ php test.php plain-arg --foo --bar=baz --funny="spam=eggs" --also-funny=spam=eggs \
     * > 'plain arg 2' -abc -k=value "plain arg 3" --s="original" --s='overwrite' --s
     *   [0]       => "plain-arg"
     *   ["foo"]   => true
     *   ["bar"]   => "baz"
     *   ["funny"] => "spam=eggs"
     *   ["also-funny"]=> "spam=eggs"
     *   [1]       => "plain arg 2"
     *   ["a"]     => true
     *   ["b"]     => true
     *   ["c"]     => true
     *   ["k"]     => "value"
     *   [2]       => "plain arg 3"
     *   ["s"]     => "overwrite"
     *
     * Not supported: `-cd=dog`.
     *
     * @author              Patrick Fisher <patrick@pwfisher.com>
     * @since               August 21, 2009
     * @see                 https://github.com/pwfisher/CommandLine.php
     * @see                 http://www.php.net/manual/en/features.commandline.php
     *                      #81042 function arguments($argv) by technorati at gmail dot com, 12-Feb-2008
     *                      #78651 function getArgs($args) by B Crawford, 22-Oct-2007
     * @usage               $args = CommandLine::parseArgs($_SERVER['argv']);
     * @param array $argv
     * @return array
     */
    public static function parseArgs(array $argv=[]) : array
    {
        $argv = $argv ?: ( ! empty($_SERVER['argv']) ? $_SERVER['argv'] : [] );
        array_shift($argv);
        $out = [];
        for ( $i = 0, $j = count($argv); $i < $j; $i++ )
        {
            $arg = $argv[$i];
            // --foo --bar=baz
            if ( mb_substr($arg, 0, 2) === '--' )
            {
                $eqPos = mb_strpos($arg, '=');
                // --foo
                if ($eqPos === false)
                {
                    $key = mb_substr($arg, 2);
                    // --foo value
                    if ($i + 1 < $j && $argv[$i + 1][0] !== '-')
                    {
                        $value = $argv[$i + 1];
                        $i++;
                    }
                    else
                    {
                        $value = isset($out[$key]) ? $out[$key] : true;
                    }
                    $out[$key] = $value;
                }
                // --bar=baz
                else
                {
                    $key        = mb_substr($arg, 2, $eqPos - 2);
                    $value      = mb_substr($arg, $eqPos + 1);
                    $out[$key]  = $value;
                }
            }
            // -k=value -abc
            else if (mb_substr($arg, 0, 1) === '-')
            {
                // -k=value
                if (mb_substr($arg, 2, 1) === '=')
                {
                    $key       = mb_substr($arg, 1, 1);
                    $value     = mb_substr($arg, 3);
                    $out[$key] = $value;
                }
                // -abc
                else
                {
                    $chars = str_split(mb_substr($arg, 1));
                    $key = '';
                    foreach ( $chars as $char )
                    {
                        $key       = $char;
                        $value     = isset($out[$key]) ? $out[$key] : true;
                        $out[$key] = $value;
                    }
                    // -a value1 -abc value2
                    if ($i + 1 < $j && $argv[$i + 1][0] !== '-')
                    {
                        $out[$key] = $argv[$i + 1];
                        $i++;
                    }
                }
            }
            // plain-arg
            else
            {
                $value = $arg;
                $out[] = $value;
            }
        }
        foreach ( $out as $idx => $val )
        {
            if ( is_string($val) && strpos($val, '|') !== false )
            {
                $out[$idx] = explode('|', $val);
            }
        }

        return $out;
    }

    /**
     * @param array $params
     * @param array  $argv
     * @return bool
     */
    public static function cliToHttpRequest(array $params, array $argv=[]) : bool
    {
        $host = $port = '';
        extract($params);
        if ( ! defined('STDERR') )
        {
            return true;
        }
        $args = static::parseArgs($argv);
        $uri  = [];
        foreach ( $args as $key => $value )
        {
            if ( ! is_numeric($key) )
            {
                break;
            }
            $uri[$key] = $value;
            unset($args[$key]);
        }
        $_GET                       = $args;
        $_SERVER['REQUEST_METHOD']  = 'GET';
        $_SERVER['SERVER_NAME']     = $host;
        $_SERVER['SERVER_PORT']     = $port;
        $_SERVER['REMOTE_ADDR']     = '127.0.0.1';
        $_SERVER['REQUEST_URI']     = '/' . implode('/', $uri);
        $_SERVER['QUERY_STRING']    = http_build_query($_GET);

        return true;
    }
}

/**
 * NotFoundHttpException.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Ben Ramsey <ben@benramsey.com>
 */
interface HttpExceptionInterface
{
    /**
     * Returns the status code.
     *
     * @return int An HTTP response status code
     */
    public function getStatusCode() : int;
    /**
     * Returns response headers.
     *
     * @return array Response headers
     */
    public function getHeaders() : array;
}

/**
 * Class HttpException
 *
 * @package Terah\Utils
 */
class HttpException extends RuntimeException implements HttpExceptionInterface
{
    private int $statusCode = 500;
    private array $headers    = [];

    /**
     * HttpException constructor.
     *
     * @param int             $statusCode
     * @param string          $message
     * @param Exception|null $previous
     * @param array           $headers
     * @param int             $code
     */
    public function __construct(int $statusCode, string $message='', Exception $previous=null, array $headers=[], $code = 0)
    {
        $this->statusCode   = $statusCode;
        $this->headers      = $headers;

        parent::__construct($message, $code === 0 ? $statusCode : $code, $previous);
    }

    /**
     * @return int
     */
    public function getStatusCode() : int
    {
        return (int)$this->statusCode;
    }

    /**
     * @return array
     */
    public function getHeaders() : array
    {
        return $this->headers;
    }
}


class AccessDeniedHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(403, $message, $previous, [], $code);
    }
}


class BadRequestHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(400, $message, $previous, [], $code);
    }
}


class ConflictHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(409, $message, $previous, [], $code);
    }
}


class GoneHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(410, $message, $previous, [], $code);
    }
}


class LengthRequiredHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(411, $message, $previous, [], $code);
    }
}


class MethodNotAllowedHttpException extends HttpException
{
    public function __construct(array $allow, string $message='', ?Exception $previous=null, int $code=0)
    {
        $headers = ['Allow' => strtoupper(implode(', ', $allow))];
        parent::__construct(405, $message, $previous, $headers, $code);
    }
}


class NotAcceptableHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(406, $message, $previous, [], $code);
    }
}


class NotFoundHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(404, $message, $previous, [], $code);
    }
}


class PreconditionFailedHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(412, $message, $previous, [], $code);
    }
}


class PreconditionRequiredHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(428, $message, $previous, [], $code);
    }
}


class ServiceUnavailableHttpException extends HttpException
{
    public function __construct(int $retryAfter=0, string $message='', ?Exception $previous=null, int $code=0)
    {
        $headers = [];
        if ( $retryAfter )
        {
            $headers = ['Retry-After' => $retryAfter];
        }
        parent::__construct(503, $message, $previous, $headers, $code);
    }
}


class TooManyRequestsHttpException extends HttpException
{
    public function __construct(int $retryAfter=0, string $message='', ?Exception $previous=null, int $code=0)
    {
        $headers = [];
        if ( $retryAfter )
        {
            $headers = ['Retry-After' => $retryAfter];
        }
        parent::__construct(429, $message, $previous, $headers, $code);
    }
}


class UnauthorizedHttpException extends HttpException
{
    public function __construct($message='You do not have permission to access this resource.', Exception $previous=null, $code=0)
    {
        parent::__construct(401, $message, $previous, [], $code);
    }
}


class UserNotFound extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(401, $message, $previous, [], $code);
    }
}


class UnprocessableEntityHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(422, $message, $previous, [], $code);
    }
}


class UnsupportedMediaTypeHttpException extends HttpException
{
    public function __construct(string $message='', ?Exception $previous=null, int $code=0)
    {
        parent::__construct(415, $message, $previous, [], $code);
    }
}

