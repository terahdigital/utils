<?php declare(strict_types=1);

namespace Terah\Utils;

use stdClass;

class CliUtils
{
    const EXIT_STATUS_SUCCESS = 0;
    const EXIT_STATUS_FAILURE = 1;

    /**
     * @param int $status
     * @param bool $exit
     * @return bool
     */
    public static function exitWithStatus(int $status, bool $exit=true) : bool
    {
        if ( $exit )
        {
            exit($status);
        }

        return static::EXIT_STATUS_FAILURE === $status;
    }

    /**
     * Whether php is called in CLI client
     *
     * @return boolean
     */
    static function isCommandLine() : bool
    {
        return defined("STDERR");
    }

    /**
     * @param string $cmdStr
     * @return stdClass
     */
    public static function exec(string $cmdStr) : stdClass
    {
        exec($cmdStr, $outputArr, $return_val);

        return (object)[
            'output' => $outputArr,
            'result' => $return_val == 0,
        ];
    }

    /**
     * @param string $cmdStr
     * @param string $logger
     * @param string $identifier
     * @param string $stripFromLog
     * @return stdClass
     */
    public static function execCommandAndLog(string $cmdStr, string $logger='debug', string $identifier='', string $stripFromLog='') : stdClass
    {
        $res_obj = static::execCommandAndLogToObj($cmdStr, $logger, $identifier, $stripFromLog);

        return $res_obj->result;
    }

    /**
     * @param string $cmdStr
     * @param string $logger
     * @param string $identifier
     * @param string $stripFromLog
     * @return stdClass
     */
    public static function execCommandAndLogToObj(string $cmdStr, string $logger='debug', string $identifier='', string $stripFromLog='') : stdClass
    {
        //$identifier     = $identifier ?: md5($cmdStr) . '-' . date('YmdHis');
        //$logStr 	    = ! $stripFromLog ? $cmdStr : str_replace($stripFromLog, 'XXXXXXX', $cmdStr);
        //App::log()->logWithIdentifier("Running command: {$log_str}", $logger, $identifier);
        exec($cmdStr, $outputArr, $returnVal);

        return (object)[
            'output' 	=> $outputArr,
            'result' 	=> $returnVal == 0,
            'line' 		=> '',
        ];
    }

    public static function execCommand(string $command) : string
    {
        exec($command, $outputArr, $returnVal);

        return is_array($outputArr) ? implode("\n", $outputArr) : '';
    }

    /**
     * Converting object/array to CLI friendly string
     *
     * @param array $ary
     * @param string $separator
     * @return string
     */
    public static function aryToStr(array $ary, string $separator=";") : string
    {
        if (is_string($ary)) return $ary;
        if (empty($ary)) return "";
        // Using http_build_query because json_encode does not work on old servers. feel free to change this method.

        return urldecode(http_build_query($ary, "", $separator));
    }

    /**
     * Print backtrace with nicer format.
     * @param  boolean $returnAsString
     * @param  boolean $unset_last_trace	In case this method is called from some logging method.
     * @param  integer $substring_args	If positive number, substring args.
     * @return string/void
     */
    public static function backtrace(bool $returnAsString=false, bool $unset_last_trace=false, int $substring_args=64) : string
    {
        $return_string = "BACKTRACE (inner comes first):\n";
        //$orders = ['file','line','function'];
        $i=1;

        $bt = debug_backtrace();
        $pad = mb_strlen(count($bt))+5;
        unset($bt[0]);
        if($unset_last_trace) unset($bt[1]);

        foreach($bt as $inner_ary)
        {
            $return_string .= str_pad("".$i.". ", $pad, " ", STR_PAD_LEFT);
            $return_string .= "{$inner_ary['file']}:{$inner_ary['line']}:{$inner_ary['function']}".PHP_EOL;

            if(!empty($inner_ary['args'])) {
                $return_string .= str_pad("", $pad, " ", STR_PAD_LEFT)."ARGS(".count($inner_ary['args'])."):".PHP_EOL;
                foreach($inner_ary['args'] as $ai=>$arg)
                {
                    if (is_array($arg)) {
                        $val=static::aryToStr($arg);
                        $val="array(".count($arg)."):".$val;
                    }
                    else if ($arg===true) {
                        $val="bool:TRUE";
                    }
                    else if ($arg===false) {
                        $val="bool:FALSE";
                    }
                    else if ($arg===null) {
                        $val="NULL";
                    }
                    else if ($arg==='') {
                        $val="string(0):\"\"";
                    }
                    else if (is_string($arg)) {
                        $val="string(".mb_strlen($arg)."):\"{$arg}\"";
                    }
                    else {
                        //$val=static::aryToStr($arg);
                        $val=str_replace("\n","; ",print_r($arg, true));
                    }
                    if($substring_args > 0 && mb_strlen($val) > $substring_args) $val = mb_substr($val, 0, $substring_args)."...";
                    $return_string .= str_pad(($ai+1), ($pad+4), " ", STR_PAD_LEFT)." = {$val}".PHP_EOL;
                }
                $return_string .= PHP_EOL;
            }
            $i++;
        }
        if ( ! $returnAsString )
        {
            print $return_string;
        }

        return $return_string;
    }


    public static function getPidsByUid(?string $uid=null, bool $add_prefix=true)  : ?string
    {
        if ( is_null($uid) )
        {
            return null;
        }
        $uid                    = $add_prefix ? "--uid={$uid}" : $uid;

        return static::isOtherProcessRunning($uid);
    }


    public static function killByUid(?string $uid=null, bool $add_prefix=true) : ?stdClass
    {
        if ( is_null($uid) )
        {
            return null;
        }
        $pids_str               = static::getPidsByUid($uid, $add_prefix);
        if ( !$pids_str )
        {
            return null;
        }

        return static::killByPids($pids_str);
    }

    public static function killByPids(string $pids_str) : ?stdClass
    {
        if ( empty($pids_str) )
        {
            return null;
        }

        return static::execCommandAndLogToObj("kill -9 {$pids_str}");
    }

    /**
     * Check PS outputs with given filename or with regular expression (-E, not -P)
     * Note: ps auxwww output does not show single/double quote
     *       Also to support FreeBSD, using grep -E, not -P, so that regex is limited.
     *
     * @param string $filename_or_regex
     * @return string|null
     */
    public static function isOtherProcessRunning(string $filename_or_regex) : ?string
    {
        $my_pid            = getmypid();
        // FIXME: This might be a good enough but strictly speaking, the following line should loop until $my_ppid becomes 0.
        $my_ppid           = exec("ps -p {$my_pid} -o ppid=");
        $my_ppid           = trim($my_ppid);
        // FIXME: Need more complex escaping. currently escaping single quote and '--' ('grep -E --' is for escaping "--" in $filename_or_regex)
        $filename_or_regex = escapeshellarg($filename_or_regex);
        $cmdStr         = "ps auxwww | grep -E -- {$filename_or_regex} | grep -v -w -E '(grep|{$my_ppid}|{$my_pid})' | awk '{print $2}'";
        $res = static::execCommandAndLogToObj($cmdStr);
        if( $res->result && !empty($res->output[0]) )
        {
            return implode(' ', $res->output);
        }
        return null;
    }

    public static function isDirInLocalFileSystem($dir_path): bool
    {
        if (!is_dir($dir_path)) return false;

        // FIXME: Old FreeBSD's df or stat does not show file system type. Once we get rid of those servers, please change below command.
        $output=`df -P {$dir_path} | grep '^/dev/'`;
        $output_trimmed = trim($output);

        return (empty($output_trimmed))?false:true;
    }

    /**
     * Ask a question.
     * TODO: does not support secret question (eg: password)
     *
     * @param string $question
     * @param string $default
     * @return string
     */
    public static function ask(string $question, string $default="") : string
    {
        if ( !empty($default) )
        {
            echo "{$question} [{$default}]: ";
        }
        else
        {
            echo "{$question}: ";
        }

        $handle         = fopen("php://stdin","r");
        $line           = fgets($handle);
        $line_trimmed   = trim($line);

        if ( empty($line_trimmed) && !empty($default) )
        {
            return $default;
        }
        return $line_trimmed;
    }

    /**
     * @param $name
     * @return bool|string
     */
    public static function getBinary($name)
    {
        $name   = is_array($name) ? $name : [$name];
        $paths  = ['/bin/', '/usr/local/bin/', '/usr/local/bin/svnversion', '/usr/bin/'];
        foreach ( $name as $binary )
        {
            foreach ( $paths as $path )
            {
                if ( file_exists("{$path}{$binary}") )
                {
                    return "{$path}{$binary}";
                }
            }
        }
        return false;
    }
}
