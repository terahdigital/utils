<?php declare(strict_types=1);

namespace Terah\Utils;

use Closure;
use Terah\Assert\Assert;

class Registry {

    /** @var array  */
    static protected array $resources         = [];

    /** @var array  */
    static protected array $shutDownFunctions = [];

    /**
     * @param string $name
     * @param object|callable $object
     *
     * @return bool
     */
    static public function set(string $name, $object) : bool
    {
        static::$resources[$name] = $object;

        return true;
    }

    /**
     * @param string $name
     * @param object|callable $object
     *
     * @return bool
     */
    static protected function _set(string $name, $object) : bool
    {
        return static::set($name, $object);
    }

    /**
     * @param $name
     * @return object
     */
    static protected function _get(string $name): object
    {
        return static::get($name);
    }

    /**
     * @param $name
     * @return object
     */
    static public function get(string $name): object
    {
        Assert::that(static::$resources)->keyExists($name, "The object {$name} does not exist in the registry.");
        if ( is_object(static::$resources[$name]) && (static::$resources[$name] instanceof Closure) )
        {
            static::$resources[$name] = static::$resources[$name]->__invoke();
        }

        return static::$resources[$name];
    }

    /**
     * @param string $name
     * @param array $args
     * @return object|bool
     */
    public static function __callStatic(string $name, array $args)
    {
        return empty($args) ? static::_get($name) : static::_set($name, $args[0]);
    }


    public static function isRegistered(string $name) : bool
    {
        return ! empty(static::$resources[$name]);
    }


    public static function isInitialised(string $name) : bool
    {
        return static::isRegistered($name) && is_object(static::$resources[$name]);
    }


    public static function registerShutDownFunction(Closure $fnCallback)
    {
        static::$shutDownFunctions[] = $fnCallback;
    }

    public static function shutDown()
    {
        foreach ( static::$shutDownFunctions as $fnCallback )
        {
            $fnCallback();
        }
    }
}