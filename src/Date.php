<?php declare(strict_types=1);

namespace Terah\Utils;

use DateTime;
use DateTimeZone;

/**
 * Class Date
 * @package Terah\Utils
 */
class Date
{
    const ONE_DAY                   = 86400;
    const ONE_WEEK                  = 60480060;
    const ONE_HOUR                  = 3600;
    const TEN_MINS                  = 600;
    const FIVE_MINS                 = 300;

    /**
     * Set the default timezone
     *
     * @param array $config
     */
    public static function init(array $config)
    {
        date_default_timezone_set($config['time_zone']);
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function date($time=null) : string
    {
        return date('Y-m-d', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function formattedDate($time=null) : string
    {
        return date('M j, Y', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function time($time=null) : string
    {
        return date('H:i:s', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function dateTime($time=null) : string
    {
        return date('Y-m-d H:i:s', Date::getTime($time));
    }

    /**
     * @return string
     */
    public static function dateMicroTime() : string
    {
        return DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''))->setTimezone(new DateTimeZone('Australia/Brisbane'))->format('Y-m-d H:i:s:u');
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function dayDateTime($time=null) : string
    {
        return date('D, M j, Y g:i A', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function atom($time=null) : string
    {
        return date('Y-m-d\TH:i:sP', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function cookie($time=null) : string
    {
        return date('l, d-M-y H:i:s T', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function iso8601($time=null) : string
    {
        return date('Y-m-d\TH:i:sO', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function rfc822($time=null) : string
    {
        return date(DATE_RFC822, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function rfc850($time=null) : string
    {
        return date(DATE_RFC850, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function rfc1036($time=null) : string
    {
        return date(DATE_RFC1036, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function rfc1123($time=null) : string
    {
        return date(DATE_RFC1123, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function rfc2822($time=null) : string
    {
        return date(DATE_RFC2822, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function rfc3339($time=null) : string
    {
        return date(DATE_RFC3339, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function rss($time=null) : string
    {
        return date(DATE_RSS, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function w3c($time=null) : string
    {
        return date(DATE_W3C, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function report($time=null) : string
    {
        return date('Y-m-d-H-i', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function pretty($time=null) : string
    {
        return date(DATE_RFC822, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function letter($time=null) : string
    {
        return date('D, d M Y', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function numericWithMicro($time=null) : string
    {
        return date('YmdHisu', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function invoice($time=null) : string
    {
        return date('d-M-Y', Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return string
     */
    public static function journal($time=null) : string
    {
        return date('dmY', Date::getTime($time));
    }

    /**
     * @param string $format
     * @param string|int|null $time
     * @return string
     */
    public static function custom(string $format, $time=null) : string
    {
        return date($format, Date::getTime($time));
    }

    /**
     * @param string|int|null $time
     * @return int
     */
    public static function getTime($time=null) : int
    {
        if ( ! $time )
        {
            return time();
        }
        if ( is_int($time) )
        {
            return $time;
        }

        return strtotime($time);
    }


    /**
     * http://snippetsofcode.wordpress.com/2012/08/25/php-function-to-convert-seconds-into-human-readable-format-months-days-hours-minutes/
     *
     * @param $seconds
     * @return string
     */
    static function seconds2human(int $seconds) : string
    {
        $secs       = $seconds % 60;
        $mins       = floor(($seconds % 3600)/60) > 0         ?   floor(($seconds % 3600)/60) . ' minutes'       : '';
        $hours      = floor(($seconds % 86400) / 3600) > 0    ?   floor(($seconds % 86400) / 3600) . ' hours'    : '';
        $days       = floor(($seconds % 2592000) / 86400) > 0 ?   floor(($seconds % 2592000) / 86400) . ' days'  : '';
        $months     = floor($seconds / 2592000) > 0           ?   floor($seconds / 2592000) . ' months'          : '';

        return trim("{$months} {$days} {$hours} {$mins} {$secs} seconds");
    }

    /**
     * @param      $from_time
     * @param null $to_time
     * @param bool $include_seconds
     *
     * @return string
     * @see: http://trac.symfony-project.org/browser/branches/1.4/lib/helper/DateHelper.php
     */
    public static function distanceOfTimeInWords($from_time, $to_time = null, $include_seconds = false): string
    {
        $to_time = $to_time? $to_time: time();

        $distance_in_minutes = floor(abs($to_time - $from_time) / 60);
        $distance_in_seconds = floor(abs($to_time - $from_time));

        $parameters = [];

        if ($distance_in_minutes <= 1)
        {
            if (!$include_seconds)
            {
                $string = $distance_in_minutes == 0 ? 'less than a minute' : '1 minute';
            }
            else
            {
                if ($distance_in_seconds <= 5)
                {
                    $string = 'less than 5 seconds';
                }
                else if ($distance_in_seconds >= 6 && $distance_in_seconds <= 10)
                {
                    $string = 'less than 10 seconds';
                }
                else if ($distance_in_seconds >= 11 && $distance_in_seconds <= 20)
                {
                    $string = 'less than 20 seconds';
                }
                else if ($distance_in_seconds >= 21 && $distance_in_seconds <= 40)
                {
                    $string = 'half a minute';
                }
                else if ($distance_in_seconds >= 41 && $distance_in_seconds <= 59)
                {
                    $string = 'less than a minute';
                }
                else
                {
                    $string = '1 minute';
                }
            }
        }
        else if ($distance_in_minutes >= 2 && $distance_in_minutes <= 44)
        {
            $string = '%minutes% minutes';
            $parameters['%minutes%'] = $distance_in_minutes;
        }
        else if ($distance_in_minutes >= 45 && $distance_in_minutes <= 89)
        {
            $string = 'about 1 hour';
        }
        else if ($distance_in_minutes >= 90 && $distance_in_minutes <= 1439)
        {
            $string = 'about %hours% hours';
            $parameters['%hours%'] = round($distance_in_minutes / 60);
        }
        else if ($distance_in_minutes >= 1440 && $distance_in_minutes <= 2879)
        {
            $string = '1 day';
        }
        else if ($distance_in_minutes >= 2880 && $distance_in_minutes <= 43199)
        {
            $string = '%days% days';
            $parameters['%days%'] = round($distance_in_minutes / 1440);
        }
        else if ($distance_in_minutes >= 43200 && $distance_in_minutes <= 86399)
        {
            $string = 'about 1 month';
        }
        else if ($distance_in_minutes >= 86400 && $distance_in_minutes <= 525959)
        {
            $string = '%months% months';
            $parameters['%months%'] = round($distance_in_minutes / 43200);
        }
        else if ($distance_in_minutes >= 525960 && $distance_in_minutes <= 1051919)
        {
            $string = 'about 1 year';
        }
        else
        {
            $string = 'over %years% years';
            $parameters['%years%'] = floor($distance_in_minutes / 525960);
        }

        return strtr($string, $parameters);
    }


    /**
     * @param $timestamp
     * @return bool
     */
    public static function isUnixTimestamp($timestamp): bool
    {
        return( is_int($timestamp) && strtotime(date('d-m-Y H:i:s', $timestamp) ) === (int)$timestamp );
    }

    /**
     * @param $timestamp
     * @return array
     */
    public static function dateArray($timestamp): array
    {
        $date = (object)getdate($timestamp);

        return [
            $date->seconds,
            $date->minutes,
            $date->hours,
            $date->mday,
            $date->mon - 1,
            $date->year
        ];
    }

    /**
     * @param string $format
     * @return string
     */
    public static function lastMonth(string $format='M') : string
    {
        return date($format, Date::getTime('Last month'));
    }

    /**
     * @param int $numMonths
     * @param string $format
     * @return string
     */
    public static function xMonthsAgo(int $numMonths, string $format='M') : string
    {
        return date($format, Date::getTime("{$numMonths} months ago"));
    }
}