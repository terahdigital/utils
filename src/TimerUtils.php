<?php declare(strict_types=1);

namespace Terah\Utils;

use Terah\Assert\Assert;

/**
 * Class TimerUtils
 *
 * @method static array tick($key='app', $remove=false)
 * @method static float timeTaken($key='app')
 * @method static string getStart($key='app', $format=false)
 * @method static string getLast($key='app', $format=false)
*/
class TimerUtils
{

    protected static ?Timer $_oTimer = null;


    public static function init() : ?Timer
    {
        static::$_oTimer = new Timer();

        return static::$_oTimer;
    }

    /**
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic(string $name, array $arguments)
    {
        Assert::that(static::$_oTimer)->notEmpty("The Timer object has not been initialized.");
        Assert::that($name)->methodExists(static::$_oTimer, "The method {$name} does not exist on timer");

        return call_user_func_array([static::$_oTimer, $name], $arguments);
    }


    public static function getTimer() : ?Timer
    {
        return static::$_oTimer;
    }
}
