<?php declare(strict_types=1);

namespace Terah\Utils;

/*
The MIT License

Copyright (c) 2010 Antoine Leclair <antoineleclair@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

use Exception;

/**
 * Taken from https://raw.githubusercontent.com/ockam/php-csv/master/csv.php
 *
 * Class CsvWriter
 */

class CsvWriter
{
    /** @var resource */
    protected $fileHandle;

    /** @var string */
    protected string $filename;


    /**
     * CsvWriter constructor.
     * @param string $filename
     * @param string $mode
     */
    public function __construct(string $filename='', string $mode='w')
    {
        if ( $mode != 'w' and $mode != 'a' )
        {
            throw new Exception('CsvWriter only accepts "w" and "a" mode.');
        }
        $this->filename     = $filename ?: $this->tempFilename();
        $this->fileHandle   = fopen($this->filename, $mode);
        if ( ! $this->fileHandle )
        {
            throw new Exception("Impossible to open file {$this->filename}.");
        }
    }

    /**
     * @param array $rows
     */
    public function addLines(array $rows)
    {
        foreach ( $rows as $idx => $line )
        {
            $this->addLine((array)$line);
        }
    }

    /**
     * @param array $values
     */
    public function addLine(array $values)
    {
        foreach ( $values as $key => $value )
        {
            $values[$key] = utf8_decode(Csv::escapeString($value));
        }
        $string = implode(',', $values) . "\r\n";
        fwrite($this->fileHandle, $string);
    }

    /**
     * @return string
     */
    protected function tempFilename() : string
    {
        return tempnam(sys_get_temp_dir(), "csv_writer_");
    }

    public function writeToStdOut()
    {
        readfile($this->filename);
    }

    /**
     * @return string
     */
    public function writeToString() : string
    {
        return file_get_contents($this->filename);
    }

    // You should not have to call it unless you need to flush the
    // data from the buffer to your file explicitly before the
    // end of your script
    public function close()
    {
        if ( $this->fileHandle )
        {
            fclose($this->fileHandle);
            $this->fileHandle = null;
        }
    }

    public function __destruct()
    {
        $this->close();
    }
}