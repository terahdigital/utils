<?php declare(strict_types=1);


namespace Terah\Utils;

use PDO;
use stdClass;

class SqlUtils
{

    /**
     * @param string $dsn
     * @param string $user
     * @param string $pass
     * @return PDO
     */
    public static function getPdo(string $dsn, string $user, string $pass) : PDO
    {
        $pdo    = new PDO($dsn, $user, $pass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        $pdo->exec("SET NAMES utf8 COLLATE utf8_unicode_ci");

        return $pdo;
    }

    /**
     * @param string $dsn
     * @param string $name
     * @return string
     */
    public static function getValueFromDsn(string $dsn, string $name) : string
    {
        $driver                 = strpos($dsn, ':') !== false ? substr($dsn, 0, strpos($dsn, ':')) : $dsn;
        $driver                 = ucfirst(strtolower($driver));
        $dsn                    = preg_replace("/^{$driver}:/i", '', $dsn);
        $parts                  = explode(';', $dsn);
        foreach ( $parts as $conf )
        {
            $conf                   = explode('=', $conf);
            $config[$conf[0]]       = isset($conf[1]) ? $conf[1] : true;
        }
        $config['driver']       = $driver;

        return $config[$name];
    }

    /**
     * @param string $sql
     * @param string $idx
     * @param string $val
     * @param array  $params
     * @param PDO    $pdo
     * @return array
     */
    public static function fetchList(string $sql, string $idx, string $val, array $params, PDO $pdo): array
    {
        $res    = static::fetchAll($sql, $params, $pdo);
        $data   = [];
        foreach ( $res as $row )
        {
            $row = (array)$row;
            $data[$row[$idx]] = $row[$val];
        }
        return $data;
    }

    /**
     * @param string $sql
     * @param array  $params
     * @param PDO    $pdo
     * @return array
     */
    public static function fetchAll(string $sql, array $params, PDO $pdo) : array
    {
        $sth = $pdo->prepare($sql);
        $sth->execute($params);

        $allData = $sth->fetchAll(PDO::FETCH_OBJ);
        $sth->closeCursor();

        return $allData;
    }

    /**
     * @param string $sql
     * @param array  $params
     * @param PDO    $pdo
     * @return stdClass
     */
    public static function fetchOne(string $sql, array $params, PDO $pdo) : stdClass
    {
        $sth = $pdo->prepare($sql);
        $sth->execute($params);

        $allData = $sth->fetchObject();
        $sth->closeCursor();

        return $allData;
    }

    /**
     * @param string $sql
     * @param array  $params
     * @param PDO    $pdo
     * @return mixed
     */
    public static function fetchField(string $sql, array $params, PDO $pdo)
    {
        $sth = $pdo->prepare($sql);
        $sth->execute($params);

        return $sth->fetchColumn();
    }

    /**
     * @param string $input
     * @param bool   $stripDefiner
     * @return string
     */
    public static function cleanSchema(string $input, bool $stripDefiner=true) : string
    {
        if ( $stripDefiner )
        {
            $input = preg_replace('/DEFINER=`([a-zA-Z0-9-_]+)`@`([a-zA-Z0-9-_%]+)` /', '', $input);
        }
        //$input = str_replace('ALGORITHM = UNDEFINED SQL SECURITY DEFINER ', '', $input);
        //   $input = str_replace('ALGORITHM=UNDEFINED SQL SECURITY DEFINER ', '', $input);


        $input = str_replace('\n', "\n", $input);

        return $input;
    }

    /**
     * @param string $database
     * @param PDO    $pdo
     * @return array
     */
    public static function getProcedures(string $database, PDO $pdo) : array
    {
        return static::fetchAll("SHOW PROCEDURE STATUS WHERE Db = '{$database}'", [], $pdo);
    }

    /**
     * @param string $database
     * @param PDO    $pdo
     * @param bool   $stripDefiner
     * @return string
     */
    public static function getProcedureDefs(string $database, PDO $pdo, bool $stripDefiner=true) : string
    {
        $output             = [];
        foreach ( static::getProcedures($database, $pdo) as $func )
        {
            $definition         = static::fetchOne("SHOW CREATE PROCEDURE {$func->Name}", [], $pdo);
            $definition         = array_change_key_case((array)$definition, CASE_LOWER);
            $definition         = static::cleanSchema((string)$definition['create procedure'], $stripDefiner);
            $definition         = str_replace("\nBEGIN", "\n    BEGIN", $definition);
            $output[]           = <<<SQL

-- === {$func->Name} === --
    
DELIMITER $$

DROP PROCEDURE IF EXISTS {$func->Name}$$

{$definition}$$

DELIMITER ;

SQL;
        }

        return implode("\n", $output);
    }


    /**
     * @param string $database
     * @param PDO    $pdo
     * @return array
     */
    public static function getFunctions(string $database, PDO $pdo) : array
    {
        return static::fetchAll("SHOW FUNCTION STATUS WHERE Db = '{$database}'", [], $pdo);
    }

    /**
     * @param string $database
     * @param PDO    $pdo
     * @param bool   $stripDefiner
     * @return string
     */
    public static function getFunctionDefs(string $database, PDO $pdo, bool $stripDefiner=true) : string
    {
        $output             = [];
        foreach ( static::getFunctions($database, $pdo) as $func )
        {
            $definition         = static::fetchOne("SHOW CREATE FUNCTION {$func->Name}", [], $pdo);
            $definition         = array_change_key_case((array)$definition, CASE_LOWER);
            $definition         = static::cleanSchema((string)$definition['create function']);
            $definition         = str_replace("\nBEGIN", "\n    BEGIN", $definition);
            $output[]           = <<<SQL

-- === {$func->Name} === --

DELIMITER $$

DROP FUNCTION IF EXISTS {$func->Name}$$

{$definition}$$

DELIMITER ;

SQL;
        }

        return implode("\n", $output);
    }

    /**
     * @param string $database
     * @param PDO    $pdo
     * @return array
     */
    public static function getViews(string $database, PDO $pdo) : array
    {
        return static::fetchAll("SELECT TABLE_NAME FROM information_schema.`TABLES` WHERE TABLE_TYPE LIKE 'VIEW' AND TABLE_SCHEMA = '{$database}' ORDER BY TABLE_NAME", [], $pdo);
    }

    /**
     * @param string $database
     * @param PDO    $pdo
     * @return string
     */
    public static function getViewDefs(string $database, PDO $pdo) : string
    {
        $output             = [];
        foreach ( static::getViews($database, $pdo) as $view )
        {
            $definition     = static::fetchOne("SHOW CREATE VIEW {$view->TABLE_NAME}", [], $pdo);
            $definition     = array_change_key_case((array)$definition, CASE_LOWER);
            $definition         = static::cleanSchema($definition['create view']);
            $definition         = SqlFormatter::format($definition, false);
            $output[]           = <<<SQL

-- === {$view->TABLE_NAME} === --
    
DROP VIEW IF EXISTS {$view->TABLE_NAME};
    
{$definition};


SQL;
        }

        return implode("\n", $output);
    }


    public static function getTriggers(string $database, PDO $pdo) : array
    {
        return static::fetchAll("SHOW TRIGGERS IN `{$database}`", [], $pdo);
    }


    public static function getTriggerDefs(string $database, PDO $pdo) : string
    {
        $output             = [];
        foreach ( static::getTriggers($database, $pdo) as $trigger )
        {
            $def                = static::fetchOne("SHOW CREATE TRIGGER {$trigger->Trigger}", [], $pdo);
            $def                = array_change_key_case((array)$def, CASE_LOWER);
            $definition         = static::cleanSchema($def['sql original statement']);
            $output[]           = <<<SQL

-- === {$trigger->Trigger} === --
DELIMITER $$

DROP TRIGGER IF EXISTS {$trigger->Trigger}$$

{$definition}$$

DELIMITER ;

SQL;
        }

        return implode("\n", $output);
    }



    public static function getTables(string $database, PDO $pdo) : array
    {
        return static::fetchAll("SELECT TABLE_NAME FROM information_schema.`TABLES` WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA = '{$database}' ORDER BY TABLE_NAME", [], $pdo);
    }


    public static function getTableDefs(string $database, PDO $pdo) : string
    {
        $output             = [];
        foreach ( static::getTables($database, $pdo) as $table )
        {
            $definition         = static::fetchOne("SHOW CREATE TABLE {$table->TABLE_NAME}", [], $pdo);
            $definition         = array_change_key_case((array)$definition, CASE_LOWER);
            $definition         = static::cleanSchema($definition['create table']);
            $definition         = preg_replace('/ AUTO_INCREMENT[ ]{0,1}=[ ]{0,1}[0-9]{1,10}/', '', $definition);
            $definition         = SqlFormatter::format($definition, false);
            $output[]           = <<<SQL

-- === {$table->TABLE_NAME} === --
    
DROP TABLE IF EXISTS {$table->TABLE_NAME};
    
{$definition};


SQL;
        }

        return implode("\n", $output);
    }


    public static function getFunctionsAndProcedures(string $database, PDO $pdo, bool $stripDefiner=true) : string
    {
        return static::getProcedureDefs($database, $pdo,  $stripDefiner) . static::getFunctionDefs($database, $pdo, $stripDefiner);
    }

}