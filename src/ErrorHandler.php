<?php declare(strict_types=1);

namespace Terah\Utils;

use Closure;
use ErrorException;
use Exception;
use Throwable;
use const E_COMPILE_ERROR;
use const E_CORE_ERROR;
use const E_ERROR;
use const E_RECOVERABLE_ERROR;
use const E_USER_ERROR;

/**
 * ErrorHandler
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to terry@terah.com.au so I can send you a copy immediately.
 */

/**
 * ErrorHandler library
 *
 * @author Terry Cullen <terry@terah.com.au>
 *
 */
class ErrorHandler
{
    protected static ?Closure $_errorHandler    = null;

    protected static ?Closure $_warningHandler  = null;

    protected static ?Closure $_signalHandler   = null;

    /** @var bool  */
    protected static bool $_isRegistered        = false;

    public static function handleFatalErrors(Throwable $e=null) : string
    {
        // Getting Last Error
        if ( is_null($e) )
        {
            $e                      = (object)error_get_last();
            if ( ! isset($e->type) || ! in_array($e->type, [E_ERROR, E_RECOVERABLE_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR]) )
            {
                return '';
            }
            $e                      = new ErrorException($e->message, 500, 0, $e->file, $e->line);
        }
        if ( static::$_errorHandler instanceof Closure )
        {
            return static::$_errorHandler->__invoke($e);
        }

        throw $e;
    }

    /**
     * @param Closure $signal
     * @return mixed
     */
    public static function handleSignal(Closure $signal)
    {
        if ( static::$_signalHandler instanceof Closure )
        {
            return static::$_signalHandler->__invoke($signal);
        }

        exit;
    }


    public static function registerErrorHandler(Closure $callback)
    {
        static::$_errorHandler  = $callback;
        if ( ! static::$_isRegistered )
        {
            register_shutdown_function('\Terah\Utils\ErrorHandler::handleFatalErrors');
            static::$_isRegistered  = true;
        }
    }


    public static function registerWarningHandler(Closure $callback)
    {
        static::$_warningHandler  = $callback;
        if ( ! static::$_isRegistered )
        {
            register_shutdown_function('\Terah\Utils\ErrorHandler::handleFatalErrors');
            static::$_isRegistered  = true;
        }
    }


    public static function registerSignalHandler(Closure $callback) : bool
    {
        if ( strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' )
        {
            return true;
        }
        if ( ! function_exists('pcntl_signal') )
        {
            if ( defined('STDERR') )
            {
                throw new Exception('The process control extension pcntl must be installed in cli mode');
            }

            return false;
        }
        declare(ticks=1);
        static::$_signalHandler = $callback;
        pcntl_signal(SIGINT,  '\Terah\Utils\ErrorHandler::handleSignal');
        pcntl_signal(SIGTERM, '\Terah\Utils\ErrorHandler::handleSignal');
        pcntl_signal(SIGHUP,  '\Terah\Utils\ErrorHandler::handleSignal');
        # Nothing for SIGKILL as it won't work and trying to will give you a warning.

        return true;
    }
}