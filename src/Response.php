<?php declare(strict_types=1);

namespace Terah\Utils;

use Closure;
use Exception;
use Prism\Helpers\Registry;
use Terah\ColourLog\LoggerTrait;
use Terah\View\ViewRendererInterface;
use Prism\Helpers\Registry as App;

/**
 * Class Response
 *
 * @package Terah\Utils
 */
class Response
{
    use LoggerTrait;

    const HTTP_CONTINUE = 100;
    const HTTP_SWITCHING_PROTOCOLS = 101;
    const HTTP_PROCESSING = 102;            // RFC2518
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NON_AUTHORITATIVE_INFORMATION = 203;
    const HTTP_NO_CONTENT = 204;
    const HTTP_RESET_CONTENT = 205;
    const HTTP_PARTIAL_CONTENT = 206;
    const HTTP_MULTI_STATUS = 207;          // RFC4918
    const HTTP_ALREADY_REPORTED = 208;      // RFC5842
    const HTTP_IM_USED = 226;               // RFC3229
    const HTTP_MULTIPLE_CHOICES = 300;
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_FOUND = 302;
    const HTTP_SEE_OTHER = 303;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_USE_PROXY = 305;
    const HTTP_RESERVED = 306;
    const HTTP_TEMPORARY_REDIRECT = 307;
    const HTTP_PERMANENTLY_REDIRECT = 308;  // RFC7238
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_PAYMENT_REQUIRED = 402;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;
    const HTTP_REQUEST_TIMEOUT = 408;
    const HTTP_CONFLICT = 409;
    const HTTP_GONE = 410;
    const HTTP_LENGTH_REQUIRED = 411;
    const HTTP_PRECONDITION_FAILED = 412;
    const HTTP_REQUEST_ENTITY_TOO_LARGE = 413;
    const HTTP_REQUEST_URI_TOO_LONG = 414;
    const HTTP_UNSUPPORTED_MEDIA_TYPE = 415;
    const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    const HTTP_EXPECTATION_FAILED = 417;
    const HTTP_I_AM_A_TEAPOT = 418;                                               // RFC2324
    const HTTP_UNPROCESSABLE_ENTITY = 422;                                        // RFC4918
    const HTTP_LOCKED = 423;                                                      // RFC4918
    const HTTP_FAILED_DEPENDENCY = 424;                                           // RFC4918
    const HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425;   // RFC2817
    const HTTP_UPGRADE_REQUIRED = 426;                                            // RFC2817
    const HTTP_PRECONDITION_REQUIRED = 428;                                       // RFC6585
    const HTTP_TOO_MANY_REQUESTS = 429;                                           // RFC6585
    const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;                             // RFC6585
    const HTTP_INTERNAL_SERVER_ERROR = 500;
    const HTTP_NOT_IMPLEMENTED = 501;
    const HTTP_BAD_GATEWAY = 502;
    const HTTP_SERVICE_UNAVAILABLE = 503;
    const HTTP_GATEWAY_TIMEOUT = 504;
    const HTTP_VERSION_NOT_SUPPORTED = 505;
    const HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL = 506;                        // RFC2295
    const HTTP_INSUFFICIENT_STORAGE = 507;                                        // RFC4918
    const HTTP_LOOP_DETECTED = 508;                                               // RFC5842
    const HTTP_NOT_EXTENDED = 510;                                                // RFC2774
    const HTTP_NETWORK_AUTHENTICATION_REQUIRED = 511;                             // RFC6585

    /**
     * @var array HTTP response codes and messages
     */
    protected static array $messages = [
        //Informational 1xx
        100 => '100 Continue',
        101 => '101 Switching Protocols',
        //Successful 2xx
        200 => '200 OK',
        201 => '201 Created',
        202 => '202 Accepted',
        203 => '203 Non-Authoritative Information',
        204 => '204 No Content',
        205 => '205 Reset Content',
        206 => '206 Partial Content',
        226 => '226 IM Used',
        //Redirection 3xx
        300 => '300 Multiple Choices',
        301 => '301 Moved Permanently',
        302 => '302 Found',
        303 => '303 See Other',
        304 => '304 Not Modified',
        305 => '305 Use Proxy',
        306 => '306 (Unused)',
        307 => '307 Temporary Redirect',
        //Client Error 4xx
        400 => '400 Bad Request',
        401 => '401 Unauthorised',
        402 => '402 Payment Required',
        403 => '403 Forbidden',
        404 => '404 Not Found',
        405 => '405 Method Not Allowed',
        406 => '406 Not Acceptable',
        407 => '407 Proxy Authentication Required',
        408 => '408 Request Timeout',
        409 => '409 Conflict',
        410 => '410 Gone',
        411 => '411 Length Required',
        412 => '412 Precondition Failed',
        413 => '413 Request Entity Too Large',
        414 => '414 Request-URI Too Long',
        415 => '415 Unsupported Media Type',
        416 => '416 Requested Range Not Satisfiable',
        417 => '417 Expectation Failed',
        418 => '418 I\'m a teapot',
        422 => '422 Unprocessable Entity',
        423 => '423 Locked',
        426 => '426 Upgrade Required',
        428 => '428 Precondition Required',
        429 => '429 Too Many Requests',
        431 => '431 Request Header Fields Too Large',
        //Server Error 5xx
        500 => '500 Internal Server Error',
        501 => '501 Not Implemented',
        502 => '502 Bad Gateway',
        503 => '503 Service Unavailable',
        504 => '504 Gateway Timeout',
        505 => '505 HTTP Version Not Supported',
        506 => '506 Variant Also Negotiates',
        510 => '510 Not Extended',
        511 => '511 Network Authentication Required'
    ];

    static protected string $_http_version = '1.1';

    protected Request $_request;

    protected array $_settings            = [
        'http_version'                  => '1.1',
    ];

    /** @var Closure[] */
    protected $_middleware          = [];


    public function __construct(Request $request, Result $meta, array $settings)
    {
        $this->_request     = $request;
        $this->_settings    = array_merge($this->_settings, $settings);
        static::$_http_version = ! empty($this->_settings['http_version']) ? $this->_settings['http_version'] : static::$_http_version;
    }


    public function middleware(string $name, Closure $fnMiddleware) : Response
    {
        $this->_middleware[$name] = $fnMiddleware;

        return $this;
    }


    public function runMiddleware(?Result $result=null) : Result
    {
        foreach ( $this->_middleware as $name => $fnCallback )
        {
            $result                 = $fnCallback->__invoke($this, $result);
        }

        return $result;
    }


    public function redirect(string $location)
    {
        header("Location: {$location}");

        exit;
    }


    public function respondByResult(Result $result)
    {
        $status             = 200;
        $format             = '';
        $redirect           = '';
        $attachment         = '';
        $fileName           = '';
        $template           = '';
        extract($result->toArray(false));
        $format             = $format ? $format : Registry::request()->getResponseType();
        try
        {
            $format             = App::isRegistered($format) ? $format : 'html';
            $result->format($format);
            $viewRenderer       = App::get($format);
            if ( ! $viewRenderer instanceof ViewRendererInterface )
            {
                $format             = 'html';
                $viewRenderer       = App::get($format);
            }
            $data               = $result->toString($viewRenderer, $this->_request);
        }
        catch ( Exception $e )
        {
            $this->logger->error($e->getMessage(), [$_SERVER]);
            throw $e;
        }
        $this->handleRedirect($format, $redirect, (string)$template);
        $result->method($this->_request->getMethod());
        $result->self($this->_request->getUrlWithQueryString());
        $result                 = $this->runMiddleware($result);
        $headers                = $this->getMetaHeaders($result);
        switch ( $format )
        {
            case '':
            case 'html':

                $headers['Content-Type']        = 'text/html;charset=utf-8';
                $data                           = $this->getErrorHtmlByStatusCode($status, $data);
                $this->_send($data, $status, $headers);

                exit;

            case 'pdf':

                $headers['Content-Type']        = 'application/pdf';
                $attachment                     = $attachment ?: 'attachment';
                $headers['Content-Disposition'] = "{$attachment};filename={$fileName}.pdf";
                $data                           = $this->getErrorHtmlByStatusCode($status, $data);
                $this->_send($data, $status, $headers);

                exit;

            case 'json':

                $headers['Content-Type']        = 'application/json;charset=utf-8';
                if ( $attachment )
                {
                    $headers['Content-Disposition'] = "{$attachment};filename={$fileName}.json";
                }
                $this->_send($data, $status, $headers);

                exit;

            case 'ics':

                $headers['Content-Type']        = 'Content-type: text/calendar; charset=utf-8';
                $this->_send($data, $status, $headers);

                exit;

            case 'xml':

                $headers['Content-Type']        = 'application/xml;charset=utf-8';
                $this->_send($data, $status, $headers);

                exit;

            case 'csv':

                $headers['Content-Type']        = 'application/csv';
                $attachment                     = $attachment ?: 'attachment';
                $headers['Content-Disposition'] = "{$attachment};filename={$fileName}.csv";
                $this->_send($data, $status, $headers);

                exit;

            case 'xlsx':
            case 'xls':

                $headers['Content-Type']        = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                $attachment                     = $attachment ?: 'attachment';
                $headers['Content-Disposition'] = "{$attachment};filename={$fileName}.xlsx";
                $this->_send($data, $status, $headers);

                exit;

            case 'txt':

                $headers['Content-Type']        = 'text/plain';
                $this->_send($data, $status, $headers);

                exit;

            case 'jpg':

                $headers['Content-Type']    = 'image/jpeg';
                $this->_send($data, $status, $headers);

                exit;

            case 'png':

                $headers['Content-Type']    = 'image/png';
                $this->_send($data, $status, $headers);

                exit;

            case 'gif':

                $headers['Content-Type']    = 'image/gif';
                $this->_send($data, $status, $headers);

                exit;

            case 'js':

                $headers['Content-Type']    = 'application/javascript';
                $this->_send($data, $status, $headers);

                exit;

            case 'css':

                $headers['Content-Type']    = 'text/css';
                $this->_send($data, $status, $headers);

                exit;

            default:

                throw new BadRequestHttpException("Unsupported format.");
        }
    }

    /**
     * @param Result $result
     * @return array
     */
    protected function getMetaHeaders(Result $result) : array
    {
        $metaData   = $result->toArray();
        $headers    = [];
        foreach ( $metaData as $header => $value )
        {
            $headers[$this->_settings['header_prefix'] . str_replace(' ', '-', ucwords(str_replace('_', ' ', $header)))] = json_encode($value);
        }

        return $headers;
    }


    /**
     * @param string $responseType
     * @param string $redirect
     * @param string $template
     */
    protected function handleRedirect(string $responseType, string $redirect, string $template)
    {
        if ( empty($redirect) )
        {
            return;
        }
        if ( in_array($responseType, ['json', 'xml']) )
        {
            return;
        }
        if ( in_array($responseType, ['html']) && $template === 'errors/redirect' )
        {
            return;
        }
        $this->redirect($redirect);
    }

    /**
     * @param int  $status
     * @param string $data
     * @return string
     */
    protected function getErrorHtmlByStatusCode(int $status=200, string $data='') : string
    {
        if ( ! empty($data) ||  $status < 400 )
        {
            return $data;
        }
        if ( $status === self::HTTP_NOT_FOUND )
        {
            return static::getNotFoundPage();
        }
        if ( $status === self::HTTP_UNAUTHORIZED )
        {
            return static::getNotAuthPage();
        }

        return static::getGeneralErrorPage($status);
    }

    /**
     * @param mixed $body
     * @param int $status
     * @param array $headers
     * @return void
     */
    protected function _send(string $body, int $status, array $headers)
    {
        //Send headers
        if ( ! $this->isCli() && headers_sent() === false )
        {
            static::sendStatusHeader($status);
            //Send headers
            foreach ( $headers as $name => $value )
            {
                $values = explode("\n", (string)$value);
                foreach ( $values as $header )
                {
                    header("{$name}: {$header}", false);
                }
            }
            if ( ! empty($headers['Content-Disposition']) )
            {
                setcookie('fileDownload', 'true', time()+3600, "/");
            }
        }
        //Send body, but only if it isn't a HEAD request
        if ( ! $this->_request->isHead() && $body )
        {
            echo $body;
        }
        $this->_exit($status);

        exit;
    }

    /**
     * @param int $status
     * @return void
     */
    protected function _exit(int $status=200)
    {
        exit($status < 400 ? 0 : 1);
    }

    /**
     * @param int $status
     */
    public function exit(int $status=200)
    {
        if ( $this->isCli() )
        {
            $this->_exit($status);
        }
        if ( headers_sent() === false )
        {
            static::sendStatusHeader($status);
        }
        $this->_exit($status);
    }

    /**
     * @param int $status
     * @return Response
     */
    public function sendHttpHeader(int $status) : Response
    {
        static::sendStatusHeader($status);

        return $this;
    }

    /**
     * @param int $status
     */
    public static function sendStatusHeader(int $status)
    {
        //Send status
        if ( strpos(PHP_SAPI, 'cgi') === 0 )
        {
            header(sprintf('Status: %s', static::getMessageForCode($status)));
        }
        else
        {
            header(sprintf('HTTP/%s %s', static::$_http_version, static::getMessageForCode($status)));
        }
    }

    /**
     * @param string $header
     * @param array $headers
     * @return bool
     */
    public static function sendHeader(string $header, array $headers) : bool
    {
        if ( ! array_key_exists($header, $headers) )
        {
            return true;
        }
        $values = explode("\n", $headers[$header]);
        foreach ( $values as $value )
        {
            header("{$header}: {$value}", false);
        }

        return true;
    }

    /**
     * Get message for HTTP status code
     * @param  int         $status
     * @return string
     */
    public static function getMessageForCode(int $status) : string
    {
        return isset(static::$messages[$status]) ? static::$messages[$status] : '';
    }

    /**
     * @return bool
     */
    public function isCli() : bool
    {
        return defined('STDERR');
    }

    /**
     * @return string
     */
    public static function getErrorPage() : string
    {
        return static::generateTemplateMarkup('Error', '<p>A website error has occurred. The website administrator has been notified of the issue. Sorry for the temporary inconvenience.</p>');
    }

    /**
     * @return string
     */
    public static function getNotFoundPage() : string
    {
        return static::generateTemplateMarkup('Page Not Found', '<p>The page you requested does not exist. Sorry for the temporary inconvenience.</p>');
    }

    /**
     * @return string
     */
    public static function getNotAuthPage() : string
    {
        //return static::generateTemplateMarkup('Not Authorised', '<p>You do not have the required authority to view the requested page. Sorry for the temporary inconvenience.</p>');
        return '';
    }

    /**
     * @param int $status
     * @return string
     */
    public static function getGeneralErrorPage(int $status) : string
    {
        return static::generateTemplateMarkup(static::getMessageForCode($status), '<p>A website error has occurred. The website administrator has been notified of the issue. Sorry for the temporary inconvenience.</p>');
    }

    /**
     * Generate diagnostic template markup
     *
     * This method accepts a title and body content to generate an HTML document layout.
     *
     * @param  string   $title  The title of the HTML template
     * @param  string   $body   The body content of the HTML template
     * @return string
     */
    protected static function generateTemplateMarkup(string $title, string $body) : string
    {
        return sprintf("<html><head><title>%s</title><style>body{margin:0;padding:30px;font:12px/1.5 Helvetica,Arial,Verdana,sans-serif;}h1{margin:0;font-size:48px;font-weight:normal;line-height:48px;}strong{display:inline-block;width:65px;}</style></head><body><h1>%s</h1>%s</body></html>", $title, $title, $body);
    }
}
