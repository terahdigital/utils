<?php declare(strict_types=1);

namespace Terah\Utils;

use Exception;
use Terah\Assert\Assert;


/**
 * Allows for multi-dimensional ini files.
 *
 * The native parse_ini_file() function will convert the following ini file:...
 *
 * [production]
 * localhost.database.host = 1.2.3.4
 * localhost.database.user = root
 * localhost.database.password = abcdef
 * debug.enabled = false
 *
 * [development : production]
 * localhost.database.host = localhost
 * debug.enabled = true
 *
 * ...into the following array:
 *
 * array
 *   'localhost.database.host' => 'localhost'
 *   'localhost.database.user' => 'root'
 *   'localhost.database.password' => 'abcdef'
 *   'debug.enabled' => 1
 *
 * This class allows you to convert the specified ini file into a multi-dimensional
 * array. In this case the structure generated will be:
 *
 * array
 *   'localhost' =>
 *     array
 *       'database' =>
 *         array
 *           'host' => 'localhost'
 *           'user' => 'root'
 *           'password' => 'abcdef'
 *   'debug' =>
 *     array
 *       'enabled' => 1
 *
 * As you can also see you can have sections that extend other sections (use ":" for that).
 * The extendable section must be defined BEFORE the extending section or otherwise
 * you will get an exception.
 *
 * Usage: $this->get('paths.webroot');
 *
 */
class IniConfig
{
    /**
     * Internal storage array
     */
    private array $_aConfigSet  = [];

    private array $_aKeyMap     = [];

    private string $_sIniFile   = '';

    /**
     * This is a filter based on something like bnedev01-prod
     *
     * @var string
     */
    private string $_sSectionName = '';

    private bool $_bInitialized = false;

    private ?string $_sRequestId = null;

    /**
     * IniConfig constructor.
     *
     * @param      $iniFile
     * @param      $appRoot
     * @param      $webRoot
     * @param null $websiteId
     * @param null $requestId
     * @throws NoSectionException
     * @throws NoSettingException
     * @throws NoWebsiteIdException
     */
    public function __construct(string $iniFile, string $appRoot, string $webRoot, $websiteId=null, $requestId=null)
    {
        $appRoot = str_replace(DIRECTORY_SEPARATOR, '/', $appRoot);
        $webRoot = str_replace(DIRECTORY_SEPARATOR, '/', $webRoot);
        // Set a 'one-true-root' for the app where everything can be included from...
        Assert::that($iniFile)->file("The config file {$iniFile} does not exist.");
        Assert::that($appRoot)->directory("The app root directory {$appRoot} does not exist.");
        Assert::that($webRoot)->directory("The web root directory {$webRoot} does not exist.");

        if ( ! defined('APP_ROOT') ) define('APP_ROOT', $appRoot);
        if ( ! defined('WEB_ROOT') ) define('WEB_ROOT', $webRoot);
        $this->parse($iniFile, true);
        $this->loadEnvironmentConf($websiteId);
        $this->setPhpIniSettings();
        $this->_bInitialized = true;
        $this->get('is.valid', 'validate_ini');
        $this->_sIniFile = $iniFile;
        $this->_sRequestId = $requestId ?: uniqid() . str_pad(rand(1,999), 3, '0', STR_PAD_LEFT);
    }

    public function requestId(): ?string
    {
        return $this->_sRequestId;
    }

    public function checkEnvironment(array $iniSettings, array $extensions, array $writableLocations)
    {
        foreach ( $iniSettings as $setting => $value )
        {
            if ( ini_get($setting) != $value )
            {
                throw new IncompleteConfigException("The php configuration value '{$setting}' needs to be {$value}");
            }
        }
        foreach ( $extensions as $extension )
        {
            if ( ! extension_loaded($extension) )
            {
                throw new IncompleteConfigException("The php extension '{$extension}' needs to be enabled");
            }
        }
        foreach ( $writableLocations as $writableLocation )
        {
            if ( ! is_writable($writableLocation) )
            {
                throw new IncompleteConfigException("The file location '{$writableLocation}' needs to be writable");
            }
        }
    }

    /**
     * Get your config variable from the default website config or a different one
     *
     * @param string $name
     * @param null|string $section
     * @param bool   $throwExceptions
     *
     * @throws NoSettingException
     * @return array|bool|int|null|string
     */
    public function get(string $name, ?string $section=null, bool $throwExceptions = true)
    {
        Assert::that($name)->string()->notEmpty('The config key name can not be empty.');
        // Choose a global config or a website one
        $section = ! is_null($section) ? $section : $this->_sSectionName;
        return isset( $this->_aKeyMap[$section][$name] )
            ? $this->_aKeyMap[$section][$name]
            : $this->_get($name, $section, $throwExceptions);
    }

    public function getFlattened($name, $section = null, $throwExceptions = true): array
    {
        return $this->flattenKeys($this->get($name, $section, $throwExceptions));
    }

    public function write($path, $value)
    {
        $this->_aKeyMap[$this->_sSectionName][$path] = $value;
    }

    protected function _throwMissingConfigIfNeeded($throwExceptions, $name): bool
    {
        Assert::that(! $throwExceptions)->true("The config key ({$name}) does not exist.");
        return true;
    }

    protected function _get(string $name, $section, $throwExceptions)
    {
        $k      = explode('.', $name);
        $depth = count($k);
        Assert::that($depth < 6)->true("The config engine doesn't support key depth greater than five.");
        switch ( $depth )
        {
            case 1:
                if ( ! isset( $this->_aConfigSet[$section][$k[0]] ) )
                {
                    return $this->_throwMissingConfigIfNeeded($throwExceptions, $name);
                }
                $this->_aConfigSet[$section][$k[0]]
                    = $this->_postProcessValues($name, $section, $this->_aConfigSet[$section][$k[0]]);
                $this->_aKeyMap[$section][$name] = & $this->_aConfigSet[$section][$k[0]];
                return $this->_aKeyMap[$section][$name];
            case 2:
                if ( ! isset( $this->_aConfigSet[$section][$k[0]][$k[1]] ) )
                {
                    return $this->_throwMissingConfigIfNeeded($throwExceptions, $name);
                }
                $this->_aConfigSet[$section][$k[0]][$k[1]]
                    = $this->_postProcessValues($name, $section, $this->_aConfigSet[$section][$k[0]][$k[1]]);
                $this->_aKeyMap[$section][$name] = & $this->_aConfigSet[$section][$k[0]][$k[1]];
                return $this->_aKeyMap[$section][$name];
            case 3:
                if ( ! isset( $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]] ) )
                {
                    return $this->_throwMissingConfigIfNeeded($throwExceptions, $name);
                }
                $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]]
                    = $this->_postProcessValues($name, $section, $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]]);
                $this->_aKeyMap[$section][$name] = & $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]];
                return $this->_aKeyMap[$section][$name];
            case 4:
                if ( ! isset( $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]][$k[3]] ) )
                {
                    return $this->_throwMissingConfigIfNeeded($throwExceptions, $name);
                }
                $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]][$k[3]]
                    = $this->_postProcessValues($name, $section, $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]][$k[3]]);
                $this->_aKeyMap[$section][$name] = & $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]][$k[3]];
                return $this->_aKeyMap[$section][$name];
            case 5:
                if ( ! isset( $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]][$k[3]][$k[4]] ) )
                {
                    return $this->_throwMissingConfigIfNeeded($throwExceptions, $name);
                }
                $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]][$k[3]][$k[4]]
                    = $this->_postProcessValues($name, $section, $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]][$k[3]][$k[4]]);
                $this->_aKeyMap[$section][$name] = & $this->_aConfigSet[$section][$k[0]][$k[1]][$k[2]][$k[3]][$k[4]];
                return $this->_aKeyMap[$section][$name];
        }
        if ( $throwExceptions )
        {
            throw new NoSettingException( "The config key ({$name}) does not exist." );
        }
        return null;
    }

    public function getClassPath($name)
    {
        return $this->get($name, 'class-map', false);
    }

    public function getArray($name, $section = null, $throwExceptions = true)
    {
        return explode('|', $this->get($name, $section, $throwExceptions));
    }

    public function env($key)
    {
        if ( $key === 'HTTPS' )
        {
            if ( isset( $_SERVER['HTTPS'] ) )
            {
                return ( ! empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] !== 'off' );
            }
            return ( strpos($this->env('SCRIPT_URI'), 'https://') === 0 );
        }
        if ( $key === 'SCRIPT_NAME' )
        {
            if ( $this->env('CGI_MODE') && isset( $_ENV['SCRIPT_URL'] ) )
            {
                $key = 'SCRIPT_URL';
            }
        }
        $val = null;
        if ( isset( $_SERVER[$key] ) )
        {
            $val = $_SERVER[$key];
        }
        elseif ( isset( $_ENV[$key] ) )
        {
            $val = $_ENV[$key];
        }
        elseif ( getenv($key) !== false )
        {
            $val = getenv($key);
        }
        if ( $key === 'REMOTE_ADDR' && $val === $this->env('SERVER_ADDR') )
        {
            $addr = $this->env('HTTP_PC_REMOTE_ADDR');
            if ( $addr !== null )
            {
                $val = $addr;
            }
        }
        if ( $val !== null )
        {
            return $val;
        }
        switch ( $key )
        {
            case 'DOCUMENT_ROOT':
                $name     = $this->env('SCRIPT_NAME');
                $filename = $this->env('SCRIPT_FILENAME');
                $offset   = 0;
                if ( ! strpos($name, '.php') )
                {
                    $offset = 4;
                }
                return substr($filename, 0, - ( strlen($name) + $offset ));
            case 'PHP_SELF':
                return str_replace($this->env('DOCUMENT_ROOT'), '', $this->env('SCRIPT_FILENAME'));
            case 'CGI_MODE':
                return ( PHP_SAPI === 'cgi' );
            case 'HTTP_BASE':
                $host  = $this->env('HTTP_HOST');
                $parts = explode('.', $host);
                $count = count($parts);
                if ( $count === 1 )
                {
                    return '.' . $host;
                }
                elseif ( $count === 2 )
                {
                    return '.' . $host;
                }
                elseif ( $count === 3 )
                {
                    $gTLD = [
                        'aero',
                        'asia',
                        'biz',
                        'cat',
                        'com',
                        'coop',
                        'edu',
                        'gov',
                        'info',
                        'int',
                        'jobs',
                        'mil',
                        'mobi',
                        'museum',
                        'name',
                        'net',
                        'org',
                        'pro',
                        'tel',
                        'travel',
                        'xxx'
                    ];
                    if ( in_array($parts[1], $gTLD) )
                    {
                        return '.' . $host;
                    }
                }
                array_shift($parts);
                return '.' . implode('.', $parts);
        }
        return null;
    }

    protected function _decryptionEnabled(?string $key=null, ?string $section=null): bool
    {
        return false;
//        if ( ! $this->_bInitialized )
//        {
//            return false;
//        }
//        // Added section exclusions to the array below
//        if ( ! is_null($section) && in_array($section, ['class-map']) )
//        {
//            return false;
//        }
//        // Added key exclusions to the array below
//        $regex = '/(' . implode('|', ['encryption']) . ')/';
//        if ( ! is_null($key) && preg_match($regex, $key) )
//        {
//            return false;
//        }
//        return true;
    }

    protected function _postProcessValues($name, $section, $values)
    {
        $values = $this->_decryptRecursive($name, $section, $values);
        return $this->_varReplacementRecursive($values);
    }

    protected function _varReplacementRecursive($values)
    {
        if ( is_array($values) )
        {
            foreach ( $values as $idx => $value )
            {
                $values[$idx] = $this->_varReplacementRecursive($value);
            }
            return $values;
        }
        if ( $values === "true" || $values === "false" )
        {
            return $values === "true";
        }
        if ( ! is_string($values) || mb_strpos($values, '{{') === false )
        {
            return $values;
        }
        $key   = StringUtils::between('{{', '}}', $values);
        $count = 0;
        while ( $key && $count < 10 )
        {
            $count ++;
            $replace = $this->get($key);
            $values  = str_replace('{{' . $key . '}}', $replace, $values);
            $key     = StringUtils::between('{{', '}}', $values);
        }
        return $values;
    }

    protected function _decryptRecursive($name, $section, $values)
    {
        if ( ! $this->_decryptionEnabled($name, $section) )
        {
            return $values;
        }
        if ( is_array($values) )
        {
            foreach ( $values as $idx => $value )
            {
                $values[$idx] = $this->_decryptRecursive($name, $section, $value);
            }
            return $values;
        }
        return is_string($values) ? Encryption::decrypt($values) : $values;
    }

    public function isProd(): bool
    {
        return $this->get('app.mode') === 'prod';
    }

    public function isDev(): bool
    {
        return $this->get('app.mode') === 'dev';
    }

    /**
     * This just sets the website config block based on the website identifier
     *
     * @param null|string $websiteId
     *
     * @throws NoWebsiteIdException
     */
    public function loadEnvironmentConf(?string $websiteId = null)
    {
        $websiteId              = ! is_null($websiteId) ? $websiteId : $this->getEnvironmentIdentifier();
        if ( empty( $websiteId ) )
        {
            throw new NoWebsiteIdException( "Could not determine valid environment variable.  Please add appenv=dev|test|stage|prod to your CLI scripts or SetEnv appenv dev|test|stage|prod to your apache virtual host container." );
        }
        if ( ! $this->sectionExists($websiteId) )
        {
            throw new NoWebsiteIdException( "Could not find the {$websiteId} section of the config file.  Please check the environment is in the config file.");
        }
        $this->setSection($websiteId);
    }

    public function getEnvironmentIdentifier() : string
    {
        $websiteEnv = $this->get('config.environment.var', 'prod');
        if ( empty( $websiteEnv ) )
        {
            throw new NoWebsiteIdException( "Could not determine valid website environment variable name." );
        }
        $websiteId = getenv($websiteEnv);
        if ( ! $websiteId )
        {
            // Try via the cli
            extract(CommandLineParser::parseArgs(), EXTR_PREFIX_ALL, 'opts');
            $websiteId = ! empty( $opts_w ) ? $opts_w : ( ! empty( $opts_website ) ? $opts_website : false );
        }
        // TODO -c Config: Add more functionality here to find the website id from sources other than env vars.
        return $websiteId;
    }

    public function sectionExists(string $name): bool
    {
        $validSections = array_keys($this->_aConfigSet);
        return in_array($name, $validSections);
    }

    public function setSection(string $name)
    {
        if ( ! $this->sectionExists($name) )
        {
            throw new NoSectionException( "Configuration section ({$name}) does not exist." );
        }
        $this->_sSectionName = $name;
    }

    public function getSection(string $name)
    {
        return isset( $this->_aConfigSet[$name] ) ? $this->_aConfigSet[$name] : null;
    }

    public function setPhpIniSettings($settings = null)
    {
        $settings = is_array($settings) ? $settings : $this->flattenKeys($this->get('php_ini.ini'));
        foreach ( $settings as $setting => $value )
        {
            if ( $setting === 'date.timezone' )
            {
                date_default_timezone_set($value);
                continue;
            }
            ini_set($setting, $value);
//            if ( false === ini_set($setting, $value) )
//            {
//                throw new \Exception( "Could not set ini setting ({$setting}) to value ({$value})" );
//            }
        }
    }

    public function flattenKeys($nested, $flat = [], $parents = []): array
    {
        foreach ( $nested as $key => $value )
        {
            $this_key = array_merge($parents, [$key]);
            if ( is_array($value) )
            {
                $flat = $this->flattenKeys($value, $flat, $this_key);
                continue;
            }
            $flat[implode('.', $this_key)] = $value;
        }
        return $flat;
    }

    /**
     * Loads in the ini file specified in filename, and returns the settings in
     * it as an associative multi-dimensional array
     *
     * @param string  $filename        The filename of the ini file being parsed
     * @param boolean $processSections By setting the process_sections parameter to TRUE,
     *                                  you get a multidimensional array, with the section
     *                                  names and settings included. The default for
     *                                  process_sections is FALSE
     * @param null|string  $section_name    Specific section name to extract upon processing
     *
     * @return array|bool
     * @throws NoSectionException
     */
    public function parse(string $filename, bool $processSections = false, ?string $section_name = null)
    {
        // load the raw ini file
        $ini = parse_ini_file($filename, $processSections);
        // fail if there was an error while processing the specified ini file
        if ( $ini === false )
        {
            return false;
        }
        // reset the result array
        $this->_aConfigSet = [];
        if ( $processSections === true )
        {
            // loop through each section
            foreach ( $ini as $section => $contents )
            {
                // process sections contents
                $this->_processSection($section, $contents);
            }
        }
        else
        {
            // treat the whole ini file as a single section
            $this->_aConfigSet = $this->_processSectionContents($ini);
        }
        //  extract the required section if required
        if ( $processSections === true )
        {
            if ( $section_name !== null )
            {
                // return the specified section contents if it exists
                if ( isset( $this->_aConfigSet[$section_name] ) )
                {
                    return $this->_aConfigSet[$section_name];
                }
                else
                {
                    throw new NoSectionException( 'Section ' . $section_name . ' not found in the ini file' );
                }
            }
        }
        // if no specific section is required, just return the whole result
        return $this->_aConfigSet;
    }

    // Process contents of the specified section
    private function _processSection(string $section, array $contents) : void
    {
        // the section does not extend another section
        if ( mb_stripos($section, ':') === false )
        {
            $this->_aConfigSet[$section] = $this->_processSectionContents($contents);
            // section extends another section
        }
        else
        {
            // extract section names
            list( $extTarget, $extSource ) = explode(':', $section);
            $extTarget = trim($extTarget);
            $extSource = trim($extSource);
            // check if the extended section exists
            if ( ! isset( $this->_aConfigSet[$extSource] ) )
            {
                throw new Exception( 'Unable to extend section ' . $extSource . ', section not found' );
            }
            // process section contents
            $this->_aConfigSet[$extTarget] = $this->_processSectionContents($contents);
            // merge the new section with the existing section values
            $this->_aConfigSet[$extTarget] = $this->_arrayMergeRecursive($this->_aConfigSet[$extSource], $this->_aConfigSet[$extTarget]);
        }
    }

    /**
     * Process contents of a section
     *
     * @param array $contents Section contents
     *
     * @return array
     */
    private function _processSectionContents(array $contents) : array
    {
        $result                 = [];
        // loop through each line and convert it to an array
        foreach ( $contents as $path => $value )
        {
            // convert all a.b.c.d to multi-dimensional arrays
            $process = $this->_processContentEntry($path, $value);
            // merge the current line with all previous ones
            $result = $this->_arrayMergeRecursive($result, $process);
        }

        return $result;
    }

    /**
     * Convert a.b.c.d paths to multi-dimensional arrays
     *
     * @param string $path  Current ini file's line's key
     * @param mixed  $value Current ini file's line's value
     *
     * @return array
     */
    private function _processContentEntry(string $path, $value) : array
    {
        $position               = mb_strpos($path, '.');
        if ( $position === false )
        {
            return [$path => $value];
        }
        $key                    = mb_substr($path, 0, $position);
        $path                   = mb_substr($path, $position + 1);

        return [
            $key                    => $this->_processContentEntry($path, $value),
        ];
    }

    /**
     * Merge two arrays recursively overwriting the keys in the first array
     * if such key already exists
     *
     * @param mixed $a Left array to merge right array into
     * @param mixed $b Right array to merge over the left array
     *
     * @return mixed
     */
    private function _arrayMergeRecursive($a, $b)
    {
        // merge arrays if both variables are arrays
        if ( is_array($a) && is_array($b) )
        {
            // loop through each right array's entry and merge it into $a
            foreach ( $b as $key => $value )
            {
                if ( isset( $a[$key] ) )
                {
                    $a[$key] = $this->_arrayMergeRecursive($a[$key], $value);
                }
                else
                {
                    if ( $key === 0 )
                    {
                        $a = [0 => $this->_arrayMergeRecursive($a, $value)];
                    }
                    else
                    {
                        $a[$key] = $value;
                    }
                }
            }
        }
        else
        {
            // one of values is not an array
            $a = $b;
        }
        return $a;
    }

    public function outputFormattedIniFile()
    {
        $fileData = file($this->_sIniFile);
        foreach ( $fileData as $data )
        {
            echo strpos($data, '=') === false ? $data : $this->_formatIniLine($data, 50);
        }
    }

    protected function _formatIniLine($line, $numPadding): string
    {
        $key = StringUtils::before('=', $line);
        $val = StringUtils::after('=', $line);
        return str_pad(trim($key), $numPadding) . '= ' . trim($val) . "\n";
    }
}

class NoSectionException extends Exception
{
}

class NoSettingException extends Exception
{
}

class NoWebsiteIdException extends Exception
{
}

class IncompleteConfigException extends Exception {

}
