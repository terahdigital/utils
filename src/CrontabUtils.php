<?php declare(strict_types=1);


namespace Terah\Utils;

use Cron\CronExpression;
use Exception;
use Liliumdev\ICalendar\ZCiCal;
use Liliumdev\ICalendar\ZCiCalDataNode;
use Liliumdev\ICalendar\ZCiCalNode;
use stdClass;
use Terah\Assert\Assert;

class CrontabUtils
{
    /**
     * @param string $cronFile
     * @return string
     */
    public static function crontabToIcs(string $cronFile) : string
    {
        $start                  = strtotime('1 week ago');
        $crontab                = file($cronFile);
        $crontab                = ArrayUtils::filterEmpty($crontab, true);
        $crontab                = ArrayUtils::trim($crontab);
        $crontab                = preg_grep('/^[0-9*]/', $crontab);
        $calendar               = new ZCiCal();
        foreach ( $crontab as $line )
        {
            if ( ! preg_match_all('/^(?<min>[0-9*\/,-]+)[ ]+(?<hour>[0-9*\/,-]+)[ ]+(?<day>[0-9*\/,-]+)[ ]+(?<month>[0-9*\/,-]+)[ ]+(?<dow>[0-9*\/,-]+)/i', $line, $matches, PREG_SET_ORDER) )
            {
                throw new Exception('Bad line in crontab');
            }
            $matches                = (object)array_filter($matches[0] ?? [], "is_string", ARRAY_FILTER_USE_KEY);
            $matches->line          = "{$matches->min} {$matches->hour} {$matches->day} {$matches->month} {$matches->dow}";
            $rrule                  = static::getRRuleFromCron($matches);
            $lastRunDate            = (int)CronExpression::factory($matches->line)->getPreviousRunDate()->format('U');
            $entry                  = (object)[
                'start'                 => date('Ymd\THis', $lastRunDate),
                'end'                   => date('Ymd\THis', $lastRunDate + 30),
                'rrule'                 => $rrule,
                'desc'                  => $line,
                'job'                   => static::getTaskFromLine($line),
            ];
            $event                  = new ZCiCalNode("VEVENT", $calendar->curnode);
            // add title
            $event->addNode(new ZCiCalDataNode("SUMMARY: {$entry->job}"));
            // add start date
            $event->addNode(new ZCiCalDataNode("DTSTART:{$entry->start}"));
            // add end date
            if ( $entry->end )
            {
                $event->addNode(new ZCiCalDataNode("DTEND:{$entry->end}"));
            }
            // UID is a required item in VEVENT, create unique string for this event
            // Adding your domain to the end is a good way of creating uniqueness
            $uid                    = md5(json_encode($entry));
            $event->addNode(new ZCiCalDataNode("UID:{$uid}"));
            // DTSTAMP is a required item in VEVENT
            $event->addNode(new ZCiCalDataNode("DTSTAMP:" . date('Ymd\THis')));
            // Add description
            $event->addNode(new ZCiCalDataNode("DESCRIPTION: " . ZCiCal::formatContent($entry->desc)));
            // Add RRULE
            $event->addNode(new ZCiCalDataNode("RRULE:{$entry->rrule}"));
        }

        return $calendar->export();
    }

    /**
     * @param string $line
     * @return string
     */
    public static function getTaskFromLine(string $line) : string
    {
        $command                = StringUtils::after('./prism', $line);
        if ( $command )
        {
            $command                = StringUtils::before('-', $command, true);
            $command                = StringUtils::before('>', $command, true);

            return trim($command);
        }
        $command                = StringUtils::after('prism.php', $line);
        if ( $command )
        {
            $command                = StringUtils::before('-', $command, true);
            $command                = StringUtils::before('>', $command, true);

            return trim($command);
        }
        $command                = StringUtils::after('./', $line);
        if ( $command )
        {
            $command                = StringUtils::before('-', $command, true);
            $command                = StringUtils::before('>', $command, true);

            return trim($command);
        }


        return $line;
    }

    /**
     * @param stdClass $cron
     * @return string
     */
    public static function getRRuleFromCron(stdClass $cron) : string
    {
        $data                   = [
            static::getFrequency($cron),
            static::getInterval($cron),
            static::getByTypes($cron),
        ];
        $data                   = ArrayUtils::filterEmpty($data);

        return implode(';', $data);
    }

    /**
     * @param stdClass $cron
     * @return string
     */
    protected static function getFrequency(stdClass $cron) : string
    {
        $map                    = [
            'min'                   => 'MINUTELY',  // minute (0 - 59)
            'hour'                  => 'HOURLY',    // hour (0 - 23)
            'day'                   => 'DAILY',     // day of month (1 - 31)
            'month'                 => 'MONTHLY',   // month (1 - 12)
            //'dow'                   => '',        // day of week (0 - 6) (Sunday=0)
        ];
        foreach ( $map as $field => $rule )
        {
            if ( preg_match('/^\*(|\/[0-9]{1,3})$/', $cron->{$field}) ) // * or */5
            {
                return "FREQ={$rule}";
            }
        }

        return '';
    }

    /**
     * @param stdClass $cron
     * @return string
     */
    protected static function getInterval(stdClass $cron) : string
    {
        $map                    = [
            'min'                   => 'MINUTELY',  // minute (0 - 59)
            'hour'                  => 'HOURLY',    // hour (0 - 23)
            'day'                   => 'DAILY',     // day of month (1 - 31)
            'month'                 => 'MONTHLY',   // month (1 - 12)
            // 'dow'                   => '',       // day of week (0 - 6) (Sunday=0)
        ];
        foreach ( $map as $field => $rule )
        {
            if ( preg_match_all('/^\*\/(?<int>[0-9]{1,3})$/', $cron->{$field}, $matches, PREG_SET_ORDER) ) // */5
            {
                $matches                = (object)array_filter($matches[0] ?? [], "is_string", ARRAY_FILTER_USE_KEY);

                return "INTERVAL={$matches->int}";
            }
        }

        return '';
    }

    /**
     * @param stdClass $cron
     * @return string
     */
    protected static function getByTypes(stdClass $cron) : string
    {
        $rules                  = [];
        $dowMap                 = [
            '0'                     => 'SU',
            '1'                     => 'MO',
            '2'                     => 'TU',
            '3'                     => 'WE',
            '4'                     => 'TH',
            '5'                     => 'FR',
            '6'                     => 'SA'
        ];
        $map                    = [
            'min'                   => 'BYMINUTE',      // minute (0 - 59)
            'hour'                  => 'BYHOUR',        // hour (0 - 23)
            'day'                   => 'BYMONTHDAY',    // day of month (1 - 31)
            'month'                 => 'BYMONTH',       // month (1 - 12)
            'dow'                   => 'BYDAY',         // day of week (0 - 6) (Sunday=0)
        ];
        foreach ( $map as $field => $rule )
        {
            $values                     = explode(',', $cron->{$field});
            if ( preg_match_all('/^(?<start>[0-9]{1,3})-(?<end>[0-9]{1,3})$/', $cron->{$field}, $dates, PREG_SET_ORDER) ) // 3-12
            {
                $dates                      = (object)array_filter($dates[0] ?? [], "is_string", ARRAY_FILTER_USE_KEY);
                $dates->start               = (int)$dates->start;
                $dates->end                 = (int)$dates->end;
                if ( $dates->start >= $dates->end )
                {
                    throw new Exception("Start must be less then end in range");
                }
                $values                     = [];
                $values[]                   = $dates->start;
                while ( $dates->start < $dates->end )
                {
                    $dates->start++;
                    $values[]                   = $dates->start;
                }
            }
            if ( count($values) < 2 )
            {
                continue;
            }
            if ( $field === 'dow' )
            {
                foreach ( $values as $idx => $dayNum )
                {
                    $dayNum                 = (int)$dayNum;
                    $dayNum                 = $dayNum === 7 ? 0 : $dayNum;
                    if ( isset($dowMap[$dayNum]) )
                    {
                        $values[$idx]           = $dowMap[$dayNum];
                    }
                }
            }
            $values                 = implode(',', $values);
            $rules[]                = "{$rule}={$values}";
        }

        return implode(';', $rules);
    }
}
