<?php declare(strict_types=1);

namespace Terah\Utils;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Prism\Helpers\EventLog;
use Prism\Models\DB;
use Prism\Helpers\Registry as App;
use stdClass;
use Terah\Assert\Assert;

/**
 * Class Email
 *
 * @package Terah\Utils
 */
class Email {

    const EMAIL_TO_TO                   = 'TO';
    const EMAIL_TO_CC                   = 'CC';
    const EMAIL_TO_BCC                  = 'BCC';

    protected array $_to                = [
        self::EMAIL_TO_TO                   => [],
        self::EMAIL_TO_CC                   => [],
        self::EMAIL_TO_BCC                  => [],
    ];
    protected string $_fromEmail            = '';
    protected ?string $_fromName            = null;
    protected string $_replyEmail           = '';
    protected ?string $_replyName           = null;
    protected string $_subject              = '';
    protected string $_htmlBody             = '';
    protected string $_txtBody              = '';
    protected ?string $_attachmentPath      = null;
    protected ?string $_sendAfterDate       = null;
    protected string $_type                 = '';
    protected ?int $_client_id              = null;
    protected ?int $_account_id             = null;

    /**
     * Email constructor.
     */
    public function __construct()
    {
        $conf   = App::config()->getObj('email.its_accounts');
        $this->replyTo($conf->reply_to, $conf->reply_name);
        $this->from($conf->from_email, $conf->from_name);
    }


    public function from(string $email, ?string $name=null) : Email
    {
        Assert::that($email)->email();
        $this->_fromEmail       = $email;
        $this->_fromName        = $name;

        return $this;
    }


    public function replyTo(string $email, ?string $name=null) : Email
    {
        Assert::that($email)->email();
        $this->_replyEmail      = $email;
        $this->_replyName       = $name;

        return $this;
    }


    public function to(string $email, ?string $name=null, string $type='TO') : Email
    {
        Assert::that($email)->email();
        Assert::that($type)->inArray(array_keys($this->_to));
        $this->_to[$type][]     = [$email, $name];
        $this->_makeToEmailsUnique();

        return $this;
    }


    public function clientId(int $clientId) : Email
    {
        Assert::that($clientId)->nullOr()->id();
        $this->_client_id       = $clientId;

        return $this;
    }


    public function accountId(int $accountId) : Email
    {
        Assert::that($accountId)->nullOr()->id();
        $this->_account_id      = $accountId;

        return $this;
    }


    protected function _makeToEmailsUnique()
    {
        $reference              = [];
        foreach ( $this->_to as $type => $emailGroup )
        {
            foreach ( $emailGroup as $idx => $emailSet )
            {
                if ( in_array($emailSet[0], $reference) )
                {
                    unset($this->_to[$type][$idx]);
                    continue;
                }
                $reference[]            = $emailSet[0];
            }
        }
    }


    public function automaticTos(int $orgUnitId, string $emailTypeIdent='') : Email
    {
        // todo : Merge in defaults and remove user to if needed

        return $this;
    }


    public function type(string $type) : Email
    {
        Assert::that($type)->notEmpty();
        $this->_type = $type;
        return $this;
    }


    public function populateTemplate(string $templateName, array $templateVars, $layout='email') : Email
    {
        $this->type($templateName);
        $htmlTemplatePath  = "email/{$templateName}";
        $txtTemplatePath   = "email/{$templateName}";
        Assert::that($templateVars)->notEmpty('Template variables has not been specified');
        Assert::that($layout)->notEmpty('The template layout has not been specified');
        $this->_htmlBody    = App::html()->renderWithLayout($templateVars, ['tmpl' => $htmlTemplatePath, 'layout' => $layout]);
        $this->_txtBody     = App::html()->renderWithLayout($templateVars, ['tmpl' => $txtTemplatePath, 'layout' => $layout, 'ext' => 'txt']);

        return $this;
    }


    public function setTextBody(string $textBody) : Email
    {
        Assert::that($textBody)->notEmpty('The text body has not been specified')->string();
        $this->_htmlBody   = $textBody;
        $this->_txtBody    = $textBody;

        return $this;
    }


    public function subject(string $subject) : Email
    {
        Assert::that($subject)->string()->notEmpty();

        $this->_subject         = $subject;

        return $this;
    }


    public function attachmentPath(string $attachmentPath) : Email
    {
        Assert::that($attachmentPath)->file();
        $this->_attachmentPath  = $attachmentPath;

        return $this;
    }


    public function sendAfter(string $dtSendAfter) : Email
    {
        $this->_sendAfterDate   = Date::dateTime($dtSendAfter);

        return $this;
    }


    public function send(string $dtSendAfter='') : bool
    {
        if ( ! empty($dtSendAfter) )
        {
            $this->sendAfter($dtSendAfter);
        }
        $this->_validateEmail();
        $this->sendAfter($this->_sendAfterDate);
        $message                = DB::Message()->insert((object)[
            'subject'               => $this->_subject,
            'html_body'             => $this->_htmlBody,
            'txt_body'              => $this->_txtBody,
            'sent_ts'               => null,
            'send_after_ts'         => $this->_sendAfterDate,
            'attachment_path'       => $this->_attachmentPath,
            'from_nm'               => $this->_fromName,
            'from_email'            => $this->_fromEmail,
            'reply_nm'              => $this->_replyName,
            'reply_email'           => $this->_replyEmail,
            'type'                  => $this->_type,
        ]);
        foreach ( $this->_to as $type => $recipients )
        {
            foreach ( $recipients as $recipient )
            {
                $email                  = is_array($recipient) ? $recipient[0] : $recipient;
                $name                   = is_array($recipient) ? $recipient[1] : null;
                DB::Recipient()->insert((object)[
                    'name'               => $name,
                    'type'               => $type,
                    'message_id'         => $message->id,
                    'email'              => $email,
                ]);
            }
        }

        return true;
    }


    protected function _validateEmail()
    {
        $allRecipients          = array_merge($this->_to[self::EMAIL_TO_TO], $this->_to[self::EMAIL_TO_CC], $this->_to[self::EMAIL_TO_BCC]);
        Assert::that($allRecipients)->notEmpty('Please specify at least one recipient');
        Assert::that($this->_subject)->notEmpty('The subject cannot be empty');
        Assert::that($this->_txtBody)->notEmpty('The text message body cannot be empty');
        Assert::that($this->_htmlBody)->notEmpty('The html message body cannot be empty');
        Assert::that($this->_fromEmail)->notEmpty('The from email cannot be empty');
        Assert::that($this->_fromName)->string('The from name has to be a string');
        Assert::that($this->_replyEmail)->notEmpty('The reply-to email cannot be empty');
        Assert::that($this->_replyName)->string('The reply-to name has to be a string');
        Assert::that($this->_attachmentPath)->nullOr()->file();
    }


    public function sendNow(bool $forceSendInDev=false) : bool
    {
        $this->_validateEmail();
        // Marshal the message into the same format as the db result
        $message                = (object)[
            'subject'               => $this->_subject,
            'html_body'             => $this->_htmlBody,
            'txt_body'              => $this->_txtBody,
            'sent_ts'               => null,
            'send_after_ts'         => $this->_sendAfterDate,
            'attachment_path'       => $this->_attachmentPath,
            'from_nm'               => $this->_fromName,
            'from_email'            => $this->_fromEmail,
            'reply_nm'              => $this->_replyName,
            'reply_email'           => $this->_replyEmail,
            'type'                  => $this->_type,
            'Recipients'            => [],
        ];
        foreach ( $this->_to as $type => $recipients )
        {
            foreach ( $recipients as $recipient )
            {
                $message->Recipients[] = (object)[
                    'name'                  => is_array($recipient) ? $recipient[1] : null,
                    'type'                  => $type,
                    'email'                 => is_array($recipient) ? $recipient[0] : $recipient,
                ];
            }
        }

        return static::_sendEmail($message, $forceSendInDev);
    }


    public static function sendUnsentMessages(string $dtSentAfter=null, bool $forceSendInDev=false) : bool
    {
        $dtSentAfter            = Date::dateTime($dtSentAfter);
        $messages               = DB::Message()->getUnsentMessages($dtSentAfter);
        foreach ( $messages as $message )
        {
            if ( static::_sendEmail($message, $forceSendInDev) )
            {
                DB::Message()->updateField('sent_ts', Date::dateTime(), $message->id);
            }
        }
        return true;
    }


    protected static function _sendEmail(stdClass $message, bool $forceSendInDev=false) : bool
    {
        if ( ! static::_canSend($message, $forceSendInDev) )
        {
            return false;
        }
        $mailer                 = new PHPMailer();
        //$mailer->SMTPDebug = 3;                               // Enable verbose debug output
        //$mailer->isSMTP();                                      // Set mailer to use SMTP
        //$mailer->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
        //$mailer->SMTPAuth = true;                               // Enable SMTP authentication
        //$mailer->Username = 'user@example.com';                 // SMTP username
        //$mailer->Password = 'secret';                           // SMTP password
        //$mailer->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        //$mailer->Port = 587;                                    // TCP port to connect to

        $mailer->setFrom($message->from_email, (string)$message->from_nm);

        $toList                 = [];
        foreach ( $message->Recipients as $recipient )
        {
            Assert::that($recipient->type)->inArray([self::EMAIL_TO_TO, self::EMAIL_TO_CC, self::EMAIL_TO_BCC]);
            $toList[] = $recipient->email;
            switch($recipient->type)
            {
                case self::EMAIL_TO_TO:
                    $mailer->addAddress($recipient->email, $recipient->name);
                    break;
                case self::EMAIL_TO_CC:
                    $mailer->addCC($recipient->email, $recipient->name);
                    break;
                case self::EMAIL_TO_BCC:
                    $mailer->addBCC($recipient->email, $recipient->name);
                    break;
            }
        }
        if ( $message->attachment_path )
        {
            $mailer->addAttachment($message->attachment_path);         // Add attachments
        }
        $mailer->WordWrap = 50;                                 // Set word wrap to 50 characters
        $mailer->isHTML(true);                                  // Set email format to HTML

        $mailer->Subject = $message->subject;
        $mailer->Body    = $message->html_body;
        $mailer->AltBody = $message->txt_body;

        if ( ! $mailer->send() )
        {
            $id = ! empty($message->id) ? $message->id : 'Custom Email';
            App::log()->error("Mailer failed to send email id {$id} with error: " . $mailer->ErrorInfo);
            return false;
        }
        if ( ! empty($message->id) )
        {
            App::eventLog()
                ->logMessage()
                ->type(EventLog::EMAILED)
                ->job(__FUNCTION__)
                ->message("<a href=\"/messages/{$message->id}.html\">/messages/{$message->id}.html</a>")
                ->otherData(['recipients' => $message->Recipients])
                ->save();
        }
        else
        {
            App::log()->debug("Sent email to " . implode(", ", $toList));
        }

        return true;
    }


    protected static function _canSend(stdClass $message, bool $forceSendInDev) : bool
    {
        return true;
    }

}
