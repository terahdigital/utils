<?php declare(strict_types=1);

namespace Terah\Utils;

use Exception;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;
use Terah\Assert\Assert;

class Sms
{
    const CLICK_SEND                    = 'clicksend';
    const SMS_GLOBAL                    = 'smsglobal';
    const DUMMY                         = 'dummy';

    protected array $primaryConfig      = [];

    protected array $secondaryConfig    = [];

    protected LoggerInterface $logger;

    public function __construct(array $primaryConfig, array $secondaryConfig, LoggerInterface $logger)
    {
        Assert::that($primaryConfig)->keysExist(['user', 'pass', 'from', 'uri']);
        Assert::that($primaryConfig['user'])->notEmpty();
        Assert::that($primaryConfig['pass'])->notEmpty();
        Assert::that($primaryConfig['from'])->notEmpty();
        Assert::that($primaryConfig['uri'])->url();
        $this->primaryConfig    = $primaryConfig;
        $this->logger           = $logger;
        if ( ! $secondaryConfig )
        {
            return ;
        }
        Assert::that($secondaryConfig)->keysExist(['user', 'pass', 'from', 'uri']);
        Assert::that($secondaryConfig['user'])->notEmpty();
        Assert::that($secondaryConfig['pass'])->notEmpty();
        Assert::that($secondaryConfig['from'])->notEmpty();
        Assert::that($secondaryConfig['uri'])->url();
        $this->secondaryConfig  = $secondaryConfig;

    }


    public function sendSmsMessage(string $mobileNumber, string $message, string $provider='any') : bool
    {
        Assert::that($provider)->inArray(['any', 'primary', 'secondary'], "Provider can only be any, primary or secondary");

        switch ( $provider )
        {
            case 'primary':

                return $this->sendSmsMessageByProvider($mobileNumber, $message, $this->primaryConfig);

            case 'secondary':

                Assert::that($this->secondaryConfig)->notEmpty('A secondary provider has not been configured for');

                return $this->secondaryConfig && $this->sendSmsMessageByProvider($mobileNumber, $message, $this->secondaryConfig);

            case 'any':

                if ( $this->sendSmsMessageByProvider($mobileNumber, $message, $this->primaryConfig) )
                {
                    return true;
                }
                if ( $this->secondaryConfig && $this->sendSmsMessageByProvider($mobileNumber, $message, $this->secondaryConfig) )
                {
                    return true;
                }

                return false;
        }

        return false;
    }


    protected function sendSmsMessageByProvider(string $mobileNumber, string $message, array $providerConfig) : bool
    {
        switch ( $providerConfig['provider'] )
        {
            case self::CLICK_SEND:

                return $this->sendSmsMessageClickSend($mobileNumber, $message, $providerConfig);

            case self::SMS_GLOBAL:

                return $this->sendSmsMessageSMSGlobal($mobileNumber, $message, $providerConfig);

            case self::DUMMY:

                return $this->sendSmsMessageDummy($mobileNumber, $message, $providerConfig);
        }

        $this->logger->error('Failed to load sms provider', $providerConfig);

        return false;
    }


    public function sendSmsMessageClickSend(string $mobileNumber, string $message, array $providerConfig) : bool
    {
        //  https://api-mapper.clicksend.com/http/v2/send.php?method=http&username=xxxx&key=xxxx&to=xxxx&message=xxxx
        $data           = [
            'action'        => 'sendsms',
            'username'      => $providerConfig['user'],
            'key'           => $providerConfig['pass'],
            'to'            => $mobileNumber,
            'from'          => $providerConfig['from'],
            'message'       => $message
        ];
        $url            = $providerConfig['uri'] . '?' . http_build_query($data);
        $this->logger->info('SMS Url: ' . $url);
        try
        {
            $result         = file_get_contents($url);
            // Sample Response
            // < ?xml version="1.0" encoding="UTF-8"? >
            // <xml>
            //     <messages recipientcount="1">
            //         <message>
            //             <to>+61400000000</to>
            //             <messageid>0B9B75C6-1319-49CC-830B-7898454</messageid>
            //             <result>0000</result>
            //             <errortext>Success</errortext>
            //             <price>0.0770</price>
            //             <currency_symbol>$</currency_symbol>
            //             <currency_type>AUD</currency_type>
            //         </message></messages>
            // </xml>
            Assert::that($result)->contains('<errortext>Success</errortext>');
        }
        catch ( Exception $e )
        {
            $this->logger->error('Failed to send SMS message with error' . $e->getMessage(), compact('providerConfig', 'result'));

            return false;
        }

        return true;
    }

    /**
     * @param string $mobileNumber
     * @param string $message
     * @param array $providerConfig
     * @return bool
     */
    public function sendSmsMessageSMSGlobal(string $mobileNumber, string $message, array $providerConfig) : bool
    {
        $data           = [
            'action'        => 'sendsms',
            'user'          => $providerConfig['user'],
            'password'      => $providerConfig['pass'],
            'to'            => $mobileNumber,
            'from'          => $providerConfig['from'],
            'text'          => $message
        ];
        $url            = $providerConfig['uri'] . '?' . http_build_query($data);
        $this->logger->info('SMS Url: ' . $url);
        $result         = file_get_contents($url);
        //Sample Response
        //OK: 0; Sent queued message ID: 04b4a8d4a5a02176 SMSGlobalMsgID:23452345
        $parts          = explode('SMSGlobalMsgID:', $result);
        Assert::that($parts)->count(2, 'Failed to send SMS message');
        Assert::that($result)->startsWith('OK', 'Failed to send SMS message');

        return true;
    }


    public function sendSmsMessageDummy(string $mobileNumber, string $message, array $providerConfig) : bool
    {
        $data                   = [
            'action'                => 'sendsms',
            'to'                    => $mobileNumber,
            'from'                  => $providerConfig['from'],
            'text'                  => $message
        ];

        if ( in_array($mobileNumber, $this->primaryConfig['whitelist']) )
        {
            return $this->sendSmsMessageSMSGlobal($mobileNumber, $message, $providerConfig);
        }

        $this->logger->info('Send Dummy Sms Message', $data);

        return true;
    }

    /**
     * @param SimpleXMLElement $xmlObject
     * @param array $out
     * @return array
     */
    protected function xml2array(SimpleXMLElement $xmlObject, array $out=[]) : array
    {
        foreach( $xmlObject->attributes() as $attr => $val )
        {
            $out['@attributes'][$attr] = (string)$val;
        }
        $hasChildren = false;
        foreach( $xmlObject as $index => $node )
        {
            $hasChildren     = true;
            $out[$index][]  = $this->xml2array($node);
        }
        if ( ! $hasChildren && $val = (string)$xmlObject )
        {
            $out['@value'] = $val;
        }
        foreach ( $out as $key => $values )
        {
            if ( is_array($values) && count($values) === 1 && array_key_exists(0, $values) )
            {
                $out[$key] = $values[0];
            }
        }

        return $out;
    }
}