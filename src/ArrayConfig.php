<?php declare(strict_types=1);

namespace Terah\Utils;

use Closure;
use Exception;
use Phalcon\Framework\Crypt;
use Terah\Assert\Assert;
use stdClass;

/**
 * Class ArrayConfig
 *
 * @package Terah\Utils
 */
class ArrayConfig
{
    /** @var array  */
    protected array $_config        = [];

    /** @var string  */
    protected string $_mode         = '';

    /** @var string  */
    protected string $_appKey       = '';

    /** @var string  */
    protected string $_requestId    = '';

    /** @var Closure|string */
    protected $_appVersion          = '';

    /** @var Crypt */
    protected Crypt $_crypt;

    /** @var string  */
    protected string $_cryptPrefix  = '';


    public function __construct(Crypt $crypt, string $configFile, string $appRoot, string $webRoot, string $requestId='')
    {
        $appEnv                 = (string)getenv('APP_ENV');
        $appKeySuffix           = (string)getenv('APP_KEY');
        Assert::that($appEnv)->regex('/^(prod|test|local)-(prod|stage|test|dev)$/', "The APP_ENV environment variable must be in the format (prod|test|local)-(prod|stage|test|dev)");
        list($serverEnv, $appEnv) = explode('-', $appEnv);
        define('APP_ENV', $appEnv);

        $appRoot                = str_replace(DIRECTORY_SEPARATOR, '/', $appRoot);
        $webRoot                = str_replace(DIRECTORY_SEPARATOR, '/', $webRoot);
        // Set a 'one-true-root' for the app where everything can be included from...
        Assert::that($configFile)->file("The config file {$configFile} does not exist.");
        Assert::that($appRoot)->directory("The app root directory {$appRoot} does not exist.");
        Assert::that($webRoot)->directory("The web root directory {$webRoot} does not exist.");
        Assert::that($appEnv)->notEmpty("The APP_ENV environment variable has not been set");
        Assert::that($serverEnv)->notEmpty("The APP_KEY environment variable has not been set");
        Assert::that($appKeySuffix)->length(16, "The APP_KEY environment variable has not been set or is not 16 characters");

        if ( ! defined('APP_ROOT') ) define('APP_ROOT', $appRoot);
        if ( ! defined('WEB_ROOT') ) define('WEB_ROOT', $webRoot);

        $config                 = require_once $configFile;
        $this->_cryptPrefix     = $config[$serverEnv][$appEnv]['secrets.prefix'] ??0?: '';
        $cryptKey               = ( $config[$serverEnv][$appEnv]['secrets.key_prefix'] ??0?: '' )  . $appKeySuffix;
        Assert::that($cryptKey)->length(32, "The secrets encryption key is not 32 characters");
        Assert::that($this->_cryptPrefix)->notEmpty("The secrets encryption prefix must not be empty.");
        $this->_crypt           = $crypt->setKey($cryptKey);
        Assert::that($config)->keyExists($serverEnv, "The server environment ({$serverEnv})) does not exist in the config array");
        $this->mapToArray($config[$serverEnv]);
        Assert::that($this->_config)->keyExists($appEnv, "The app environment ({$appEnv})) does not exist in the config array");
        Assert::that($this->_config[$appEnv])->keyExists('secrets', "The secrets values do not exist in the config array");
        $this->_mode            = $appEnv;
        $this->_requestId       = $requestId ?: uniqid() . str_pad((string)rand(1,999), 3, '0', STR_PAD_LEFT);
        $this->setPhpIniSettings();
    }


    public function dumpEncrypted(string $pattern, bool $purgeNonPasswords) : array
    {
        if ( ! defined("STDERR") )
        {
            throw new Exception('Cannot dump config in HTTP mode.');
        }

        return $this->encryptAll($this->_config, $pattern, $purgeNonPasswords);
    }


    protected function mapToArray(array $config)
    {
        foreach ( $config as $section => $conf )
        {
            foreach ( $conf as $key => $value )
            {
                $this->_config[$section] = isset($this->_config[$section]) ? $this->_config[$section] : [];
                $this->_config[$section] = $this->_arrayMergeRecursive($this->_config[$section], $this->_processContentEntry($key, $value));
            }
        }
    }


    public function requestId() : string
    {
        return $this->_requestId;
    }


    public function setAppVersion(Closure $callback)
    {
        $this->_appVersion      = $callback;
    }


    public function getAppVersion() : string
    {
        if ( is_callable($this->_appVersion) )
        {
            $this->_appVersion      = $this->_appVersion->__invoke();
        }

        return $this->_appVersion;
    }


    public function checkEnvironment(array $settings, array $extensions, array $writable)
    {
        foreach ( $settings as $setting => $val )
        {
            Assert::that(ini_get($setting))->eq($val, "The php configuration value '{$setting}' needs to be {$val}");
        }
        foreach ( $extensions as $ext )
        {
            Assert::that(extension_loaded($ext))->true("The php extension '{$ext}' needs to be enabled");
        }
        foreach ( $writable as $path )
        {
            Assert::that($path)->writeable("The file location '{$path}' needs to be writable");
        }
    }

    /**
     * Get your config variable from the default website config or a different one
     *
     * @param string $name
     * @param string $section
     * @param bool   $throwException
     *
     * @throws NoSettingException
     * @return array|bool|int|null|string
     */
    public function get(string $name, string $section='', bool $throwException=true)
    {
        Assert::that($name)->string()->notEmpty('The config key name can not be empty.');
        $section                = $section ?: $this->_mode;
        Assert::that($this->_config)->keyExists($section, "The section ({$section}) doesn't exist in the config array");

        return $this->_config[$section][$name]  ?? $this->findInArray($this->_config[$section], explode('.', $name), $throwException);
    }


    public function encryptConfigValue(string $key, string $section) : string
    {
        $value                  = $this->get($key, $section);
        Assert::that($value)->string("encryptValue() only works with string values");
        $prefix                 = preg_quote($this->_cryptPrefix);
        if ( preg_match("/^{$prefix}/", $value) )
        {
            return $value;
        }

        return $this->_cryptPrefix . $this->_crypt->encryptBase64($value, null, true);
    }


    protected function encryptAll(array $config, string $pattern, bool $purgeNonPasswords): array
    {
        $prefix                 = preg_quote($this->_cryptPrefix);
        foreach ( $config as $idx => $c )
        {
            if ( is_array($c) )
            {
                $config[$idx]           = $this->encryptAll($c, $pattern, $purgeNonPasswords);
                if ( $purgeNonPasswords && empty($config[$idx]) )
                {
                    unset($config[$idx]);
                }
            }
            else if ( is_string($c) )
            {
                if ( ! preg_match($pattern, (string)$idx) )
                {
                    if ( $purgeNonPasswords )
                    {
                        unset($config[$idx]);
                    }

                    continue;
                }
                if ( preg_match("/^{$prefix}/", (string)$c) )
                {
                    echo "Value {$c} is already encrypted.\n";
                }
                $config[$idx]           = $this->_cryptPrefix . $this->_crypt->encryptBase64($c, null, true);
            }
            elseif ( $purgeNonPasswords )
            {
                unset($config[$idx]);
            }
        }

        return $config;
    }

    protected function decrypt($value)
    {
        if ( is_string($value) )
        {
            if ( ! $this->_cryptPrefix || $value === $this->_cryptPrefix || strpos($value, $this->_cryptPrefix) !== 0 )
            {
                return $value;
            }

            return  $this->_crypt->decryptBase64(preg_replace('/^' . preg_quote($this->_cryptPrefix) . '/', '', $value), null, true);
        }
        if ( is_array($value) )
        {
            foreach ( $value as $idx => $val )
            {
                $value[$idx]        = $this->decrypt($val);
            }
        }

        return $value;
    }


    public function getObj(string $name, string $section='', bool $throwException=true) : stdClass
    {
        return (object)$this->get($name, $section, $throwException);
    }


    public function getMode() : string
    {
        return $this->_mode;
    }


    public function isProd() : bool
    {
        return $this->getMode() === 'prod';
    }


    /**
     * @param array $array
     * @param array $keys
     * @param bool $throwException
     * @return array|mixed|null
     */
    public function findInArray(array $array, array $keys, bool $throwException)
    {
        foreach ( $keys as $key )
        {
            if ( ! isset($array[$key]) )
            {
                if ( $throwException )
                {
                    Assert::that($array)->keyExists($key, "The config value ({$key}) doesn't exist in the config array");
                }

                return null;
            }
            $array                  = $array[$key];
        }

        return $array;
    }


    public function getBySubsection(string $name, string $section, bool $throwException)
    {
        Assert::that($name)->string()->notEmpty('The config key name can not be empty.');
        Assert::that($this->_config)->keyExists($section, "The section ({$section}) doesn't exist in the config array");
        foreach ( $this->_config[$section] as $key => $value )
        {
            if ( preg_match('/^' . preg_quote($name) . '\./', $key) )
            {
                $this->_config[$section][$name] = isset($this->_config[$section][$name]) ? $this->_config[$section][$name] : [];
                $this->_config[$section] = $this->_arrayMergeRecursive($this->_config[$section], $this->_processContentEntry($key, $value));
            }
        }
        if ( ! isset($this->_config[$section][$name]) && $throwException )
        {
            Assert::that($this->_config[$section])->keyExists($name, "The config value ({$name}) doesn't exist in the config array");
        }

        return isset($this->_config[$section][$name]) ? $this->_config[$section][$name] : null;
    }


    public function setPhpIniSettings(array $settings=[])
    {
        $settings = $settings ?: $this->flattenKeys($this->get('php_ini.ini'));
        foreach ( $settings as $setting => $val )
        {
            if ( $setting === 'date.timezone' )
            {
                date_default_timezone_set($val);
                continue;
            }
            ini_set($setting, $val);
        }
    }


    public function flattenKeys(array $nested, array $flattened=[], array $parents=[]) : array
    {
        foreach ( $nested as $key => $val )
        {
            $this_key           = array_merge($parents, [$key]);
            if ( is_array($val) )
            {
                $flattened          = $this->flattenKeys($val, $flattened, $this_key);

                continue;
            }
            $flattened[implode('.', $this_key)] = $val;
        }

        return $flattened;
    }


    /**
     * Convert a.b.c.d paths to multi-dimensional arrays
     *
     * @param string $path  Current ini file's line's key
     * @param mixed  $value Current ini file's line's value
     *
     * @return array
     */
    private function _processContentEntry(string $path, $value) : array
    {
        $pos                    = mb_strpos($path, '.');
        if ( $pos === false )
        {
            return [$path => $this->decrypt($value)];
        }
        $key                    = mb_substr($path, 0, $pos);
        $path                   = mb_substr($path, $pos + 1);

        return [
            $key                    => $this->_processContentEntry($path, $value),
        ];
    }

    /**
     * Merge two arrays recursively overwriting the keys in the first array
     * if such key already exists
     *
     * @param mixed $a Left array to merge right array into
     * @param mixed $b Right array to merge over the left array
     *
     * @return mixed
     */
    private function _arrayMergeRecursive($a, $b)
    {
        // merge arrays if both variables are arrays
        if ( is_array($a) && is_array($b) )
        {
            // loop through each right array's entry and merge it into $a
            foreach ( $b as $key => $value )
            {
                if ( isset( $a[$key] ) )
                {
                    $a[$key]                = $this->_arrayMergeRecursive($a[$key], $value);

                    continue;
                }
                if ( $key === 0 )
                {
                    $a                      = [0 => $this->_arrayMergeRecursive($a, $value)];

                    continue;
                }
                $a[$key]                = $value;
            }

            return $a;
        }

        return $b;
    }
}