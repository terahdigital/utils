<?php declare(strict_types=1);

namespace Terah\Utils;

use JsonSerializable;
use Terah\View\ViewRendererInterface;
use Terah\Assert\Assert;


/**
 * Class Result
 *
 * @property int count
 * @property int total
 * @property int limit
 * @property int offset
 * @property int page
 * @property int pages
 * @property array order
 * @property string self
 * @property array errors
 * @property string redirect
 * @property string notification_title
 * @property string notification_body
 * @property array filters
 * @property array fields
 * @property string method
 * @property array perms
 * @property array recent_searches
 * @property mixed data
 * @property string format
 * @property string layout
 * @property string|bool template
 * @property int status
 * @property string attachment
 * @property string fileName
 * @property string emailTo
 * @property string orientation
 * @property string wkhtmlArgs
 *
 * @package Terah\Utils
 * @method Result count(int $val)
 * @method Result total(int $val)
 * @method Result limit(int $val)
 * @method Result offset(int $val)
 * @method Result page(int $val)
 * @method Result pages(int $val)
 * @method Result order(array $val)
 * @method Result self(string $val)
 * @method Result errors(array $val)
 * @method Result redirect(string $val)
 * @method Result filters(array $val)
 * @method Result fields(array $val)
 * @method Result method(string $val)
 * @method Result perms(array $val)
 * @method Result recent_searches(array $val)
 * @method Result data($val)
 * @method Result format(string $val)
 * @method Result layout(string $val)
 * @method Result template(string|bool $val)
 * @method Result status(int $val)
 * @method Result attachment(string $val)
 * @method Result fileName(string $val)
 * @method Result emailTo(string $val)
 * @method Result orientation(string $val)
 * @method Result wkhtmlArgs(string $val)
 */
class Result implements JsonSerializable
{
    /**
     * @var array
     */
    protected array $defaults     = [
        'count'                 => 0,
        'total'                 => 0,
        'limit'                 => 0,
        'offset'                => 0,
        'page'                  => 1,
        'pages'                 => 1,
        'order'                 => [],
        'self'                  => '',
        'notification_title'    => '',
        'notification_body'     => '',
        'errors'                => [],
        'redirect'              => '',
        'filters'               => [],
        'fields'                => [],
        'method'                => '',
        'perms'                 => [],
        'recent_searches'       => [],
        'status'                => 200,
        'data'                  => null,
        'format'                => '',
        'layout'                => '',
        'template'              => null,
        'attachment'            => 'attachment',
        'fileName'              => '',
        'orientation'           => '',
        'wkhtmlArgs'            => '',
    ];

    /**
     * @var array
     */
    protected array $metaData     = [];

    /**
     * @param mixed $data
     */
    public function __construct($data=null)
    {
        $this->set('data', $data);
    }

    /**
     * @param Result[] $results
     * @return Result
     */
    public function mergeData(array $results) : Result
    {
        Assert::that($results)->all()->isInstanceOf(Result::class);

        $data = [];
        foreach ( $results as $result )
        {
            $data[] = $result->get('data');
        }

        return $this->data($data);
    }


    public function setArray(array $data) : Result
    {
        foreach ( $data as $name => $value )
        {
            $this->set($name, $value);
        }
        return $this;
    }


    public function getViewData() : array
    {
        $viewFields = [
            //'data',
            //'format',
            'layout',
            'template',
            'attachment',
            'fileName',
            'orientation',
            'wkhtmlArgs',
            'status',
            'notification_title',
            'notification_body',
        ];

        return array_intersect_key($this->metaData, array_flip($viewFields));
    }

    /**
     * @param string $name
     * @param mixed $args
     * @return Result
     */
    public function __call(string $name, $args) : Result
    {
        return $this->set($name, $args[0]);
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return Result
     */
    public function set(string $name, $value) : Result
    {
        Assert::that($this->defaults)->keyExists($name, "Invalid property ({$name}) sent to response meta");
        if ( ! is_null($this->defaults[$name]) )
        {
            Assert::that(gettype($value))->eq(gettype($this->defaults[$name]), "Invalid property type (" . gettype($value) . ") for property ({$name}) sent to response meta:" . print_r($value, true));
        }
        $this->metaData[$name] = $value;

        return $this;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->get($name);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name)
    {
        Assert::that($this->defaults)->keyExists($name, "Invalid property ({$name}) sent to response meta");

        return array_key_exists($name, $this->metaData) ? $this->metaData[$name] : $this->defaults[$name];
    }


    public function toString(ViewRendererInterface $view, Request $request) : string
    {
        $this->format($this->format ? $this->format : $request->getResponseType());
        $data       = is_array($this->data) && array_key_exists('data', $this->data) ? $this->data['data'] : $this->data;
        if ( is_string($data) )
        {
            return $data;
        }
        switch ( $this->format )
        {
            case 'xml':
            case 'json':
            case 'csv':
            case 'xls':
            case 'xlsx':

                $this->data = $data;
                break;

            case 'html':

                $this->data     = is_null($this->data) ? [] : $this->data;
                $this->data     = is_array($this->data) ? $this->data : (array)$this->data;
                if ( $this->redirect && ! $this->template )
                {
                    $this->template('errors/redirect');
                }
                if (  ! $this->template && $this->status === 404 )
                {
                    $this->set('template', 'errors/error');
                }
                if ( in_array($this->template, ['errors/redirect', 'errors/error']) )
                {
                    $this->data['action']             = 'error';
                    $this->data['status']             = $this->status;
                    $this->data['notification_title'] = ! empty($this->data['notification_title']) ? $this->data['notification_title'] : $this->notification_title;
                    $this->data['redirect']           = $this->redirect;
                    $this->data['notification_body']  = ! empty($this->data['notification_body']) ? $this->data['notification_body'] : $this->notification_body;
                }
                if ( $this->template === false )
                {
                    return $this->data[0];
                }
                break;
        }

        return $view->toString($this->data, $this->getViewData());
    }

    /**
     * @param bool $filtered
     * @return array
     */
    public function toArray(bool $filtered=true) : array
    {
        return $filtered ? $this->_getFiltered() : $this->metaData;
    }

    /**
     * @return string
     */
    public function toJson() : string
    {
        return json_encode($this->_getFiltered());
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->_getFiltered();
    }

    /**
     * @param string $title
     * @param string $message
     * @param array  $validationErrors
     * @return Result
     */
    public function setMessages(string $title, string $message='', array $validationErrors=[]) : Result
    {
        $metaData = [
            'notification_title'    => $title,
            'notification_body'     => $message,
            'errors'                => $validationErrors,
        ];

        return $this->setArray($metaData);
    }

    /**
     * @param mixed $data
     * @return Result
     */
    public static function factory(?array $data=null) : Result
    {
        return new static($data);
    }

    /**
     * @return array
     */
    protected function _getFiltered() : array
    {
        $meta   = array_merge($this->defaults, $this->metaData);
        unset($meta['data'], $meta['status'], $meta['format'], $meta['layout'], $meta['template'], $meta['attachment'], $meta['fileName'], $meta['orientation'], $meta['wkhtmlArgs']);

        return $meta;
    }
}
