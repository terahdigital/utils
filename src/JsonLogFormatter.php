<?php declare(strict_types=1);


namespace Terah\Utils;


use Phalcon\Framework\Logger\Formatter\Json;

class JsonLogFormatter extends Json
{
    public function format(string $message, int $type, int $timestamp, array $context=null) : string
    {
        $context                = (array)$context;

        $data                   = [];
        $data['DATE']           = $context['DATE'] ??0? : Date::dateMicroTime();
        $data['TYPE']           = $this->getTypeString($type);
        $data['USER']           = $context['USER'] ??0? : '';
        $data['PATH']           = $context['PATH'] ??0? : '';
        $data['ADDR']           = $context['ADDR'] ??0? : '';
        $data['AGNT']           = $context['AGNT'] ??0? : '';
        $data['MSG']            = $message;

        return json_encode($data, JSON_UNESCAPED_SLASHES) . PHP_EOL;
    }
}