<?php declare(strict_types=1);

namespace Terah\Utils;

class StringUtils
{
    /**
     * @param string $needle
     * @param string $hayStack
     * @return bool
     */
    public static function startsWith(string $needle, string $hayStack) : bool
    {
        return strpos($hayStack, $needle) === 0;
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @return bool
     */
    public static function endsWith(string $needle, string $hayStack) : bool
    {
        $needle         = strrev($needle);
        $hayStack       = strrev($hayStack);

        return strpos($hayStack, $needle) === 0;
    }

    /**
     * @param string $start
     * @param string $end
     * @param string $replace
     * @param string $hayStack
     * @param string $separator
     * @return string
     */
    public static function replaceBetween(string $start, string $end, string $replace, string $hayStack, string $separator="\n") : string
    {
        $quotedStart    = preg_quote($start, '/');
        $quotedEnd      = preg_quote($end, '/');
        $replace        = $start . $separator . $replace . $separator . $end;

        return preg_replace("/({$quotedStart})(.*)({$quotedEnd})/si", $replace, $hayStack);
    }

    /**
     * @param string $start
     * @param string $end
     * @param string $hayStack
     * @return string
     */
    public static function between(string $start, string $end, string $hayStack) : string
    {
        return StringUtils::before($end, StringUtils::after($start, $hayStack));
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @param bool $returnOrigIfNeedleNotExists
     * @return string
     */
    public static function before(string $needle, string $hayStack, bool $returnOrigIfNeedleNotExists=false) : string
    {
        $position   = mb_strpos($hayStack, $needle);
        if ( $position === false )
        {
            return $returnOrigIfNeedleNotExists ? $hayStack : '';
        }
        $result     = mb_substr($hayStack, 0, $position);
        if ( ! $result && $returnOrigIfNeedleNotExists )
        {
            return $hayStack;
        }

        return $result;
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @param bool $returnOrigIfNeedleNotExists
     * @return string
     */
    public static function after(string $needle, string $hayStack, bool $returnOrigIfNeedleNotExists=false) : string
    {
        if ( ! is_bool(mb_strpos($hayStack, $needle)) )
        {
            return mb_substr($hayStack, mb_strpos($hayStack, $needle) + mb_strlen($needle));
        }

        return $returnOrigIfNeedleNotExists ? $hayStack : '';
    }

    /**
     * @param string $start
     * @param string $end
     * @param string $hayStack
     * @return string
     */
    public static function betweenLast(string $start, string $end, string $hayStack) : string
    {
        return StringUtils::afterLast($start, StringUtils::beforeLast($end, $hayStack));
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @param bool $returnOrigIfNeedleNotExists
     * @return string
     */
    public static function afterLast(string $needle, string $hayStack, bool $returnOrigIfNeedleNotExists=false) : string
    {
        if ( ! is_bool(StringUtils::strrevpos($hayStack, $needle)) )
        {
            return mb_substr($hayStack, StringUtils::strrevpos($hayStack, $needle) + mb_strlen($needle));
        }

        return $returnOrigIfNeedleNotExists ? $hayStack : '';
    }

    /**
     * @param string $str
     * @param string $needle
     * @return bool|int
     */
    public static function strrevpos(string $str, string $needle)
    {
        $revStr = mb_strpos(strrev($str), strrev($needle));

        return $revStr === false ? false : mb_strlen($str) - $revStr - mb_strlen($needle);
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @return string
     */
    public static function beforeLast(string $needle, string $hayStack) : string
    {
        $position   = static::strrevpos($hayStack, $needle);

        return $position === -1 || $position === false ? '' : mb_substr($hayStack, 0, $position);
    }

    /**
     * @param int $bytes
     * @param int $decimals
     * @return string
     */
    public static function humanFileSize(int $bytes, int $decimals=2) : string
    {
        $sz     = 'BKMGTP';
        $factor = floor(( mb_strlen((string)$bytes) - 1 ) / 3);

        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . $sz[(int)$factor];
    }

    /**
     *
     * @convert seconds to hours minutes and seconds
     *
     * @param int $seconds
     * @return string
     */
    public static function secondsToWords(int $seconds) : string
    {
        /*** return value ***/
        $ret = "";

        /*** get the hours ***/
        $hours = intval((string)intval($seconds) / 3600);
        if($hours > 0)
        {
            $ret .= "$hours hours ";
        }
        /*** get the minutes ***/
        $minutes = bcmod((string)(intval($seconds) / 60), (string)60);
        if($hours > 0 || $minutes > 0)
        {
            $ret .= "$minutes minutes ";
        }

        /*** get the seconds ***/
        $seconds = bcmod((string)intval($seconds), (string)60);
        $ret .= "$seconds seconds";

        return $ret;
    }

    /**
     * @param string $string
     * @return string
     */
    public static function addTrailingSlash(string $string) : string
    {
        return static::addTrailingCharIfNotExists($string, '/');
    }

    /**
     * @param string $string
     * @param string $char
     * @return string
     */
    public static function addTrailingCharIfNotExists(string $string, string $char) : string
    {
        return mb_substr($string, - 1) !== $char ? "{$string}{$char}" : $string;
    }

    /**
     * @param string $string
     * @param int $length
     * @return string
     */
    public static function getElidedString(string $string, int $length) : string
    {
        return mb_strlen($string) < ( $length - 3 ) ? $string : mb_substr($string, 0, $length - 3) . '...';
    }

    /**
     * @param array $array
     * @param string $glue
     * @return string
     */
    public static function removeEmptyAndImplode(array $array, string $glue=' ') : string
    {
        return implode($glue, array_filter($array));
    }

    /**
     * @param string $string
     * @param int $length
     * @param bool $break_on_non_alpha
     * @param string $suffix
     * @param bool $force_suffix_on_orig
     * @return string
     */
    public static function getCutStringAtWordBreak(string $string, int $length, bool $break_on_non_alpha=false, string $suffix='', bool $force_suffix_on_orig = false) : string
    {
        if ( mb_strlen($string) <= $length )
        {
            $suffix = $force_suffix_on_orig ? $suffix : '';
            return $string . $suffix;
        }
        $regex = $break_on_non_alpha ? '/[^\da-z]/i' : '/\\s/';
        $res   = preg_match_all($regex, $string, $matches, PREG_OFFSET_CAPTURE, $length);
        if ( ! $res )
        {
            $suffix = $force_suffix_on_orig ? $suffix : '';
            return $string . $suffix;
        }
        return mb_substr($string, 0, $matches[0][0][1]) . $suffix;
    }

    /**
     * @param string $string
     * @return string
     */
    public static function ucname(string $string) : string
    {
        $string = ucwords(mb_strtolower($string));
        foreach ( ['-', '\''] as $delimiter )
        {
            if ( mb_strpos($string, $delimiter) !== false )
            {
                $string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
            }
        }

        return $string;
    }

    /**
     * @param string|int|bool $val
     * @param string $default
     * @return string
     */
    public static function toFlag($val, string $default='') : string
    {
        if ( $val && $default )
        {
            return $default;
        }

        return in_array($val, ['Y', 'y', true, '1', 'Yes', 'yes', 1], true) ? '1' : '0';
    }

    /**
     * @param float $size
     * @return string
     */
    public static function convertDigitSize(float $size) : string
    {
        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];

        return @round($size / pow(1024, ( $i = floor(log($size, 1024)) )), 2) . ' ' . $unit[(int)$i];
    }

    /**
     * @param string $string
     * @return string
     *
     * This function removes any duplicate spaces from a string
     */
    public static function removeDoubleSpace(string $string) : string
    {
        return preg_replace('/[ ]{2,50}/', ' ', $string);
    }

    /**
     * @param string $string
     * @return string
     *
     * This function removes duplicate spaces in a string and all trailing spaces
     */
    public static function cleanseWhiteSpace(string $string) : string
    {
        return trim(StringUtils::removeDoubleSpace($string));
    }

    /**
     * Tokenizes a string using $separator, ignoring any instance of $separator that appears between
     * $leftBound and $rightBound.
     *
     * @param string $data       The data to tokenize.
     * @param string $separator  The token to split the data on.
     * @param string $leftBound  The left boundary to ignore separators in.
     * @param string $rightBound The right boundary to ignore separators in.
     *
     * @return mixed Array of tokens in $data or original input if empty.
     */
    public static function tokenize(string $data, string $separator = ',', $leftBound = '(', $rightBound = ')') : array
    {
        if ( empty( $data ) )
        {
            return [];
        }
        $depth   = 0;
        $offset  = 0;
        $buffer  = '';
        $results = [];
        $length  = strlen($data);
        $open    = false;
        while ( $offset <= $length )
        {
            $tmpOffset = - 1;
            $offsets   = [
                strpos($data, $separator, $offset),
                strpos($data, $leftBound, $offset),
                strpos($data, $rightBound, $offset)
            ];
            for ( $i = 0; $i < 3; $i ++ )
            {
                if ( $offsets[$i] !== false && ( $offsets[$i] < $tmpOffset || $tmpOffset == - 1 ) )
                {
                    $tmpOffset = $offsets[$i];
                }
            }
            if ( $tmpOffset !== - 1 )
            {
                $buffer .= substr($data, $offset, ( $tmpOffset - $offset ));
                if ( ! $depth && $data[$tmpOffset] == $separator )
                {
                    $results[] = $buffer;
                    $buffer    = '';
                }
                else
                {
                    $buffer .= $data[$tmpOffset];
                }
                if ( $leftBound != $rightBound )
                {
                    if ( $data[$tmpOffset] == $leftBound )
                    {
                        $depth ++;
                    }
                    if ( $data[$tmpOffset] == $rightBound )
                    {
                        $depth --;
                    }
                }
                else
                {
                    if ( $data[$tmpOffset] == $leftBound )
                    {
                        if ( ! $open )
                        {
                            $depth ++;
                            $open = true;
                        }
                        else
                        {
                            $depth --;
                        }
                    }
                }
                $offset = ++$tmpOffset;
            }
            else
            {
                $results[] = $buffer . substr($data, $offset);
                $offset    = $length + 1;
            }
        }
        if ( empty( $results ) && ! empty( $buffer ) )
        {
            $results[] = $buffer;
        }
        if ( ! empty( $results ) )
        {
            return array_map('trim', $results);
        }

        return [];
    }

    /**
     * Wraps text to a specific width, can optionally wrap at word breaks.
     *
     * ### Options
     *
     * - `width` The width to wrap to. Defaults to 72.
     * - `wordWrap` Only wrap on words breaks (spaces) Defaults to true.
     * - `indent` String to indent with. Defaults to null.
     * - `indentAt` 0 based index to start indenting at. Defaults to 0.
     *
     * @param string        $text    The text to format.
     * @param array|integer $options Array of options to use, or an integer to wrap the text to.
     *
     * @return string Formatted text.
     */
    public static function wrap(string $text, array $options = []) : string
    {
        if ( is_numeric($options) )
        {
            $options = ['width' => $options];
        }
        $options += [
            'width'    => 72,
            'wordWrap' => true,
            'indent'   => null,
            'indentAt' => 0
        ];
        if ( $options['wordWrap'] )
        {
            $wrapped = self::wordWrap($text, $options['width']);
        }
        else
        {
            $wrapped = trim(chunk_split($text, $options['width'] - 1));
        }
        if ( ! empty( $options['indent'] ) )
        {
            $chunks = explode("\n", $wrapped);
            for ( $i = $options['indentAt'], $len = count($chunks); $i < $len; $i ++ )
            {
                $chunks[$i] = $options['indent'] . $chunks[$i];
            }
            $wrapped = implode("\n", $chunks);
        }

        return $wrapped;
    }

    /**
     * Unicode aware version of wordwrap.
     *
     * @param string  $text  The text to format.
     * @param integer $width The width to wrap to. Defaults to 72.
     * @param string  $break The line is broken using the optional break parameter. Defaults to '\n'.
     * @param boolean $cut   If the cut is set to true, the string is always wrapped at the specified width.
     *
     * @return string Formatted text.
     */
    public static function wordWrap(string $text, int $width=72, string $break="\n", bool $cut=false) : string
    {
        if ( $cut )
        {
            $parts = [];
            while ( mb_strlen($text) > 0 )
            {
                $part    = mb_substr($text, 0, $width);
                $parts[] = trim($part);
                $text    = trim(mb_substr($text, mb_strlen($part)));
            }

            return implode($break, $parts);
        }
        $parts = [];
        while ( mb_strlen($text) > 0 )
        {
            if ( $width >= mb_strlen($text) )
            {
                $parts[] = trim($text);
                break;
            }
            $part     = mb_substr($text, 0, $width);
            $nextChar = mb_substr($text, $width, 1);
            if ( $nextChar !== ' ' )
            {
                $breakAt = mb_strrpos($part, ' ');
                if ( $breakAt === false )
                {
                    $breakAt = mb_strpos($text, ' ', $width);
                }
                if ( $breakAt === false )
                {
                    $parts[] = trim($text);
                    break;
                }
                $part = mb_substr($text, 0, $breakAt);
            }
            $part    = trim($part);
            $parts[] = $part;
            $text    = trim(mb_substr($text, mb_strlen($part)));
        }

        return implode($break, $parts);
    }

    /**
     * Highlights a given phrase in a text. You can specify any expression in highlighter that
     * may include the \1 expression to include the $phrase found.
     *
     * ### Options:
     *
     * - `format` The piece of html with that the phrase will be highlighted
     * - `html` If true, will ignore any HTML tags, ensuring that only the correct text is highlighted
     * - `regex` a custom regex rule that is used to match words, default is '|$tag|iu'
     *
     * @param string       $text    Text to search the phrase in.
     * @param string|array $phrase  The phrase or phrases that will be searched.
     * @param array        $options An array of html attributes and options.
     *
     * @return string The highlighted text
     * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/text.html#TextHelper::highlight
     */
    public static function highlight(string $text, string $phrase, array $options=[]) : string
    {
        if ( empty( $phrase ) )
        {
            return $text;
        }
        $defaults = [
            'format' => '<span class="highlight">\1</span>',
            'html'   => false,
            'regex'  => "|%s|iu"
        ];
        $options += $defaults;
        $html = $format = '';
        extract($options);
        if ( is_array($phrase) )
        {
            $replace = [];
            $with    = [];
            foreach ( $phrase as $key => $segment )
            {
                $segment = '(' . preg_quote($segment, '|') . ')';
                if ( $html )
                {
                    $segment = "(?![^<]+>)$segment(?![^<]+>)";
                }
                $with[]    = ( is_array($format) ) ? $format[$key] : $format;
                $replace[] = sprintf($options['regex'], $segment);
            }

            return preg_replace($replace, $with, $text);
        }
        $phrase = '(' . preg_quote($phrase, '|') . ')';
        if ( $html )
        {
            $phrase = "(?![^<]+>)$phrase(?![^<]+>)";
        }

        return preg_replace(sprintf($options['regex'], $phrase), $format, $text);
    }

    /**
     * Strips given text of all links (<a href=....).
     *
     * @param string $text Text
     *
     * @return string The text without links
     * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/text.html#TextHelper::stripLinks
     */
    public static function stripLinks(string $text) : string
    {
        return preg_replace('|<a\s+[^>]+>|im', '', preg_replace('|<\/a>|im', '', $text));
    }

    /**
     * Truncates text starting from the end.
     *
     * Cuts a string to the length of $length and replaces the first characters
     * with the ellipsis if the text is longer than length.
     *
     * ### Options:
     *
     * - `ellipsis` Will be used as Beginning and prepended to the trimmed string
     * - `exact` If false, $text will not be cut mid-word
     *
     * @param string  $text    String to truncate.
     * @param integer $length  Length of returned string, including ellipsis.
     * @param array   $options An array of options.
     *
     * @return string Trimmed string.
     */
    public static function tail(string $text, int $length=100, array $options=[]) : string
    {
        $defaults = [
            'ellipsis' => '...',
            'exact'    => true
        ];
        $options += $defaults;
        $ellipsis = $exact = '';
        extract($options);
        if ( ! function_exists('mb_strlen') )
        {
            class_exists('Multibyte');
        }
        if ( mb_strlen($text) <= $length )
        {
            return $text;
        }
        $truncate = mb_substr($text, mb_strlen($text) - $length + mb_strlen($ellipsis));
        if ( ! $exact )
        {
            $spacepos = mb_strpos($truncate, ' ');
            $truncate = $spacepos === false ? '' : trim(mb_substr($truncate, $spacepos));
        }

        return $ellipsis . $truncate;
    }

    /**
     * Extracts an excerpt from the text surrounding the phrase with a number of characters on each side
     * determined by radius.
     *
     * @param string  $text     String to search the phrase in
     * @param string  $phrase   Phrase that will be searched for
     * @param integer $radius   The amount of characters that will be returned on each side of the founded phrase
     * @param string  $ellipsis Ending that will be appended
     *
     * @return string Modified string
     * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/text.html#TextHelper::excerpt
     */
    public static function excerpt(string $text, string $phrase, int $radius=100, string $ellipsis = '...') : string
    {
        if ( empty( $text ) || empty( $phrase ) )
        {
            return self::truncate($text, $radius * 2, ['ellipsis' => $ellipsis]);
        }
        $append = $prepend = $ellipsis;
        $phraseLen  = mb_strlen($phrase);
        $textLen    = mb_strlen($text);
        $pos        = mb_strpos(mb_strtolower($text), mb_strtolower($phrase));
        if ( $pos === false )
        {
            return mb_substr($text, 0, $radius) . $ellipsis;
        }
        $startPos = $pos - $radius;
        if ( $startPos <= 0 )
        {
            $startPos = 0;
            $prepend  = '';
        }
        $endPos = $pos + $phraseLen + $radius;
        if ( $endPos >= $textLen )
        {
            $endPos = $textLen;
            $append = '';
        }
        $excerpt = mb_substr($text, $startPos, $endPos - $startPos);
        $excerpt = $prepend . $excerpt . $append;

        return $excerpt;
    }

    /**
     * Truncates text.
     *
     * Cuts a string to the length of $length and replaces the last characters
     * with the ellipsis if the text is longer than length.
     *
     * ### Options:
     *
     * - `ellipsis` Will be used as Ending and appended to the trimmed string (`ending` is deprecated)
     * - `exact` If false, $text will not be cut mid-word
     * - `html` If true, HTML tags would be handled correctly
     *
     * @param string  $text    String to truncate.
     * @param integer $length  Length of returned string, including ellipsis.
     * @param array   $options An array of html attributes and options.
     *
     * @return string Trimmed string.
     * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/text.html#TextHelper::truncate
     */
    public static function truncate(string $text, int $length=100, array $options=[]) : string
    {
        $defaults = [
            'ellipsis' => '...',
            'exact'    => true,
            'html'     => false
        ];
        if ( isset( $options['ending'] ) )
        {
            $defaults['ellipsis'] = $options['ending'];
        }
        elseif ( ! empty( $options['html'] ) && mb_internal_encoding() === 'UTF-8' )
        {
            $defaults['ellipsis'] = "\xe2\x80\xa6";
        }
        $options += $defaults;
        $html = $ellipsis = $exact = '';
        extract($options);
        if ( ! function_exists('mb_strlen') )
        {
            class_exists('Multibyte');
        }
        if ( $html )
        {
            if ( mb_strlen(preg_replace('/<.*?>/', '', $text)) <= $length )
            {
                return $text;
            }
            $totalLength = mb_strlen(strip_tags($ellipsis));
            $openTags    = [];
            $truncate    = '';
            preg_match_all('/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER);
            foreach ( $tags as $tag )
            {
                if ( ! preg_match('/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2]) )
                {
                    if ( preg_match('/<[\w]+[^>]*>/s', $tag[0]) )
                    {
                        array_unshift($openTags, $tag[2]);
                    }
                    elseif ( preg_match('/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag) )
                    {
                        $pos = array_search($closeTag[1], $openTags);
                        if ( $pos !== false )
                        {
                            array_splice($openTags, $pos, 1);
                        }
                    }
                }
                $truncate .= $tag[1];
                $contentLength = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3]));
                if ( $contentLength + $totalLength > $length )
                {
                    $left           = $length - $totalLength;
                    $entitiesLength = 0;
                    if ( preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE) )
                    {
                        foreach ( $entities[0] as $entity )
                        {
                            if ( $entity[1] + 1 - $entitiesLength <= $left )
                            {
                                $left --;
                                $entitiesLength += mb_strlen($entity[0]);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    $truncate .= mb_substr($tag[3], 0, $left + $entitiesLength);
                    break;
                }
                else
                {
                    $truncate .= $tag[3];
                    $totalLength += $contentLength;
                }
                if ( $totalLength >= $length )
                {
                    break;
                }
            }
        }
        else
        {
            if ( mb_strlen($text) <= $length )
            {
                return $text;
            }
            $truncate = mb_substr($text, 0, $length - mb_strlen($ellipsis));
        }
        $openTags = [];
        if ( ! $exact )
        {
            $spacepos = mb_strrpos($truncate, ' ');
            if ( $html )
            {
                $truncateCheck = mb_substr($truncate, 0, $spacepos);
                $lastOpenTag   = mb_strrpos($truncateCheck, '<');
                $lastCloseTag  = mb_strrpos($truncateCheck, '>');
                if ( $lastOpenTag > $lastCloseTag )
                {
                    preg_match_all('/<[\w]+[^>]*>/s', $truncate, $lastTagMatches);
                    $lastTag  = array_pop($lastTagMatches[0]);
                    $spacepos = mb_strrpos($truncate, $lastTag) + mb_strlen($lastTag);
                }
                $bits = mb_substr($truncate, $spacepos);
                preg_match_all('/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER);
                if ( ! empty( $droppedTags ) )
                {
                    if ( ! empty( $openTags ) )
                    {
                        foreach ( $droppedTags as $closingTag )
                        {
                            if ( ! in_array($closingTag[1], $openTags) )
                            {
                                array_unshift($openTags, $closingTag[1]);
                            }
                        }
                    }
                    else
                    {
                        foreach ( $droppedTags as $closingTag )
                        {
                            $openTags[] = $closingTag[1];
                        }
                    }
                }
            }
            $truncate = mb_substr($truncate, 0, $spacepos);
        }
        $truncate .= $ellipsis;
        if ( $html )
        {
            foreach ( $openTags as $tag )
            {
                $truncate .= '</' . $tag . '>';
            }
        }

        return $truncate;
    }

    /**
     * Creates a comma separated list where the last two items are joined with 'and', forming natural English
     *
     * @param array  $list      The list to be joined
     * @param string $and       The word used to join the last and second last items together with. Defaults to 'and'
     * @param string $separator The separator used to join all the other items together. Defaults to ', '
     *
     * @return string The glued together string.
     * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/text.html#TextHelper::toList
     */
    public static function toList(array $list, string $and='and', string $separator=', ') : string
    {
        return count($list) ? implode($separator, array_slice($list, 0 , -1)) . ' ' . $and . ' ' . array_pop($list) : array_pop($list);
    }

    /**
     * @param string $str1
     * @param string $str2
     * @param bool $caseSensitive
     * @return bool
     */
    public static function containsEachOther(string $str1, string $str2, bool $caseSensitive=true) : bool
    {
        if ( $caseSensitive )
        {
            return mb_stripos($str1, $str2) || mb_stripos($str2, $str1);
        }
        $res1 = mb_stripos(strtolower($str1), strtolower($str2)) !== false;
        $res2 = mb_stripos(strtolower($str2), strtolower($str1)) !== false;

        return $res1 || $res2;
    }

    /**
     * @param string $str
     * @return string
     */
    public static function toASCII(string $str) : string
    {
        return strtr(utf8_decode($str),
            utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
            'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
    }

    /**
     * @param string $str
     * @return string
     */
    public static function toAlfa(string $str) : string
    {
        return preg_replace('/[^a-zA-Z]/', '', $str);
    }

    /**
     * @param string $str
     * @return string
     */
    public static function toAlfaSpace(string $str) : string
    {
        return preg_replace('/[^a-zA-Z ]/', '', $str);
    }


    public static function strReplaceList(string $str, array $replacements) : string
    {
        foreach ( $replacements as $find => $replace )
        {
            $find                   = "%({$find})";
            $str                    = str_replace($find, $replace, $str);
        }

        return $str;
    }

    /**
     * @param string $mobileNumber
     * @return mixed|string
     */
    public static function formatMobileNumber(string $mobileNumber) : string
    {
        // Remove whitepace
        $mobileNumber           = trim(str_replace(' ', '', $mobileNumber));
        // Remove non-numeric characters but keep + characters
        $mobileNumber           = preg_replace('/[^0-9+]/', '', $mobileNumber);
        // If mobile number is in 04XXXXXXXX format change to 614XXXXXXXX
        //$mobileNumber           = preg_replace('/^04/', '614', $mobileNumber);
        // If mobile number is in +614XXXXXXXX format change to 614XXXXXXXX
        // $mobileNumber     = preg_replace('/^+([1-9])/', '61$1', $mobileNumber);
        $mobileNumber           = str_replace('+', '', $mobileNumber);

        return $mobileNumber;
    }


    public static function convertBinToGuid(string $binGuid, bool $encoded=true) : string
    {
        $binGuid    = $encoded ? base64_decode($binGuid) : $binGuid;
        $unpacked   = unpack('Va/v2b/n2c/Nd', $binGuid);

        return strtolower(sprintf('%08X-%04X-%04X-%04X-%04X%08X', $unpacked['a'], $unpacked['b1'], $unpacked['b2'], $unpacked['c1'], $unpacked['c2'], $unpacked['d']));
    }


    public static function convertGuidToBin(string $uuid, bool $encode=true) : string
    {
        $binary = call_user_func_array('pack', array_merge(['VvvCCC6'], array_map('hexdec', [
                substr($uuid, 0, 8),
                substr($uuid, 9, 4), substr($uuid, 14, 4),
                substr($uuid, 19, 2), substr($uuid, 21, 2)
            ]),
            array_map('hexdec', str_split(substr($uuid, 24, 12), 2))));

        return $encode ? base64_encode($binary) : $binary;
    }

    public static function fixLowerCaseNames(string $name) : string
    {
        return preg_match('/[a-z]+/', $name) ? $name : ucwords(strtolower($name));
    }


    public static function urlSafeBase64Encode(string $text) : string
    {
        return rtrim(strtr(base64_encode($text), "+/", "-_"), "=");
    }


    public static function urlSafeBase64Decode(string $text) : string
    {
        return base64_decode(strtr($text, "-_", "+/") . substr("===", (strlen($text) + 3) % 4));
    }

    public static function long2ipv4(int $ipAddress) : string
    {
        if ( PHP_VERSION_ID < 70100 )
        {
            return long2ip((string)$ipAddress);
        }

        return long2ip($ipAddress);
    }

} // class

