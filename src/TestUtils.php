<?php declare(strict_types=1);

namespace Terah\Utils;


use libphonenumber\PhoneNumberType;
use libphonenumber\RegionCode;
use Prism\DTO\UqmeDTO;
use Prism\Models\Account;
use Prism\Models\Client;
use Prism\Models\Model;
use Prism\Models\Uqme;
use Terah\Assert\Assert;
use libphonenumber\PhoneNumberUtil;
use Terah\FluentPdoModel\Inflector;
use Terah\Types\DTO;


/**
 * Class TestUtils
 *
 * Collection of test functions
 */
class TestUtils
{
    public static function getTestUsername(bool $valid=true, bool $default=true) : string
    {
        $validUsername          = $default ? 'uqbatest' : 'uqtest';

        return $valid ? $validUsername : 'uqbates';
    }


    public static function getTestUqme(bool $default=true) : string
    {
        $username               = TestUtils::getTestUsername(true, $default);
        $clientId               = Account::fetchClientIdByAccountName($username);
        Assert::that($clientId)->id();
        $uqme                   = Client::getUqmeById($clientId);
        Assert::that($uqme)->isni();

        return $uqme;
    }


    public static function generateRandomString(int $length=10, bool $withNumber=true, bool $withUpperCase=true) : string
    {
        $lowerCase              = range('a', 'z');
        $upperCase              = range('A', 'Z');
        $number                 = range(0, 9);
        $characters             = $lowerCase;
        $characters             = $withUpperCase ? array_merge($characters, $upperCase) : $characters;
        $characters             = $withNumber ? array_merge($characters, $number) : $characters;
        $max                    = count($characters) - 1;
        $randomString           = '';
        $length                 = $withUpperCase ? $length - 1 : $length;
        $length                 = $withNumber ? $length - 1 : $length;
        for ( $i = 0; $i < $length; $i++ )
        {
            $rand                   = mt_rand(0, $max);
            $randomString           .= $characters[$rand];
        }
        $randomString           .= $withUpperCase ? $upperCase[mt_rand(0, count($upperCase)-1)] : "";
        $randomString           .= $withNumber ? $number[mt_rand(0, count($number)-1)] : "";
        $randomString           = str_shuffle($randomString);

        return $randomString;
    }


    public static function generateRandomNumber(int $length=10) : int
    {
        $min                    = pow(10, $length - 1);
        $max                    = pow(10, $length) - 1;

        return mt_rand($min, $max);
    }


    public static function generateRandomIpAddress() : string
    {
        return StringUtils::long2ipv4(rand(0, 4294967295));
    }


    public static function generateFakePhoneNumber(bool $valid=true, bool $isMobile=true, string $region=RegionCode::AU) : string
    {
        $type                   = $isMobile ? PhoneNumberType::MOBILE : PhoneNumberType::FIXED_LINE;
        $phoneNumberLib         = PhoneNumberUtil::getInstance();
        $number                 = $valid ? $phoneNumberLib->getExampleNumberForType($region, $type) : $phoneNumberLib->getInvalidExampleNumber('AU');

        return $number->getNationalNumber();
    }


    public static function generateTestPassword(bool $valid=true) : string
    {
        $randomString           = static::generateRandomString(15);

        return $valid ? $randomString : '123asd';
    }


    public static function generateFakeEmailAddress(bool $valid=true) : string
    {
        $firstPart              = static::generateRandomString(8, true, false);
        $siteName               = static::generateRandomString(5, false, false);
        $topLevelDomains        = ['com', 'net', 'com.au', 'net.au', 'org'];
        $TLD                    = $valid ? $topLevelDomains[mt_rand(0, count($topLevelDomains)-1)] : "";

        return $firstPart . "@" . $siteName . "." . $TLD;
    }


    public static function createTestDataBasedOnModel(string $modelName, bool $insertToDb=false) : DTO
    {
        $className              = "\Prism\Models\\" . Inflector::classify($modelName);
        $tableName              = Inflector::underscore($modelName);

        if ( $className === "\Prism\Models\Uqme" )
        {
            $result             = Uqme::create(['uqme' => rand(3, 99999999)]);

            return Uqme::fetchDtoById($result->id);
        }

        $dtoName                = "\Prism\DTO\\" . Inflector::classify($modelName) . "DTO";
        /** @var Model $model */
        $model                  = new $className();
        $schema                 = $model->getSchema(true);
        $default                = ['id' => 'int', 'created_by_id' => 'int', 'created_ts' => 'timestamp', 'modified_by_id' => 'int', 'modified_ts' => 'timestamp', 'status' => 'int'];
        $schema                 = array_diff_assoc($schema, $default);
        /** @var DTO $dto */
        $dto                    = new $dtoName();

        foreach ( $schema as $column => $type )
        {
            if ( preg_match('/_id$/', $column) )
            {
                $foreignTable       = preg_replace('/_id$/', '', $column);
                $foreignDto         = static::createTestDataBasedOnModel($foreignTable, true);
                $dto->{$column}     = $foreignDto->id;

                continue;
            }
            if ( preg_match('/_fg$/', $column) )
            {
                $dto->{$column}     = mt_rand(0, 1);

                continue;
            }
            if ( $column === 'uqme' )
            {
                $dto->{$column}     = (string)rand(3, 99999999);
            }
            switch ( $type )
            {
                case 'varchar':

                    $dto->{$column}          = static::generateRandomString();

                    break;

                case 'timestamp':

                    $dto->{$column}          = Date::dateTime();

                    break;

                case 'date':

                    $dto->{$column}          = Date::date();

                    break;

                case 'enum':

                    $possibleValues         = static::getEnumValues($tableName, $column);
                    $dto->{$column}         = $possibleValues[mt_rand(0, count($possibleValues)-1)];

                    break;

            }
        }

        if ( $insertToDb )
        {
            $result                 = $model->insertArr($dto->getArrayCopy());
            $dto->id                = $result->id;
        }

        return $dto;
    }

    public static function getEnumValues(string $table, string $field) : array
    {
        $type                   = (new Model)->query( "SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->fetchField('type');
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);

        return explode("','", $matches[1]);
    }
}

