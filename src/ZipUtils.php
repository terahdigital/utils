<?php declare(strict_types=1);

namespace Terah\Utils;

use Terah\Assert\Assert;
/**
 * Class ZipUtils
 *
 * @package Terah\Utils
 */
class ZipUtils
{
    /**
     * @param string $source 	- File or directory to be zipped
     * @param string $destination - Zip file to create, will default to $source.zip
     * @param int $level		- Compression level 0-9
     * @param boolean $createPath - Create the path
     * @return bool|null|string - Returns destination file name or false on error
     */
    public static function zip(string $source, string $destination='', int $level=5, bool $createPath=true) : string
    {
        return static::_compress($source, $destination, 'zip', $level, $createPath);
    }

    /**
     * @param string $source 	- File or directory to be zipped
     * @param string $destination - Zip file to create, will default to $source.gzip
     * @param int $level		- Compression level 0-9
     * @param boolean $createPath - Create the path
     * @return bool|null|string - Returns destination file name or false on error
     */
    public static function gZip(string $source, string $destination='', int $level=5, $createPath=true) : string
    {
        return static::_compress($source, $destination, 'gzip', $level, $createPath);
    }

    /**
     * @param string $source 	- File or directory to be zipped
     * @param string $destination - Zip file to create, will default to $source.7z
     * @param int $level		- Compression level 0-9
     * @param boolean $createPath - Create the path
     * @param string $password - The password to generate with
     * @return bool|null|string - Returns destination file name or false on error
     */
    public static function sevenZip(string $source, string $destination='', int $level=5, $createPath=true, $password='') : string
    {
        return static::_compress($source, $destination, '7z', $level, $createPath, $password);
    }

    /**
     * @param string $source 	- File or directory to be zipped
     * @param string $destination - Zip file to create, will default to $source.$type
     * @param string $type		- 7z, gzip, zip, bzip2, tar, iso, udf
     * @param int $level		- Compression level 0-9
     * @param boolean $createPath - Create the path
     * @param string $password	- Password to encrypt the zip with
     * @return bool|null|string - Returns destination file name or false on error
     */
    static protected function _compress(string $source, string $destination='', string $type='7z', int $level=5, bool $createPath=true, $password='') : string
    {
        Assert::that($source)->fileOrDirectoryExists("The file(s) to be compressed ({$source}) do not exist");
        if ( is_dir($source) ) {

            $source 	    = StringUtils::addTrailingSlash($source);
            Assert::that($type)->notEq('gzip', 'Gzipping directories is not supported right now');
            $destination    = $destination ?: StringUtils::beforeLast('/', $source) . ".{$type}";
        }
        else
        {
            $destination    = $destination ?: "{$source}.{$type}";
        }
        if ( $destination && $createPath && ! FileUtils::createParentDirectories($destination) ) {

            return '';
        }
        Assert::that($type)->inArray(['7z', 'gzip', 'zip', 'bzip2', 'tar', 'iso', 'udf'], "Invalid zip type specified ({$type})");
        Assert::that($level)->greaterThan(0, "Invalid zip level specified ({$level})")->lessThan(10, "Invalid zip level specified ({$level})");
        //$source			= str_replace(' ', '\ ', $source);
        //$destination	= str_replace(' ', '\ ', $destination);
        //$source           = str_replace(' ', '\ ', $source);
        $dest  			= escapeshellarg($destination);
        $source         = escapeshellarg($source);
        $level 			= "-mx{$level}";
        $type 			= "-t{$type}";
        $password  		= $password  ? "-mhe=on -p'{$password}'" : '';
        //$case_sensitive = '-ssc';
        $command		= static::_getSevenZip() . " a {$type} {$dest} {$source} {$level} {$password}";
        $res 			= CliUtils::execCommandAndLogToObj($command);
        Assert::that($res->result)->truthy("Could not {$type} file(s): {$source} to {$destination}.");

        return $destination;
    }

    /**
     * @param string $source 			- Zip file to be extracted
     * @param string $destination 		- Location of the director to extract to
     * @param bool $createDestination  - Create the destination dir if not exists
     * @param string $password			- Password
     * @return bool|string				- Destination on success, false on fail.
     */
    public static function extract(string $source, string $destination='', bool $createDestination=false, string $password='') : string
    {
        Assert::that($source)->fileOrDirectoryExists("The file to be extracted ({$source}) does not exist");
        Assert::that(file_exists($destination) && ! is_dir($destination))->true("The destination is not a directory ({$destination})");

        if ( $createDestination )
        {
            FileUtils::createParentDirectories($destination, 0777);
            Assert::that($destination)->writeable("The destination directory is not writable ({$destination})");
        }
        $dest			= "-o'{$destination}'";
        $source         = escapeshellarg($source);
        $password  		= $password  ? "-p'{$password}'" : '';
        $command		= static::_getSevenZip() . " x {$source} {$dest} {$password}";
        $res 			= CliUtils::execCommandAndLogToObj($command);
        Assert::that($res->result)->truthy("Could not extract zip file to {$destination}");

        return $destination;
    }

    /**
     * @return string
     */
    static protected function _getSevenZip() : string
    {
        $location = '';
        foreach ( ['/usr/bin/7za', '/usr/local/bin/7za'] as $location )
        {
            if ( file_exists($location) )
            {
                return $location;
            }
        }
        Assert::that($location)->notEmpty("Failed to locate 7zip binary");

        return '';
    }
}
