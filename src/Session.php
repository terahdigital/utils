<?php declare(strict_types=1);

namespace Terah\Utils;

use Terah\Assert\Assert;

/**
 * Class Session
 *
 * @package Terah\Utils
 */
class Session
{

    public function __construct(array $config)
    {
        foreach ( $config as $key => $val )
        {
            ini_set("session.{$key}", $val);
        }
        // todo: Add check on whether redis is available and throw an exception
        session_start();
    }


    public function check(string $key) : bool
    {
        Assert::that($key)->string()->notEmpty();

        return isset($_SESSION[$key]);
    }


    public function has(string $key) : bool
    {
        return $this->check($key);
    }


    /**
     * @param string $key
     * @return mixed|null
     */
    public function read(string $key)
    {
        Assert::that($key)->string()->notEmpty();

        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }


    /**
     * @param string $key
     * @return mixed|null
     */
    public function get(string $key)
    {
        return $this->read($key);
    }

    /**
     * @param string $key
     * @param mixed $val
     */
    public function write(string $key, $val)
    {
        Assert::that($key)->string()->notEmpty();

        $_SESSION[$key]         = $val;
    }

    /**
     * @param string $key
     * @param mixed $val
     */
    public function set(string $key, $val)
    {
        $this->write($key, $val);
    }


    public function delete(string $key)
    {
        Assert::that($key)->notEmpty();

        unset($_SESSION[$key]);
    }


    public function remove(string $key)
    {
        $this->delete($key);
    }


    public function all() : array
    {
        return $_SESSION;
    }


    public function getDebug() : array
    {
        return [
            'SESSION_ID'        => session_id(),
            'SESSION'           => $_SESSION,
            'SESSION_STATUS'    => session_status(),
            'COOKIE'            => isset($_COOKIE['PHPSESSID']) ? ['PHPSESSID' => $_COOKIE['PHPSESSID']] : null,
            'COOKIE_SETTINGS'   => session_get_cookie_params(),
        ];
    }


    public function destroy()
    {
        session_destroy();
    }
}