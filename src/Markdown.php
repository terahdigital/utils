<?php declare(strict_types=1);


namespace Terah\Utils;

use ArrayAccess;
use League\CommonMark\Converter;
use stdClass;

class Markdown
{
    /**
     * @param string    $html
     * @param string    $text
     * @param array     $params
     * @param Converter $markdown
     * @return stdClass
     */
    public static function renderMarkdownWithPlaceholders(string $html, string $text, array $params, Converter $markdown) : stdClass
    {
        $text           = static::renderPlaceholders($text, $params);
        $text           = static::stripMarkdown($text);
        $html           = static::renderPlaceholders($html, $params);
        $html           = $markdown->convertToHtml($html);

        return (object)[
            'text'          => $text,
            'html'          => $html,
        ];
    }

    /**
     * @param string $markdown
     * @param bool   $stripHeaders
     * @param bool   $gfm
     * @return string
     */
    public static function stripMarkdown(string $markdown, bool $stripHeaders=false, bool $gfm=true) : string
    {
        if ( $stripHeaders )
        {
            $markdown = preg_replace('/^([\s\t]*)([\*\-\+]|\d\.)\s+/m', '$1', $markdown);
        }
        if ( $gfm )
        {
            $markdown = preg_replace('/\n={2,}/', "\n", $markdown);    // Header
            $markdown = preg_replace('/~~/', '', $markdown);           // Strikethrough
            $markdown = preg_replace('/`{3}.*\n/', '', $markdown);     // Fenced codeblocks
        }

        $markdown = preg_replace('/<(.*?)>/', '$1', $markdown); //Remove HTML tags
        $markdown = preg_replace('/^[=\-]{2,}\s*$/', '', $markdown); // Remove setext-style headers
        $markdown = preg_replace('/\[\^.+?\](\: .*?$)?/', '', $markdown); // Remove footnotes?
        $markdown = preg_replace('/\s{0,2}\[.*?\]: .*?$/', '', $markdown); //
        $markdown = preg_replace('/\!\[.*?\][\[\(].*?[\]\)]/', '', $markdown); // Remove images
        //$markdown = preg_replace('/\[(.*?)\][\[\(].*?[\]\)]/', '$1', $markdown); // Remove inline links
        $markdown = preg_replace('/\[(.*?)\]/', '$1 ', $markdown); // Remove inline links
        $markdown = preg_replace('/>/', '', $markdown); // Remove Blockquotes
        $markdown = preg_replace('/^\s{1,2}\[(.*?)\]: (\S+)( ".*?")?\s*$/', '', $markdown); // Remove reference-style links?
        //$markdown = preg_replace('/^\#{1,6}\s*([^#]*)\s?/m', '$1 ', $markdown); // Remove atx-style headers
        $markdown = preg_replace('/^\#{1,6}\s*([^#]*)\s?/m', '$1', $markdown); // Remove atx-style headers
        $markdown = preg_replace('/([\*_]{1,3})(\S.*?\S)\1/', '$2', $markdown);
        $markdown = preg_replace('/(`{3,})(.*?)\1/m', '$2', $markdown);
        $markdown = preg_replace('/^-{3,}\s*$/', '', $markdown);
        $markdown = preg_replace('/`(.+?)`/', '$1', $markdown);
        $markdown = preg_replace('/\n{2,}/', "\n\n", $markdown);

        return $markdown;
    }

    /**
     * @param string $tmplText
     * @param array  $params
     * @return string
     */
    public static function renderPlaceholders(string $tmplText, array $params) : string
    {
        if ( preg_match_all("/{{[ ]{0,5}([a-zA-Z0-9_.]+)[ ]{0,5}}}/", $tmplText, $matches) )
        {
            $replaces = [];
            foreach ( $matches[0] as $idx => $match )
            {
                $replaces[$match]   = $matches[1][$idx];
            }
            foreach ( $replaces as $search => $paramKey )
            {
                $value              = static::getDotNotation($params, $paramKey, '');
                $tmplText           = str_replace($search, $value, $tmplText);
            }
        }

        return $tmplText;
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @param  ArrayAccess|array  $array
     * @param  null|string  $key
     * @param  mixed   $default
     * @return mixed
     */
    public static function getDotNotation($array, ?string $key, $default=null)
    {
        if ( ! static::accessible($array) )
        {
            return $default;
        }
        if ( is_null($key) )
        {
            return $array;
        }
        if ( static::keyExists($array, $key) )
        {
            return $array[$key];
        }
        if ( strpos($key, '.') === false )
        {
            return $array[$key] ?? $default;
        }
        foreach ( explode('.', $key) as $segment )
        {
            if ( static::accessible($array) && static::keyExists($array, $segment) )
            {
                $array          = $array[$segment];
            }
            else
            {
                return $default;
            }
        }

        return $array;
    }

    /**
     * Determine whether the given value is array accessible.
     *
     * @param  mixed  $value
     * @return bool
     */
    public static function accessible($value): bool
    {
        return is_array($value) || $value instanceof ArrayAccess;
    }

    /**
     * Determine if the given key exists in the provided array.
     *
     * @param  ArrayAccess|array  $array
     * @param  string|int  $key
     * @return bool
     */
    public static function keyExists($array, $key): bool
    {
        if ( $array instanceof ArrayAccess)
        {
            return $array->offsetExists($key);
        }

        return array_key_exists($key, $array);
    }

}