<?php declare(strict_types=1);
/*
The MIT License

Copyright (c) 2010 Antoine Leclair <antoineleclair@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */
namespace Terah\Utils;

use Exception;

/**
 * Taken from https://raw.githubusercontent.com/ockam/php-csv/master/csv.php
 *
 * Class Csv
 */

class Csv
{

    /**
     * take a CSV line (utf-8 encoded) and returns an array
     * 'string1,string2,"string3","the ""string4"""' => array('string1', 'string2', 'string3', 'the "string4"')
     *
     * @param string $string
     * @param string $separator
     * @return array
     */
    public static function parseString(string $string, string $separator=',') : array
    {
        $values = [];
        $string = str_replace("\r\n", '', $string); // eat the trailing new line, if any
        if ( $string == '' )
        {
            return $values;
        }
        $tokens = explode($separator, $string);
        $count  = count($tokens);
        for ( $i = 0; $i < $count; $i++ )
        {
            $token    = $tokens[$i];
            $len      = strlen($token);
            $newValue = '';
            if ( $len > 0 and $token[0] == '"' )
            { // if quoted
                $token = substr($token, 1); // remove leading quote
                do
                { // concatenate with next token while incomplete
                    $complete = Csv::_hasEndQuote($token);
                    $token    = str_replace('""', '"', $token); // un-escape escaped quotes
                    $len      = strlen($token);
                    if ( $complete )
                    { // if complete
                        $newValue .= substr($token, 0, -1); // remove trailing quote
                    }
                    else
                    { // incomplete, get one more token
                        $newValue .= $token;
                        $newValue .= $separator;
                        if ( $i == $count - 1 )
                        {
                            throw new Exception('Illegal unescaped quote.');
                        }
                        $token = $tokens[++$i];
                    }
                }
                while ( ! $complete );
            }
            else
            { // unescaped, use token as is
                $newValue .= $token;
            }

            $values[] = $newValue;
        }

        return $values;
    }

    /**
     * @param string $string
     * @return string
     */
    public static function escapeString(string $string) : string
    {
        $string = str_replace('"', '""', (string)$string);
        if ( strpos($string, '"') !== false or strpos($string, ',') !== false or strpos($string, "\r") !== false or strpos($string, "\n") !== false )
        {
            $string = '"' . $string . '"';
        }

        return $string;
    }

    // checks if a string ends with an unescaped quote
    // 'string"' => true
    // 'string""' => false
    // 'string"""' => true
    /**
     * @param string $token
     * @return bool
     */
    public static function _hasEndQuote(string $token) : bool
    {
        $len = strlen($token);
        if ( $len == 0 )
        {
            return false;
        }
        elseif ( $len == 1 and $token == '"' )
        {
            return true;
        }
        elseif ( $len > 1 )
        {
            while ( $len > 1 and $token[$len - 1] == '"' and $token[$len - 2] == '"' )
            { // there is an escaped quote at the end
                $len -= 2; // strip the escaped quote at the end
            }
            if ( $len == 0 )
            {
                return false;
            } // the string was only some escaped quotes
            elseif ( $token[$len - 1] == '"' )
            {
                return true;
            } // the last quote was not escaped
            else
            {
                return false;
            } // was not ending with an unescaped quote
        }

        return false;
    }

    // very basic separator detection function
    /**
     * @param string $filename
     * @param array $separators
     * @return string
     */
    public static function detectSeparator(string $filename, array $separators = [',', ';']) : string
    {
        $file   = fopen($filename, 'r');
        $string = fgets($file);
        fclose($file);
        $matched = [];
        foreach ( $separators as $separator )
        {
            if ( preg_match("/$separator/", $string) )
            {
                $matched[] = $separator;
            }
        }
        if ( count($matched) == 1 )
        {
            return $matched[0];
        }

        return '';
    }
}
