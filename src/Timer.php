<?php declare(strict_types=1);

namespace Terah\Utils;

use Psr\Log\LogLevel;
use Terah\Assert\Assert;
use Terah\ColourLog\LoggerTrait;

/**
 * Class Timer
 *
 * @package Terah\Utils
 */
class Timer
{
    use LoggerTrait;

    protected array $timeData = [];

    protected int $precision = 3;

    public function __construct()
    {
        $this->tick();
    }


    public function precision(int $precision) : Timer
    {
        Assert::that($precision)->int();
        $this->precision = $precision;

        return $this;
    }


    public function getTimers() : array
    {
        return $this->timeData;
    }


    public function start(string $key='app') : array
    {
        unset($this->timeData[$key]);

        return $this->tick($key);
    }


    public function finish(string $key='app') : array
    {
        return $this->tick($key, true);
    }


    public function finishAllJobs(string $message='', int $success=0, string $loglevel=LogLevel::NOTICE, $context=[]) : bool
    {
        foreach ( $this->timeData as $key => $data )
        {
            $this->finishJob($key, $message, $success, $loglevel, $context);
        }

        return true;
    }


    public function tick(string $key='app', bool $remove=false) : array
    {
        if ( ! isset($this->timeData[$key]['start']) )
        {
            $this->timeData[$key]['start'] = microtime(true);
        }
        $this->timeData[$key]['last']  = microtime(true);
        $this->timeData[$key]['total'] = $this->timeData[$key]['last'] - $this->timeData[$key]['start'];
        $taken = $this->timeTaken($key);
        if ( $remove )
        {
            unset($this->timeData[$key]);
        }

        return $taken;
    }


    public function addSeconds(string $key='app', float $fSeconds=10.0)
    {
        $this->timeData[$key]['total'] = isset($this->timeData[$key]['total']) ? $this->timeData[$key]['total'] : 0;
        $this->timeData[$key]['total'] += $fSeconds;
    }


    public function timeTaken(string $key='app') : array
    {
        $seconds = round($this->timeData[$key]['total'], $this->precision);

        return [$seconds, static::humanTime($seconds)];
    }


    public function getStart(string $key='app', string $format='') : string
    {
        Assert::that($this->timeData)->keyExists($key, "The timer does not exist for key {$key}");

        return $this->_formatTimestamp($key, 'start', $format);
    }


    public function getLast(string $key='app', string $format='')
    {
        Assert::that($this->timeData)->keyExists($key, "The timer does not exist for key {$key}");

        return $this->_formatTimestamp($key, 'last', $format);
    }


    public function startJob(string $function, string $loglevel=LogLevel::NOTICE, array $context=[])
    {
        $this->logger->log($loglevel, "Starting job {$function}", $context);
        $this->start($function);
    }


    public function finishJob(string $function, string $message='', int $success=0, string $loglevel=LogLevel::NOTICE, array $context=[]) : bool
    {
        if ( $message )
        {
            $this->logger->log($loglevel, $message, $context);
        }
        list($seconds, $human)  = $this->finish($function);
        $opsPerSec              = '';
        $rate                   = '';
        if ( $success && $seconds > 0 )
        {
            $opsPerSec              = round($success / $seconds, 3);
            $opsPerSec              = " at a rate of {$opsPerSec}ops/sec";
            $rate                   = round($seconds / $success, 3);
            $rate                   = " ({$rate} secs per op)";
        }
        $this->logger->log($loglevel, "The job ({$function}) took {$human} to complete{$opsPerSec}{$rate}.", $context);

        return true;
    }


    public function startProcess(string $function, string $loglevel=LogLevel::INFO, array $context=[])
    {
        $this->logger->log($loglevel, "Starting process {$function}", $context);
        $this->start($function);
    }


    public function finishProcess(string $function, string $message='', array $context=[], string $loglevel=LogLevel::INFO)
    {
        return $this->finishJob($function, $message, 0, $loglevel, $context);
    }


    protected function _formatTimestamp(string $key, string $name, string $format='') : string
    {
        Assert::that($this->timeData)->keyExists($key, "The timer does not exist for key {$key}");
        Assert::that($this->timeData[$key])->keyExists($name, "The timer value does not exist for key {$key} and name {$name}");
        if ( ! $format )
        {
            return $this->timeData[$key]['start'];
        }
        $format     = empty($format) ? 'r' : $format;

        return date($format, $this->timeData[$key]['start']);
    }


    public static function humanTime(float $seconds) : string
    {
        $microsecs  = $seconds - intval($seconds);
        $seconds    = intval($seconds);
        $result     = [];
        $hours      = intval($seconds / 3600);
        if( $hours > 0 )
        {
            $result[] = "{$hours} hours ";
        }
        $minutes = floor(bcmod((string)($seconds / 60), '60'));
        if( $hours > 0 || $minutes > 0 )
        {
            $result[] = "{$minutes} minutes ";
        }
        $seconds = bcmod((string)intval($seconds), '60');
        $seconds = number_format(floatval($seconds) + floatval($microsecs), 2);
        $result[] = "{$seconds} seconds";

        return implode('', $result);
    }
}
