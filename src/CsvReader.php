<?php declare(strict_types=1);

/*
The MIT License

Copyright (c) 2010 Antoine Leclair <antoineleclair@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

namespace Terah\Utils;

use Iterator;

/**
 * Taken from https://raw.githubusercontent.com/ockam/php-csv/master/csv.php
 *
 * Class CsvReader
 */

class CsvReader implements Iterator
{
    /** @var bool|resource  */
    protected $fileHandle           = null;

    protected int $position         = 0;

    protected string $filename      = '';

    protected ?string $currentLine  = null;

    protected ?array $currentArray  = null;

    protected string $separator     = ',';


    public function __construct(string $filename, string $separator = ',')
    {
        $this->separator        = $separator;
        $this->fileHandle       = fopen($filename, 'r');
        if ( ! $this->fileHandle )
        {
            return;
        }
        $this->filename         = $filename;
        $this->position         = 0;
        $this->_readLine();
    }

    public function __destruct()
    {
        $this->close();
    }

    // You should not have to call it unless you need to
    // explicitly free the file descriptor
    public function close()
    {
        if ( $this->fileHandle )
        {
            fclose($this->fileHandle);
            $this->fileHandle   = null;
        }
    }

    public function rewind()
    {
        if ( $this->fileHandle )
        {
            $this->position     = 0;
            rewind($this->fileHandle);
        }

        $this->_readLine();
    }

    /**
     * @return array|null
     */
    public function current(): ?array
    {
        return $this->currentArray;
    }

    /**
     * @return int|mixed|null
     */
    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        $this->position++;
        $this->_readLine();
    }

    /**
     * @return bool
     */
    public function valid(): bool
    {
        return $this->currentArray !== null;
    }

    protected function _readLine()
    {
        if ( ! feof($this->fileHandle) )
        {
            $this->currentLine      = trim(utf8_encode((string)fgets($this->fileHandle)));
        }
        else
        {
            $this->currentLine      = null;
        }
        if ( $this->currentLine != '' )
        {
            $this->currentArray     = Csv::parseString($this->currentLine, $this->separator);
        }
        else
        {
            $this->currentArray     = null;
        }
    }
}
