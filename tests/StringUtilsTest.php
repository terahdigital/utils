<?php declare(strict_types=1);

namespace Terah\Utils\Tests;

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Terah\Utils\StringUtils;
use PHPUnit_Framework_TestCase;

class StringUtilsTest extends PHPUnit_Framework_TestCase
{
    protected ?StringUtils $testSubject = null;

    protected function setUp()
    {

    }

    public function testStartsWithSuccess()
    {
        $needle                         = 'AaBbCcDd';
        $hayStack                       = 'AaBbCcDdEeFdGgHh';
        $actual                         = StringUtils::startsWith($needle, $hayStack);
        $expected                       = true;

        static::assertEquals($actual, $expected);
    }

    public function testEndsWithSuccess()
    {
        $needle                         = 'EeFdGgHh';
        $hayStack                       = 'AaBbCcDdEeFdGgHh';
        $actual                         = StringUtils::endsWith($needle, $hayStack);
        $expected                       = true;

        static::assertEquals($actual, $expected);
    }

    public function testReplaceBetweenSuccess()
    {
        $start                          = 'AaBb';
        $end                            = 'GgHh';
        $replace                        = 'XXXXX';
        $hayStack                       = 'AaBbCcDdEeFdGgHh';
        $separator                      = '#';
        $actual                         = StringUtils::replaceBetween($start, $end, $replace, $hayStack, $separator);
        $expected                       = 'AaBb#XXXXX#GgHh';

        static::assertEquals($actual, $expected);
    }

    public function testBetweenSuccess()
    {
        $start                          = 'AaBb';
        $end                            = 'GgHh';
        $hayStack                       = 'AaBbCcDdEeFdGgHh';
        $actual                         = StringUtils::between($start, $end, $hayStack);
        $expected                       = 'CcDdEeFd';

        static::assertEquals($actual, $expected);
    }

    public function testBeforeSuccess()
    {
        $needle                         = 'FdGgHh';
        $hayStack                       = 'AaBbCcDdEeFdGgHh';
        $returnOrigIfNeedleNotExists    = false;
        $actual                         = StringUtils::before($needle, $hayStack, $returnOrigIfNeedleNotExists);
        $expected                       = 'AaBbCcDdEe';

        static::assertEquals($actual, $expected);
    }

    public function testAfterSuccess()
    {
        $needle                         = 'AaBbCc';
        $hayStack                       = 'AaBbCcDdEeFdGgHh';
        $returnOrigIfNeedleNotExists    = false;
        $actual                         = StringUtils::after($needle, $hayStack, $returnOrigIfNeedleNotExists);
        $expected                       = 'DdEeFdGgHh';

        static::assertEquals($actual, $expected);
    }

    public function testBetweenLastSuccess()
    {
        $start                          = 'AaBb';
        $end                            = 'GgHh';
        $hayStack                       = 'AaBbCcDdEeFdGgHhAaBbXXXXXXXGgHh';
        $actual                         = StringUtils::betweenLast($start, $end, $hayStack);
        $expected                       = 'XXXXXXX';

        static::assertEquals($actual, $expected);
    }

    public function testAfterLastSuccess()
    {
        $needle                         = 'AaBbCcDdEeFdGgHh';
        $hayStack                       = 'AaBbCcDdEeFdGgHhAaBbCcDdEeFdGgHhXXXXAaBbCcDdEeFdGg';
        $returnOrigIfNeedleNotExists    = false;
        $actual                         = StringUtils::afterLast($needle, $hayStack, $returnOrigIfNeedleNotExists);
        $expected                       = 'XXXXAaBbCcDdEeFdGg';

        static::assertEquals($actual, $expected);
    }


    public function testStrrevposSuccess()
    {
        $str                            = 'AaBbCcDdEeFdGgHhAaBbCcDdEeFdGgHhXXXXAaBbCcDdEeFdGgHh';
        $needle                         = 'XXXX';
        $actual                         = StringUtils::strrevpos($str, $needle);
        $expected                       = 32;

        static::assertEquals($actual, $expected);
    }

    public function testBeforeLastSuccess()
    {
        $needle                         = 'AaBbCcDdEeFdGgHh';
        $hayStack                       = 'AaBbCcDdEeFdGgHhAaBbCcDdEeFdGgHhXXXXAaBbCcDdEeFdGgHh';
        $actual                         = StringUtils::beforeLast($needle, $hayStack);
        $expected                       = 'AaBbCcDdEeFdGgHhAaBbCcDdEeFdGgHhXXXX';

        static::assertEquals($actual, $expected);
    }

    public function testHumanFileSizeSuccess()
    {
        $bytes                          = 1024 * 1024 * 1024;
        $decimals                       = 2;
        $actual                         = StringUtils::humanFileSize($bytes, $decimals);
        $expected                       = '1.00G';

        static::assertEquals($actual, $expected);
    }

    public function testSecondsToWordsSuccess()
    {
        $seconds                        = 60 * 60 * 12 + 118;
        $actual                         = StringUtils::secondsToWords($seconds);
        $expected                       = '12 hours 1 minutes 58 seconds';

        static::assertEquals($actual, $expected);
    }

    public function testAddTrailingSlashSuccess()
    {
        $string                         = '/asdfasdf/asdfasdf/asdfasdf';
        $actual                         = StringUtils::addTrailingSlash($string);
        $expected                       = '/asdfasdf/asdfasdf/asdfasdf/';

        static::assertEquals($actual, $expected);
    }

    public function testAddTrailingCharIfNotExistsSuccess()
    {
        $string                         = '/asdfasdf/asdfasdf/asdfasdf';
        $char                           = '/';
        $actual                         = StringUtils::addTrailingCharIfNotExists($string, $char);
        $expected                       = '/asdfasdf/asdfasdf/asdfasdf/';

        static::assertEquals($actual, $expected);
    }

    public function testGetElidedStringSuccess()
    {
        $string                         = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $length                         = 30;
        $actual                         = StringUtils::getElidedString($string, $length);
        $expected                       = 'THE QUICK BROWN FOX JUMPED ...';

        static::assertEquals($actual, $expected);
    }

    public function testRemoveEmptyAndImplodeSuccess()
    {
        $array                          = [
            '', '   ', "", 'asdf', 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890'
        ];
        $glue                           = '#';
        $actual                         = StringUtils::removeEmptyAndImplode($array, $glue);
        $expected                       = '   #asdf#THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';

        static::assertEquals($actual, $expected);
    }

    public function testGetCutStringAtWordBreakSuccess()
    {
        $string                         = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $length                         = 23;
        $break_on_non_alpha             = false;
        $suffix                         = '';
        $force_suffix_on_orig           = false;
        $actual                         = StringUtils::getCutStringAtWordBreak($string, $length, $break_on_non_alpha, $suffix, $force_suffix_on_orig);
        $expected                       = 'THE QUICK BROWN FOX JUMPED';

        static::assertEquals($actual, $expected);
    }

    public function testUcnameSuccess()
    {
        $string                         = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $actual                         = StringUtils::ucname($string);
        $expected                       = 'The Quick Brown Fox Jumped Over The Lazy Dog\'S Back 1234567890';

        static::assertEquals($actual, $expected);
    }

    public function testToFlagSuccess()
    {
        $trueValues                     = [ 'Y', 'y', true, '1', 'Yes', 'yes', 1];
        $falseValues                    = [ 'F', 'f', false, '0', 'No', 'no', 0];
        foreach ( $trueValues as $val )
        {
            $actual                         = StringUtils::toFlag($val);
            static::assertEquals($actual, '1', "Failed asserting that {$val} flags to 1");
        }
        foreach ( $falseValues as $val )
        {
            $actual                         = StringUtils::toFlag($val);
            static::assertEquals($actual, '0', "Failed asserting that {$val} flags to 0");
        }
    }

    public function testConvertDigitSizeMbSuccess()
    {
        $size                           = 1024 * 1024 * 3.5;
        $actual                         = StringUtils::convertDigitSize($size);
        $expected                       = '3.5 mb';

        static::assertEquals($actual, $expected);
    }

    public function testConvertDigitSizeGbSuccess()
    {
        $size                           = 1024 * 1024 * 1024 + 1024 * 1024 * 512;
        $actual                         = StringUtils::convertDigitSize($size);
        $expected                       = '1.5 gb';

        static::assertEquals($actual, $expected);
    }

    public function testRemoveDoubleSpaceSuccess()
    {
        $string                         = 'THE QUICK BROWN   FOX JUMPED OVER THE   LAZY DOG\'S BACK     1234567890';
        $actual                         = StringUtils::removeDoubleSpace($string);
        $expected                       = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';

        static::assertEquals($actual, $expected);
    }

    public function testCleanseWhiteSpaceSuccess()
    {
        $string                         = 'THE QUICK BROWN   FOX JUMPED OVER THE   LAZY DOG\'S BACK     1234567890';
        $actual                         = StringUtils::cleanseWhiteSpace($string);
        $expected                       = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';

        static::assertEquals($actual, $expected);
    }

    public function testTokenizeSuccess()
    {
        $data                           = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $actual                         = StringUtils::tokenize($data);
        $expected                       = ['THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890'];

        static::assertEquals($actual, $expected);
    }

    public function testWrapSuccess()
    {
        $text                           = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $options                        = ['width' => 5];
        $actual                         = StringUtils::wrap($text, $options);
        $expected                       = "THE\nQUICK\nBROWN\nFOX\nJUMPED\nOVER\nTHE\nLAZY\nDOG'S\nBACK\n1234567890";

        static::assertEquals($actual, $expected);
    }

    public function testWordWrapSuccess()
    {
        $text                           = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $width                          = 10;
        $break                          = '<br>';
        $cut                            = false;
        $actual                         = StringUtils::wordWrap($text, $width, $break, $cut);
        $expected                       = 'THE QUICK<br>BROWN FOX<br>JUMPED<br>OVER THE<br>LAZY DOG\'S<br>BACK<br>1234567890';

        static::assertEquals($actual, $expected);
    }

    public function testHighlightSuccess()
    {
        $text                           = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $phrase                         = 'QUICK BROWN FOX';
        $options                        = [];
        $actual                         = StringUtils::highlight($text, $phrase, $options);
        $expected                       = 'THE <span class="highlight">QUICK BROWN FOX</span> JUMPED OVER THE LAZY DOG\'S BACK 1234567890';

        static::assertEquals($actual, $expected);
    }

    public function testStripLinksSuccess()
    {
        $text                           = 'THE QUICK BROWN FOX JUMPED <a href="/asdfasdf">OVER</a> THE LAZY DOG\'S BACK 1234567890';
        $actual                         = StringUtils::stripLinks($text);
        $expected                       = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';

        static::assertEquals($actual, $expected);
    }

    public function testTailSuccess()
    {
        $text                           = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $length                         = 10;
        $options                        = [];
        $actual                         = StringUtils::tail($text, $length, $options);
        $expected                       = '...4567890';

        static::assertEquals($actual, $expected);
    }

    public function testExcerptSuccess()
    {
        $text                           = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $phrase                         = '';
        $radius                         = 0;
        $ellipsis                       = '';
        $actual                         = StringUtils::excerpt($text, $phrase, $radius, $ellipsis);
        $expected                       = '';

        static::assertEquals($actual, $expected);
    }

    public function testTruncateSuccess()
    {
        $text                           = 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890';
        $length                         = 30;
        $options                        = [];
        $actual                         = StringUtils::truncate($text, $length, $options);
        $expected                       = 'THE QUICK BROWN FOX JUMPED ...';

        static::assertEquals($actual, $expected);
    }

    public function testToListSuccess()
    {
        $list                           = explode(' ', 'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG\'S BACK 1234567890');
        $and                            = 'and';
        $separator                      = ', ';
        $actual                         = StringUtils::toList($list, $and, $separator);
        $expected                       = 'THE, QUICK, BROWN, FOX, JUMPED, OVER, THE, LAZY, DOG\'S, BACK and 1234567890';

        static::assertEquals($actual, $expected);
    }

    public function testContainsEachOtherSuccess()
    {
        $str1                           = 'AaBbCcDdEeFdGgHh';
        $str2                           = 'BbCcDdEeFdGg';
        $caseSensitive                  = false;
        $actual                         = StringUtils::containsEachOther($str1, $str2, $caseSensitive);
        $expected                       = true;

        static::assertEquals($actual, $expected);
    }

    public function testToASCIISuccess()
    {
        $str                            = 'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ';
        $actual                         = StringUtils::toASCII($str);
        $expected                       = 'YYYYYYYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy';

        static::assertEquals($actual, $expected);
    }

    public function testToAlfaSuccess()
    {
        $str                            = '!"#$%&\'()*+,-./012 3456789:;<=>?@ABCD EFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefgh ijklmnopq rstuvwxyz{|}~';
        $actual                         = StringUtils::toAlfa($str);
        $expected                       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        static::assertEquals($actual, $expected);
    }

    public function testToAlfaSpaceSuccess()
    {
        $str                            = '!"#$%&\'()*+,-./012 3456789:;<=>?@ABCD EFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefgh ijklmnopq rstuvwxyz{|}~';
        $actual                         = StringUtils::toAlfaSpace($str);
        $expected                       = ' ABCD EFGHIJKLMNOPQRSTUVWXYZabcdefgh ijklmnopq rstuvwxyz';

        static::assertEquals($actual, $expected);
    }

    public function testStrReplaceListSuccess()
    {
        $data                           = [
            'day'                           => '25',
            'month'                         => 'July',
            'year'                          => '1950',
        ];
        $strings                        = [
            '%(day) %(month) %(year)'       => '25 July 1950',
            '%(month) %(month) %(year)'     => 'July July 1950',
            '%(year) %(month) %(year)'      => '1950 July 1950',
            '%(month)%%(month)%%(year)'     => 'July%July%1950',
        ];
        foreach ( $strings as $str => $expected )
        {
            $actual                         = StringUtils::strReplaceList($str, $data);
            static::assertEquals($actual, $expected);
        }
    }

    public function tearDown()
    {

    }

    /**
     * @param bool $force
     * @return StringUtils
     */
    protected function getTestSubject($force=false): ?StringUtils
    {
        if ( $force || ! $this->testSubject )
        {
            $this->testSubject = new StringUtils();
        }

        return $this->testSubject;
    }
}

